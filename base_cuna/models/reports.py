# -*- coding: utf-8 -*-

import calendar
import datetime
from ...report_xlsx.report.report_xlsx import ReportXlsx
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError
from odoo import fields
from xlsxwriter.utility import xl_range


def calculate_duration(start, end):
    daygenerator = [start + timedelta(x + 1) for x in xrange((end - start).days)]
    daygenerator += [start + timedelta(0)]
    days = sum(1 for day in daygenerator if day.weekday() < 5)
    return days


def obtener_dias_asistencia(obj, anio, mes):
    # Sirve para calcular los dias de asistencia por niño dependiendo de su fecha de ingreso y fin al modulo.
    # Tambien fue usado para validar si un actor comunal estuvo un mes determinado.
    modulo_ninios = False
    if obj._name == 'modulo.ninios':
        modulo_ninios = True
    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')

    # primero obtengo el primer y ultimo dia del mes
    primer_dia, ultimo_dia = get_month_day_range(date_str)

    # convierto la fecha del obj a datetime
    fecha_inicio = convertir_str_date(obj.fecha_inicio if modulo_ninios else obj.fecha_ingreso)

    fecha = obj.fecha_fin if modulo_ninios else obj.fecha_retiro
    if fecha:
        fecha_fin = convertir_str_date(obj.fecha_fin if modulo_ninios else obj.fecha_retiro)

        if fecha_inicio <= primer_dia:
            if fecha_fin < primer_dia:
                return False
            else:
                days = calculate_duration(primer_dia, fecha_fin)
                return days
        else:
            if fecha_inicio > ultimo_dia:
                return False
            else:
                if fecha_fin < ultimo_dia:
                    days = calculate_duration(fecha_inicio, fecha_fin)
                else:
                    days = calculate_duration(fecha_inicio, ultimo_dia)
                return days
    else:
        if fecha_inicio > ultimo_dia:
            return False
        else:
            if fecha_inicio <= primer_dia:
                days = calculate_duration(primer_dia, ultimo_dia)
            else:
                days = calculate_duration(fecha_inicio, ultimo_dia)
            return days


def validar_periodos(periodo_inicio, periodo_fin):
    periodo_inicio = datetime.strptime(periodo_inicio, '%Y-%m-%d')
    periodo_fin = datetime.strptime(periodo_fin, '%Y-%m-%d')
    periodos = []

    while periodo_fin >= periodo_inicio:

        anio = periodo_inicio.year
        mes = str(periodo_inicio.month)
        if mes == 12:
            anio -= 1
        if len(mes) == 1:
            mes = '0' + mes
        periodo_inicio += relativedelta(months=1)
        periodos.append((str(anio), mes))
    return periodos


def cantidad_meses(d1, d2):
    format_str = '%Y-%m-%d'
    d1 = datetime.strptime(d1, format_str)
    d2 = datetime.strptime(d2, format_str)
    return (d1.year - d2.year) * 12 + d1.month - d2.month


def buscar_tabla_base(name, line):
    data = line.filtered(lambda x: x.name == name)
    if len(data) == 1:
        return 1
    else:
        return 0


def buscar_discapacidad(name, line):
    if line.abrev == name:
        return 1
    else:
        return 0


def buscar_jefe_hogar(line):
    rec = line.filtered(lambda x: not x.fecha_fin and x.es_jefe_hogar)
    if len(rec) == 1:
        return 1
    else:
        return 0


def convertir_str_date(string):
    format_str = '%Y-%m-%d'
    dia = datetime.strptime(string, format_str)
    return dia


def get_month_day_range(date):
    # Obtiene primer y ultimo dia de un mes
    date = datetime.strptime(date, '%Y-%m-%d')
    first_day = date.replace(day=1)
    last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
    return first_day, last_day


def verificar_fechas(obj, anio, mes):
    # Con esta funcion se valida que si una persona estuvo activo durante un determinado mes
    date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
    # primero obtengo el primer y ultimo dia del mes
    primer_dia, ultimo_dia = get_month_day_range(date_str)
    # convierto la fecha del obj a datetime
    fecha_inicio = convertir_str_date(obj.fecha_ingreso)
    if fecha_inicio and not obj.fecha_retiro:
        if fecha_inicio <= primer_dia:
            return True
        else:
            if fecha_inicio <= ultimo_dia:
                return True
            else:
                return False
    else:
        fecha_fin = convertir_str_date(obj.fecha_ingreso)
        if fecha_fin <= ultimo_dia:
            return True
        else:
            if ultimo_dia >= fecha_inicio >= primer_dia:
                return True
            else:
                return False


def obtener_todas_las_ut(self, servicio):
    uts = self.env['hr.department'].search([
        ('is_office', '=', True),
        ('estado', '=', 'activo'),
        ('services', 'in', ['ambos', servicio])
    ])
    return uts


class ActoresComunalesReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Actores Comunales')
        sheet.write(0, 0, 'ANIO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'CGREQ_UBIGEO', title_blue_format)
        sheet.write(0, 3, 'CG UNIDAD_TERRITORIAL_NUEVA', title_blue_format)
        sheet.write(0, 4, 'CORD_UNIDAD_TERRITORIAL', title_blue_format)
        sheet.write(0, 5, 'DEPARTAMENTO', title_blue_format)
        sheet.write(0, 6, 'PROVINCIA', title_blue_format)
        sheet.write(0, 7, 'DISTRITO', title_blue_format)
        sheet.write(0, 8, 'UT_PERSONA_ID', title_blue_format)
        sheet.write(0, 9, 'UT_PERSONA_NOM', title_blue_format)
        sheet.write(0, 10, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 11, 'NOMDEP', title_blue_format)
        sheet.write(0, 12, 'NOMPRO', title_blue_format)
        sheet.write(0, 13, 'NOMDIS', title_blue_format)
        sheet.write(0, 14, 'TCCPP_ID', title_blue_format)
        sheet.write(0, 15, 'CODCENPOB', title_blue_format)
        sheet.write(0, 16, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 17, 'CG_ID', title_blue_format)
        sheet.write(0, 18, 'CG_COD', title_blue_format)
        sheet.write(0, 19, 'CG_NOM', title_blue_format)
        sheet.write(0, 20, 'FECINIATE', title_blue_format)
        sheet.write(0, 21, 'FECCIEATE', title_blue_format)
        sheet.write(0, 22, 'CARGO_INTEGRANTE', title_blue_format)
        sheet.write(0, 23, 'ACTOR_COD', title_blue_format)
        sheet.write(0, 24, 'ACTOR_PATERNO', title_blue_format)
        sheet.write(0, 25, 'ACTOR_MATERNO', title_blue_format)
        sheet.write(0, 26, 'ACTOR_NOMBRE', title_blue_format)
        sheet.write(0, 27, 'ACTOR_FECNAC', title_blue_format)
        sheet.write(0, 28, 'TIPO_DOCUMENTO', title_blue_format)
        sheet.write(0, 29, 'NUMDOC', title_blue_format)
        sheet.write(0, 30, 'SEXO', title_blue_format)
        sheet.write(0, 31, 'ES_JEFE_HOGAR', title_blue_format)
        sheet.write(0, 32, 'LEE_ESCRIBE', title_blue_format)
        sheet.write(0, 33, 'DISC_VISUAL', title_blue_format)
        sheet.write(0, 34, 'DISC_OIR', title_blue_format)
        sheet.write(0, 35, 'DISC_HABLAR', title_blue_format)
        sheet.write(0, 36, 'DISC_EXTREMIDADES', title_blue_format)
        sheet.write(0, 37, 'DISC_MENTAL', title_blue_format)
        sheet.write(0, 38, 'NO_TIENE_DISCAPACIDAD', title_blue_format)
        sheet.write(0, 39, 'NRO_HIJOS', title_blue_format)
        sheet.write(0, 40, 'CUNAMAS', title_blue_format)
        sheet.write(0, 41, 'QALI_WARMA', title_blue_format)
        sheet.write(0, 42, 'FONCODES', title_blue_format)
        sheet.write(0, 43, 'JUNTOS', title_blue_format)
        sheet.write(0, 44, 'PENSION_65', title_blue_format)
        sheet.write(0, 45, 'OTRO', title_blue_format)
        sheet.write(0, 46, 'NO_BENEF_PROGRAMA', title_blue_format)
        sheet.write(0, 47, 'RELIGION', title_blue_format)
        sheet.write(0, 48, 'LENGUA_MATERNA', title_blue_format)
        sheet.write(0, 49, 'NIVEL_EDUCATIVO', title_blue_format)
        sheet.write(0, 50, 'ULT_ANIO_APROBADO', title_blue_format)
        sheet.write(0, 51, 'OCUPACION', title_blue_format)
        sheet.write(0, 52, 'ESTADO_CIVIL', title_blue_format)
        sheet.write(0, 53, 'IDTCENPOB', title_blue_format)
        sheet.write(0, 54, 'FECHA_INICIO', title_blue_format)
        sheet.write(0, 55, 'FECHA_FIN', title_blue_format)
        sheet.write(0, 56, 'FUNCIONO', title_blue_format)
        sheet.write(0, 57, 'REPORTE_FECHA', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)

        if uts:
            for ut in uts:
                for comite in ut.ut_comites_gestion:
                    if comite.state == 'activo':
                        for actor in comite.afiliaciones_ids:
                            sheet.write(row, 0, anio, body_format)
                            sheet.write(row, 1, mes, body_format)
                            sheet.write(row, 2, ut.id, body_format)
                            sheet.write(row, 3, ut.name, body_format)
                            sheet.write(row, 4, ut.name, body_format)
                            sheet.write(row, 5, comite.departamento_id.name, body_format)
                            sheet.write(row, 6, comite.provincia_id.name, body_format)
                            sheet.write(row, 7, comite.distrito_id.name, body_format)
                            sheet.write(row, 8, actor.id, body_format)
                            sheet.write(row, 9, actor.person_id.name, body_format)
                            sheet.write(row, 10, ut.id, body_format)
                            sheet.write(row, 11, comite.departamento_id.name, body_format)
                            sheet.write(row, 12, comite.provincia_id.name, body_format)
                            sheet.write(row, 13, comite.distrito_id.name, body_format)
                            sheet.write(row, 14, comite.centro_poblado_id.id, body_format)
                            sheet.write(row, 15, comite.centro_poblado_id.name, body_format)
                            sheet.write(row, 16, comite.centro_poblado_id.name, body_format)
                            sheet.write(row, 17, comite.id, body_format)
                            sheet.write(row, 18, comite.id, body_format)
                            sheet.write(row, 19, comite.name, body_format)
                            sheet.write(row, 20, comite.fecha_inicio, body_format)
                            sheet.write(row, 21, comite.fecha_cierre or '', body_format)
                            sheet.write(row, 22, actor.cargo, body_format)
                            sheet.write(row, 23, actor.id, body_format)
                            sheet.write(row, 24, actor.person_id.firstname, body_format)
                            sheet.write(row, 25, actor.person_id.secondname, body_format)
                            sheet.write(row, 26, actor.person_id.name, body_format)
                            sheet.write(row, 27, actor.person_id.birthdate, body_format)
                            sheet.write(row, 28, actor.person_id.type_document, body_format)
                            sheet.write(row, 29, actor.person_id.document_number, body_format)
                            sheet.write(row, 30, actor.person_id.gender, body_format)
                            sheet.write(row, 31, buscar_jefe_hogar(actor.person_id.integrantes_lines_ids), body_format)
                            sheet.write(row, 32, actor.person_id.sabe_leer_y_escribir, body_format)
                            sheet.write(
                                row, 33, buscar_discapacidad('DISC_VISUAL', actor.person_id.discapacidad_id),
                                body_format
                            )
                            sheet.write(
                                row, 34, buscar_discapacidad('DISC_OIR', actor.person_id.discapacidad_id),
                                body_format
                            )
                            sheet.write(
                                row, 35, buscar_discapacidad('DISC_HABLAR', actor.person_id.discapacidad_id),
                                body_format
                            )
                            sheet.write(
                                row, 36, buscar_discapacidad('DISC_EXTREMIDADES', actor.person_id.discapacidad_id),
                                body_format
                            )
                            sheet.write(
                                row, 37, buscar_discapacidad('DISC_MENTAL', actor.person_id.discapacidad_id),
                                body_format
                            )
                            sheet.write(row, 38, 'SI' if not actor.person_id.discapacidad_id else 'NO', body_format)
                            sheet.write(row, 39, actor.person_id.nro_hijos, body_format)
                            sheet.write(
                                row, 40, buscar_tabla_base(u'CunaMás', actor.person_id.programa_social_ids), body_format
                            )
                            sheet.write(
                                row, 41, buscar_tabla_base('Qali Warma', actor.person_id.programa_social_ids),
                                body_format
                            )
                            sheet.write(
                                row, 42, buscar_tabla_base('Foncodes', actor.person_id.programa_social_ids), body_format
                            )
                            sheet.write(row, 43, buscar_tabla_base('Juntos', actor.person_id.programa_social_ids),
                                        body_format)
                            sheet.write(
                                row, 44, buscar_tabla_base(u'Pensión', actor.person_id.programa_social_ids), body_format
                            )
                            sheet.write(row, 45, buscar_tabla_base('Otro', actor.person_id.programa_social_ids),
                                        body_format)
                            sheet.write(row, 46, buscar_tabla_base('Ninguno', actor.person_id.programa_social_ids),
                                        body_format)
                            sheet.write(row, 47, actor.person_id.religion_id.name, body_format)
                            sheet.write(row, 48, actor.person_id.lengua_materna_id.name, body_format)
                            sheet.write(row, 49, actor.person_id.nivel_educativo_id.name, body_format)
                            sheet.write(row, 50, actor.person_id.ultimo_grado_apr_id.name, body_format)
                            sheet.write(row, 51, actor.person_id.ocupacion_id.name, body_format)
                            sheet.write(row, 52, actor.person_id.marital_status, body_format)
                            sheet.write(row, 53, comite.centro_poblado_id.id, body_format)
                            sheet.write(row, 54, actor.fecha_ingreso, body_format)
                            sheet.write(row, 55, actor.fecha_retiro or '', body_format)
                            sheet.write(row, 56, 0 if actor.fecha_retiro else 1, body_format)
                            sheet.write(row, 57, lines.fecha_creacion, body_format)
                            row += 1
        else:
            raise ValidationError('No se encontraron Unidades Territoriales.')


ActoresComunalesReportXls('report.base_cuna.ActoresComunalesReportXls.xlsx', 'dynamic.report.wizard')


class ApoyoAdministrativoReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Apoyo Administrativo')
        sheet.set_column('C:C', 25)
        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM UNIDAD_TERRITORIAL', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'ID_PERSONA', title_blue_format)
        sheet.write(0, 9, 'APEPATERNO', title_blue_format)
        sheet.write(0, 10, 'APEMATERNO', title_blue_format)
        sheet.write(0, 11, 'NOMBRE', title_blue_format)
        sheet.write(0, 12, 'FECNAC', title_blue_format)
        sheet.write(0, 13, 'NUMDOC', title_blue_format)
        sheet.write(0, 14, 'NUM_DIAS_LABORADOS', title_blue_format)
        sheet.write(0, 15, 'MONTO', title_blue_format)
        sheet.write(0, 16, 'TIPDOC', title_blue_format)
        sheet.write(0, 17, 'ESTCIV', title_blue_format)
        sheet.write(0, 18, 'NIVEL EDUCATIVO', title_blue_format)
        sheet.write(0, 19, 'SEGURO', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if uts:
            for ut in uts:
                for comite in ut.ut_comites_gestion:
                    if comite.state == 'activo':
                        apoyo_adm = comite.afiliaciones_ids.filtered(
                            lambda x: x.cargo == 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
                        )
                        for actor in apoyo_adm:
                            just = self.env['justificacion.actor.lines'].search([
                                ('comite_gestion_id', '=', comite.id),
                                ('anio', '=', lines.anio.id),
                                ('mes', '=', lines.mes),
                                ('afiliacion_id', '=', actor.id)
                            ])
                            if just:
                                sheet.write(row, 0, anio, body_format)
                                sheet.write(row, 1, mes, body_format)
                                sheet.write(row, 2, ut.name, body_format)
                                sheet.write(row, 3, comite.departamento_id.name, body_format)
                                sheet.write(row, 4, comite.provincia_id.name, body_format)
                                sheet.write(row, 5, comite.distrito_id.name, body_format)
                                sheet.write(row, 6, ut.id, body_format)
                                sheet.write(row, 7, comite.name, body_format)
                                sheet.write(row, 8, actor.person_id.id, body_format)
                                sheet.write(row, 9, actor.person_id.firstname, body_format)
                                sheet.write(row, 10, actor.person_id.secondname, body_format)
                                sheet.write(row, 11, actor.person_id.name, body_format)
                                sheet.write(row, 12, actor.person_id.birthdate, body_format)
                                sheet.write(row, 13, actor.person_id.document_number, body_format)
                                sheet.write(row, 14, just.dias_trabajados, body_format)
                                sheet.write(row, 15, just.monto, body_format)
                                sheet.write(row, 16, actor.person_id.type_document, body_format)
                                sheet.write(row, 17, actor.person_id.marital_status, body_format)
                                sheet.write(row, 18, actor.person_id.nivel_educativo_id.name, body_format)
                                sheet.write(row, 19, actor.person_id.seguro_id.name, body_format)
                                row += 1
        else:
            raise ValidationError('No se encontraron Unidadades Territoriales.')


ApoyoAdministrativoReportXls('report.base_cuna.ApoyoAdministrativoReportXls.xlsx', 'dynamic.report.wizard')


class PlanillaVigenteReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Planilla Vigente')
        sheet.set_column('C:C', 25)
        sheet.write(0, 0, 'MES', title_blue_format)
        sheet.write(0, 1, u'AÑO', title_blue_format)
        sheet.write(0, 2, 'NOM UNIDAD_TERRITORIAL', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'ID_PERSONA', title_blue_format)
        sheet.write(0, 9, 'APEPATERNO', title_blue_format)
        sheet.write(0, 10, 'APEMATERNO', title_blue_format)
        sheet.write(0, 11, 'NOMBRE', title_blue_format)
        sheet.write(0, 12, 'FECNAC', title_blue_format)
        sheet.write(0, 13, 'NUMDOC', title_blue_format)
        sheet.write(0, 14, 'SEXO', title_blue_format)
        sheet.write(0, 15, 'NUM_DIAS_LABORADOS', title_blue_format)
        sheet.write(0, 16, 'MONTO', title_blue_format)
        sheet.write(0, 17, 'TIPDOC', title_blue_format)
        sheet.write(0, 18, 'ESTCIV', title_blue_format)
        sheet.write(0, 19, 'NIVEL EDUCATIVO', title_blue_format)
        sheet.write(0, 20, 'SEGURO', title_blue_format)
        sheet.write(0, 21, 'RENIEC', title_blue_format)
        sheet.write(0, 22, 'FECHA REPORTE', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1

        cargo = [
            'PRESIDENTE DE COMITE DE GESTION',
            'PRIMER VOCAL',
            'SEGUNDO VOCAL',
            'SECRETARIO DE COMITE DE GESTION',
            'TESORERO DE COMITE DE GESTION',
            'APOYO ADMINISTRATIVO DEL COMITE DE GESTION',
            'CONSEJO DE VIGILANCIA',
            'REPARTIDOR'
        ]
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontro Unidades Territoriales.')
        for ut in uts:
            for comite in uts.ut_comites_gestion:
                if comite.state == 'activo':
                    personas = comite.afiliaciones_ids.filtered(lambda x: x.cargo not in cargo)
                    for actor in personas:
                        just = self.env['justificacion.actor.lines'].search([
                            ('comite_gestion_id', '=', comite.id),
                            ('anio', '=', lines.anio.id),
                            ('mes', '=', lines.mes),
                            ('afiliacion_id', '=', actor.id)
                        ])
                        if just:
                            sheet.write(row, 0, mes, body_format)
                            sheet.write(row, 1, anio, body_format)
                            sheet.write(row, 2, ut.name, body_format)
                            sheet.write(row, 3, comite.departamento_id.name, body_format)
                            sheet.write(row, 4, comite.provincia_id.name, body_format)
                            sheet.write(row, 5, comite.distrito_id.name, body_format)
                            sheet.write(row, 6, ut.id, body_format)
                            sheet.write(row, 7, comite.name, body_format)
                            sheet.write(row, 8, actor.person_id.id, body_format)
                            sheet.write(row, 9, actor.person_id.firstname, body_format)
                            sheet.write(row, 10, actor.person_id.secondname, body_format)
                            sheet.write(row, 11, actor.person_id.name, body_format)
                            sheet.write(row, 12, actor.person_id.birthdate, body_format)
                            sheet.write(row, 13, actor.person_id.document_number, body_format)
                            sheet.write(row, 14, actor.person_id.gender.upper() or '', body_format)
                            sheet.write(row, 15, just.dias_trabajados, body_format)
                            sheet.write(row, 16, just.monto, body_format)
                            sheet.write(row, 17, actor.person_id.type_document.upper() or '', body_format)
                            sheet.write(row, 18, actor.person_id.marital_status, body_format)
                            sheet.write(row, 19, actor.person_id.nivel_educativo_id.name, body_format)
                            sheet.write(row, 20, actor.person_id.seguro_id.name, body_format)
                            sheet.write(row, 21, actor.person_id.validado_reniec, body_format)
                            sheet.write(row, 22, lines.fecha_creacion, body_format)
                            row += 1


PlanillaVigenteReportXls('report.base_cuna.PlanillaVigenteReportXls.xlsx', 'dynamic.report.wizard')


class JuntaDirectivaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Junta Directiva')
        sheet.set_column('C:C', 25)
        sheet.write(0, 0, 'MES', title_blue_format)
        sheet.write(0, 1, u'AÑO', title_blue_format)
        sheet.write(0, 2, 'UT', title_blue_format)
        sheet.write(0, 3, 'DEPARTAMENTO', title_blue_format)
        sheet.write(0, 4, 'PROVINCIA', title_blue_format)
        sheet.write(0, 5, 'DISTRITO', title_blue_format)
        sheet.write(0, 6, 'CG ID', title_blue_format)
        sheet.write(0, 7, 'CG_NOMBRE', title_blue_format)
        sheet.write(0, 8, 'ID_PERSONA', title_blue_format)
        sheet.write(0, 9, 'APEPATERNO', title_blue_format)
        sheet.write(0, 10, 'APEMATERNO', title_blue_format)
        sheet.write(0, 11, 'NOMBRES', title_blue_format)
        sheet.write(0, 12, 'SEXO', title_blue_format)
        sheet.write(0, 13, 'FECHA NAC', title_blue_format)
        sheet.write(0, 14, 'TIPO_OCUPACION', title_blue_format)
        sheet.write(0, 15, 'SABEE_LEER_ESCRIBIR', title_blue_format)
        sheet.write(0, 16, 'ULTIMO_GRADO', title_blue_format)
        sheet.write(0, 17, 'NUMDOC', title_blue_format)
        sheet.write(0, 18, 'USU_TIPODOC', title_blue_format)
        sheet.write(0, 19, 'ESTADO CIVIL', title_blue_format)
        sheet.write(0, 20, 'CG_CARGO_PERSONA', title_blue_format)
        sheet.write(0, 21, 'FECHA INICIO', title_blue_format)
        sheet.write(0, 22, 'FECHA FIN', title_blue_format)
        sheet.write(0, 23, 'FECHA REPORTE', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1

        cargo = [
            'PRESIDENTE DE COMITE DE GESTION',
            'PRIMER VOCAL',
            'SEGUNDO VOCAL',
            'SECRETARIO DE COMITE DE GESTION',
            'TESORERO DE COMITE DE GESTION',
            'CONSEJO DE VIGILANCIA',
        ]
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for comite in ut.ut_comites_gestion:
                if comite.state == 'activo':
                    personas = comite.afiliaciones_ids.filtered(lambda x: x.cargo in cargo)
                    for actor in personas:
                        sheet.write(row, 0, mes, body_format)
                        sheet.write(row, 1, anio, body_format)
                        sheet.write(row, 2, ut.name, body_format)
                        sheet.write(row, 3, comite.departamento_id.name, body_format)
                        sheet.write(row, 4, comite.provincia_id.name, body_format)
                        sheet.write(row, 5, comite.distrito_id.name, body_format)
                        sheet.write(row, 6, comite.id, body_format)
                        sheet.write(row, 7, comite.name, body_format)
                        sheet.write(row, 8, actor.person_id.id, body_format)
                        sheet.write(row, 9, actor.person_id.firstname, body_format)
                        sheet.write(row, 10, actor.person_id.secondname, body_format)
                        sheet.write(row, 11, actor.person_id.name, body_format)
                        sheet.write(row, 12, actor.person_id.gender.upper() or '', body_format)
                        sheet.write(row, 13, actor.person_id.birthdate, body_format)
                        sheet.write(row, 14, actor.person_id.ocupacion_id.name, body_format)
                        sheet.write(row, 15, 'SI' if actor.person_id.sabe_leer_y_escribir else 'NO', body_format)
                        sheet.write(row, 16, actor.person_id.ultimo_grado_apr_id.name, body_format)
                        sheet.write(row, 17, actor.person_id.document_number, body_format)
                        sheet.write(row, 18, actor.person_id.type_document.upper() or '', body_format)
                        sheet.write(row, 19, actor.person_id.marital_status, body_format)
                        sheet.write(row, 20, actor.cargo, body_format)
                        sheet.write(row, 21, actor.fecha_ingreso, body_format)
                        sheet.write(row, 22, actor.fecha_retiro or '', body_format)
                        sheet.write(row, 23, lines.fecha_creacion, body_format)
                        row += 1


JuntaDirectivaReportXls('report.base_cuna.JuntaDirectivaReportXls.xlsx', 'dynamic.report.wizard')


class ListaLocalesReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Lista Locales')
        sheet.set_column('C:C', 25)
        sheet.set_column(24, 40)
        sheet.write(0, 0, 'MES', title_blue_format)
        sheet.write(0, 1, u'AÑO', title_blue_format)
        sheet.write(0, 2, 'UT', title_blue_format)
        sheet.write(0, 3, 'DEPCG', title_blue_format)
        sheet.write(0, 4, 'PROCG', title_blue_format)
        sheet.write(0, 5, 'DISCG', title_blue_format)
        sheet.write(0, 6, 'CG CODIGO', title_blue_format)
        sheet.write(0, 7, 'CG NOMBRE', title_blue_format)
        sheet.write(0, 8, 'DIRCOM', title_blue_format)
        sheet.write(0, 9, 'REFDIR', title_blue_format)
        sheet.write(0, 10, 'UBIGEO_LOCAL', title_blue_format)
        sheet.write(0, 11, 'CODCENPOB', title_blue_format)
        sheet.write(0, 12, 'COD_CCPP_LOCAL', title_blue_format)
        sheet.write(0, 13, 'NOM_CCPP_LOCAL', title_blue_format)
        sheet.write(0, 14, 'DEP_LOCAL', title_blue_format)
        sheet.write(0, 15, 'PRO_LOCAL', title_blue_format)
        sheet.write(0, 16, 'DIS_LOCAL', title_blue_format)
        sheet.write(0, 17, 'CODIGOLOCAL', title_blue_format)
        sheet.write(0, 18, 'NOMBRELOCAL', title_blue_format)
        sheet.write(0, 19, 'TIPLOC', title_blue_format)
        sheet.write(0, 20, 'DIRLOC', title_blue_format)
        sheet.write(0, 21, 'REFDIR_1', title_blue_format)
        sheet.write(0, 22, 'LATITUD_INICIO', title_blue_format)
        sheet.write(0, 23, 'LONGITUD_INICIO', title_blue_format)
        sheet.write(0, 24, 'URL', title_blue_format)
        sheet.write(0, 25, 'CA_USU', title_blue_format)
        sheet.write(0, 26, 'FECHA_REGISTRO', title_blue_format)
        sheet.write(0, 27, 'TIESERALI', title_blue_format)

        reporte_fecha = datetime.now()
        anio = reporte_fecha.year
        mes = reporte_fecha.month
        row = 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for comite in ut.ut_comites_gestion:
                if comite.state == 'activo':
                    locales = comite.local_ids.filtered(lambda x: x.state == 'activacion')
                    for local in locales:
                        url = 'http://maps.google.com/maps?q=%f,%f&ll=%f,%f&z=12' % (
                            float(local.lat), float(local.lng), float(local.lat), float(local.lng)
                        )
                        cant_usuarios = 0
                        for rec in local.modulos_ids:
                            cant_usuarios += len(rec.ninios_ids)
                        sheet.write(row, 0, mes, body_format)
                        sheet.write(row, 1, anio, body_format)
                        sheet.write(row, 2, ut.name, body_format)
                        sheet.write(row, 3, comite.departamento_id.name, body_format)
                        sheet.write(row, 4, comite.provincia_id.name, body_format)
                        sheet.write(row, 5, comite.distrito_id.name, body_format)
                        sheet.write(row, 6, comite.id, body_format)
                        sheet.write(row, 7, comite.name, body_format)
                        sheet.write(row, 8, comite.direccion or '', body_format)
                        sheet.write(row, 9, comite.referencia or '', body_format)
                        sheet.write(row, 10, comite.codigo, body_format)
                        sheet.write(row, 11, comite.centro_poblado_id.code or '', body_format)
                        sheet.write(row, 12, local.centro_poblado_id.code or '', body_format)
                        sheet.write(row, 13, local.centro_poblado_id.name or '', body_format)
                        sheet.write(row, 14, local.departamento_id.name, body_format)
                        sheet.write(row, 15, local.provincia_id.name, body_format)
                        sheet.write(row, 16, local.distrito_id.name, body_format)
                        sheet.write(row, 17, local.code, body_format)
                        sheet.write(row, 18, local.name, body_format)
                        sheet.write(row, 19, local.tipo_local, body_format)
                        sheet.write(row, 20, local.direccion or '', body_format)
                        sheet.write(row, 21, local.referencia or '', body_format)
                        sheet.write(row, 22, local.lat, body_format)
                        sheet.write(row, 23, local.lng, body_format)
                        sheet.write(row, 24, url, body_format)
                        sheet.write(row, 25, cant_usuarios, body_format)
                        sheet.write(row, 26, lines.fecha_creacion, body_format)
                        sheet.write(row, 27, len(local.sa_ids), body_format)
                        row += 1


ListaLocalesReportXls('report.base_cuna.ListaLocalesReportXls.xlsx', 'dynamic.report.wizard')


class LocalesInfraestructuraReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Locales-Infraestructura')
        sheet.set_column('C:C', 25)
        sheet.set_column('E:E', 30)
        sheet.set_column('O:Q', 20)
        sheet.set_column('AC:AI', 30)

        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'IDTCOM', title_blue_format)
        sheet.write(0, 2, 'CG NOMBRE', title_blue_format)
        sheet.write(0, 3, 'ID LOCAL', title_blue_format)
        sheet.write(0, 4, 'NOMLOC', title_blue_format)
        sheet.write(0, 5, 'ESTADO', title_blue_format)
        sheet.write(0, 6, 'CG CODIGO', title_blue_format)
        sheet.write(0, 7, 'DIRLOC', title_blue_format)
        sheet.write(0, 8, 'NUMTEL', title_blue_format)
        sheet.write(0, 9, 'PROAPEPAT', title_blue_format)
        sheet.write(0, 10, 'PROAPEMAT', title_blue_format)
        sheet.write(0, 11, 'PRONOMBRE', title_blue_format)
        sheet.write(0, 12, 'PRODNI', title_blue_format)
        sheet.write(0, 13, 'TIPO_LOCAL', title_blue_format)
        sheet.write(0, 14, 'TIENE_SERV_ALI', title_blue_format)
        sheet.write(0, 15, 'AREA_CONSTRUIDA', title_blue_format)
        sheet.write(0, 16, 'AREA_TOTAL_TERRENO', title_blue_format)
        sheet.write(0, 17, 'IDTCENPOB', title_blue_format)
        sheet.write(0, 18, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 19, 'NUMERO_SALAS', title_blue_format)
        sheet.write(0, 20, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 21, 'IDTUBI', title_blue_format)
        sheet.write(0, 22, 'NOMDIS', title_blue_format)
        sheet.write(0, 23, 'NOMDEP', title_blue_format)
        sheet.write(0, 24, 'NOMPRO', title_blue_format)
        sheet.write(0, 25, 'TIPO_HOGAR', title_blue_format)
        sheet.write(0, 26, 'MOD_INTERVENCION', title_blue_format)
        sheet.write(0, 27, 'AREA_CEDIDA', title_blue_format)
        sheet.write(0, 28, u'AÑO_CONSRUCCION', title_blue_format)
        sheet.write(0, 29, 'LINDERO_MEDIDA_POR_FRENTE', title_blue_format)
        sheet.write(0, 30, 'LINDERO_MEDIDA_POR_DERECHA', title_blue_format)
        sheet.write(0, 31, 'LINDERO_MEDIDA_POR_IZQUIERDA', title_blue_format)
        sheet.write(0, 32, 'LINDERO_MEDIDA_POR_FONDO', title_blue_format)
        sheet.write(0, 33, 'POSIB_AMP_AREA_CONSTR', title_blue_format)
        sheet.write(0, 34, 'CUENTA_CONVENIO_ACTA', title_blue_format)
        sheet.write(0, 35, 'FECINICONV', title_blue_format)
        sheet.write(0, 36, 'FECFINCONV', title_blue_format)

        row = 1
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            for comite in ut.ut_comites_gestion:
                if comite.state == 'activo':
                    for local in comite.local_ids:
                        sheet.write(row, 0, ut.name, body_format)
                        sheet.write(row, 1, comite.id, body_format)
                        sheet.write(row, 2, comite.name, body_format)
                        sheet.write(row, 3, local.id, body_format)
                        sheet.write(row, 4, local.name, body_format)
                        sheet.write(row, 5, local.state.upper() or '', body_format)
                        sheet.write(row, 6, comite.codigo, body_format)
                        sheet.write(row, 7, local.direccion or '', body_format)
                        sheet.write(row, 8, local.nro_telefono or '', body_format)
                        sheet.write(row, 9, local.propietario_id.firstname or '', body_format)
                        sheet.write(row, 10, local.propietario_id.secondname or '', body_format)
                        sheet.write(row, 11, local.propietario_id.name or '', body_format)
                        sheet.write(row, 12, local.propietario_id.document_number, body_format)
                        sheet.write(row, 13, local.tipo_local.upper() or '', body_format)
                        sheet.write(row, 14, 'SI' if local.sa_ids else 'NO', body_format)
                        sheet.write(row, 15, local.area_construida, body_format)
                        sheet.write(row, 16, local.area_total_terreno, body_format)
                        sheet.write(row, 17, local.centro_poblado_id.id or '', body_format)
                        sheet.write(row, 18, local.centro_poblado_id.name or '', body_format)
                        sheet.write(row, 19, len(local.salas_ids), body_format)
                        sheet.write(row, 20, local.centro_poblado_id.code, body_format)
                        sheet.write(row, 21, local.distrito_id.id, body_format)
                        sheet.write(row, 22, local.distrito_id.name, body_format)
                        sheet.write(row, 23, local.departamento_id.name, body_format)
                        sheet.write(row, 24, local.provincia_id.name, body_format)
                        sheet.write(row, 25, local.procedencia.upper() or '', body_format)
                        sheet.write(row, 26, local.modalidad_intervencion.upper() or '', body_format)
                        sheet.write(row, 27, local.area_cedida, body_format)
                        sheet.write(row, 28, local.anio_construccion, body_format)
                        sheet.write(row, 29, local.lindero_frente, body_format)
                        sheet.write(row, 30, local.lindero_derecha, body_format)
                        sheet.write(row, 31, local.lindero_izquierda, body_format)
                        sheet.write(row, 32, local.lindero_fondo, body_format)
                        sheet.write(row, 33, local.posibilid_ampliacion, body_format)
                        convenio = local.convenios.filtered(lambda x: x.state == 'activo')
                        sheet.write(row, 34, 'SI' if convenio else 'NO', body_format)
                        sheet.write(row, 35, convenio.fecha_inicio if convenio else '', body_format)
                        sheet.write(row, 36, convenio.fecha_fin if convenio else '', body_format)
                        row += 1


LocalesInfraestructuraReportXls('report.base_cuna.LocalesInfraestructuraReportXls.xlsx', 'dynamic.report.wizard')


class RequerimientosUTReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Requerimientos x UT')
        sheet.set_column('C:C', 25)

        sheet.write(0, 0, 'ANIO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'UT', title_blue_format)
        sheet.write(0, 3, 'CAN CG', title_blue_format)
        sheet.write(0, 4, 'CG SIN GEN', title_blue_format)
        sheet.write(0, 5, 'CG GENE', title_blue_format)
        sheet.write(0, 6, 'CON SCAN', title_blue_format)
        sheet.write(0, 7, 'SIN SCAN', title_blue_format)

        row = 1

        mes = lines.mes
        anio = lines.anio

        uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            cgs = ut.ut_comites_gestion.filtered(lambda x: x.state != 'cerrado')
            cg_gene = cg_sin_gene = con_scan = sin_scan = 0
            for cg in cgs:
                reqs = cg.requerimientos_ids.filtered(lambda x: x.anio == anio and x.mes == mes and x.servicio == 'scd')
                if reqs:
                    for req in reqs:
                        if req.estado == 'borrador':
                            cg_sin_gene += 1
                        elif req.estado == 'desembolso':
                            cg_gene += 1
                            sin_scan += 1
                        else:
                            cg_gene += 1
                            con_scan += 1
                else:
                    cg_sin_gene += 1

            sheet.write(row, 0, anio.name, body_format)
            sheet.write(row, 1, mes, body_format)
            sheet.write(row, 2, ut.name, body_format)
            sheet.write(row, 3, len(cgs), body_format)
            sheet.write(row, 4, cg_sin_gene, body_format)
            sheet.write(row, 5, cg_gene, body_format)
            sheet.write(row, 6, con_scan, body_format)
            sheet.write(row, 7, sin_scan, body_format)
            row += 1


RequerimientosUTReportXls('report.base_cuna.RequerimientosUTReportXls.xlsx', 'dynamic.report.wizard')


class Ficha9ReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        total_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 7,
            'bg_color': '#53a8ba',
            'border': True,
        })

        sheet = workbook.add_worksheet('Resumen')

        sheet.set_column('B:L', 35)
        sheet.write(0, 0, 'N°', title_blue_format)
        sheet.write(0, 1, 'UT', title_blue_format)
        sheet.write(0, 2, 'Alerta N° Niños registrados en más de 1 ficha 9', title_blue_format)
        sheet.write(0, 3, 'Alerta N° niños registrados por ficha que superan 8', title_blue_format)
        sheet.write(0, 4, 'N° de Niños menores de 6 meses', title_blue_format)
        sheet.write(0, 5, 'Niños mayores a 37 meses', title_blue_format)
        sheet.write(0, 6, 'N° de Niño con  0 días de asistencia y más de 10 días enfermo', title_blue_format)
        sheet.write(0, 7, 'N° de Niños con  0 días de asistencia y 0 días enfermo', title_blue_format)
        sheet.write(0, 8, "='data'!BL2", title_blue_format)
        sheet.write(0, 9, "='data'!BM2", title_blue_format)
        sheet.write(0, 10, 'Registros con al menos una observación', title_blue_format)
        sheet.write(0, 11, 'Cantidad de Observaciones', title_blue_format)

        row = 1

        uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        mes = lines.mes
        anio = lines.anio

        for ut in uts:
            sheet.write(row, 0, row, body_format)
            sheet.write(row, 1, ut.name, body_format)
            sheet.write(row, 2, "=SUMIFS('data'!BF:BF,'data'!B:B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 3, "=SUMIFS('data'!BG:BG,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 4, "=SUMIFS('data'!BH:BH,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 5, "=SUMIFS('data'!BI:BI,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 6, "=SUMIFS('data'!BJ:BJ,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 7, "=SUMIFS('data'!BK:BK,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 8, "=SUMIFS('data'!BL:Bl,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 9, "=SUMIFS('data'!BM:BM,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 10, "=SUMIFS('data'!BN:BN,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            sheet.write(row, 11, "=SUMIFS('data'!BO:BO,'data'!$B:$B,'Resumen'!B" + str(row + 1) + ')', body_format)
            row += 1
        # Pagina 2
        sheet2 = workbook.add_worksheet('data')
        sheet2.set_column('BB:BQ', 35)
        sheet2.set_column('P:P', 30)
        sheet2.write(1, 0, 'FICHA ID', title_blue_format)
        sheet2.write(1, 1, 'UT', title_blue_format)
        sheet2.write(1, 2, 'NOMDEP', title_blue_format)
        sheet2.write(1, 3, 'NOMPRO', title_blue_format)
        sheet2.write(1, 4, 'NOMDIS', title_blue_format)
        sheet2.write(1, 5, 'IDCOM', title_blue_format)
        sheet2.write(1, 6, 'NOMCOM', title_blue_format)
        sheet2.write(1, 7, 'AT_CODIGO', title_blue_format)
        sheet2.write(1, 8, 'AT_PATERNO', title_blue_format)
        sheet2.write(1, 9, 'AT_MATERNO', title_blue_format)
        sheet2.write(1, 10, 'AT_NOMBRES', title_blue_format)
        sheet2.write(1, 11, 'NOMDEPLOC', title_blue_format)
        sheet2.write(1, 12, 'NOMPROLOC', title_blue_format)
        sheet2.write(1, 13, 'NOMDISLOC', title_blue_format)
        sheet2.write(1, 14, 'LOCAL ID', title_blue_format)
        sheet2.write(1, 15, 'NOMLOCAL', title_blue_format)
        sheet2.write(1, 16, 'TIPOLOCAL', title_blue_format)
        sheet2.write(1, 17, 'ID SALA', title_blue_format)
        sheet2.write(1, 18, 'NOMSALA', title_blue_format)
        sheet2.write(1, 19, 'ASISTENCIA_CONTROL_ID', title_blue_format)
        sheet2.write(1, 20, 'CUIDADOR_CODIGO', title_blue_format)
        sheet2.write(1, 21, 'CUIDADOR_PATERNO', title_blue_format)
        sheet2.write(1, 22, 'CUIDADOR_MATERNO', title_blue_format)
        sheet2.write(1, 23, 'CUIDADOR_NOMBRES', title_blue_format)
        sheet2.write(1, 24, 'CUIDADOR_FECNAC', title_blue_format)
        sheet2.write(1, 25, 'CUIDADOR_NUMDOC', title_blue_format)
        sheet2.write(1, 26, 'APEPATUSU', title_blue_format)
        sheet2.write(1, 27, 'APEMATUSU', title_blue_format)
        sheet2.write(1, 28, 'NOMUSU', title_blue_format)
        sheet2.write(1, 29, 'SEXO', title_blue_format)
        sheet2.write(1, 30, 'FECNACUSU', title_blue_format)
        sheet2.write(1, 31, 'NUMDOCUSU', title_blue_format)
        sheet2.write(1, 32, 'USU_TIPDOC', title_blue_format)
        sheet2.write(1, 33, 'N_FALTA_ENFERMO_EDA', title_blue_format)
        sheet2.write(1, 34, 'N_FALTA_ENFERMO_IRA', title_blue_format)
        sheet2.write(1, 35, 'N_FALTA_ENFERMO_OTRO', title_blue_format)
        sheet2.write(1, 36, 'N_TOTAL_FALTA', title_blue_format)
        sheet2.write(1, 37, 'N_TOTAL_ENFERMO', title_blue_format)
        sheet2.write(1, 38, 'N TOTAL ASISTIO', title_blue_format)
        sheet2.write(1, 39, 'CONSUME_MMN', title_blue_format)
        sheet2.write(1, 40, 'TRAECRED', title_blue_format)
        sheet2.write(1, 41, 'TRAEMMN', title_blue_format)
        sheet2.write(1, 42, 'SESSOC', title_blue_format)
        sheet2.write(1, 43, 'PRALAVMAN', title_blue_format)
        sheet2.write(1, 44, 'PRAHIGBUC', title_blue_format)
        sheet2.write(1, 45, 'PRACUENTO', title_blue_format)
        sheet2.write(1, 46, 'PRAJUEGOS', title_blue_format)
        sheet2.write(1, 47, 'USUCRE', title_blue_format)
        sheet2.write(1, 48, 'USUMOD', title_blue_format)
        sheet2.write(1, 49, 'Mes', title_blue_format)
        sheet2.write(1, 50, u'Año', title_blue_format)
        sheet2.write(1, 51, 'Reporte', title_blue_format)
        sheet2.write(1, 52, 'EDAD', title_blue_format)
        sheet2.write(1, 53, 'Asistencia + ENFERMO + FALTA  por niño', title_blue_format)
        sheet2.write(1, 54, '(Asistencia + ENFERMO  por niño) por ficha', title_blue_format)
        sheet2.write(1, 55, 'N° Niños por ficha', title_blue_format)
        sheet2.write(1, 56, 'CONTAR USUARIOS', title_blue_format)
        sheet2.write(1, 57, 'Alerta N° Niños registrados en más de 1 ficha 9', title_blue_format)
        sheet2.write(1, 58, 'Alerta N° niños registrados por ficha que superan 8', title_blue_format)
        sheet2.write(1, 59, 'N° de Niños menores de 6 meses', title_blue_format)
        sheet2.write(1, 60, 'Niños mayores a 37 meses', title_blue_format)
        sheet2.write(1, 61, 'N° de Niño con  0 días de asistencia y más de 10 días enfermo', title_blue_format)
        sheet2.write(1, 62, 'N° de Niños con  0 días de asistencia y 0 días enfermo', title_blue_format)
        sheet2.write(1, 63,
                     '="Suma de días de (Asistencia + Enfermo+Falta) que supera los "&BD1&" días utiles del mes"',
                     title_blue_format
                     )
        sheet2.write(1, 64,
                     '="Ficha con total de niños multiplicado por el número de días que superarn "&(8*BD1)&" '
                     'atenciones(8 niños * "&$BD$1&" días utiles)"',
                     title_blue_format
                     )
        sheet2.write(1, 65, 'Registros con al menos una observación', title_blue_format)
        sheet2.write(1, 66, 'Cantidad de Observaciones', title_blue_format)
        sheet2.write(1, 67, 'FECHA_INGRESO', title_blue_format)

        sheet2.write(0, 54, u'DÍAS UTILES', title_blue_format)
        row1 = 2
        for cg in lines.unidad_territorial_id.ut_comites_gestion:
            asis_ids = self.env['asistencia.control'].search([
                ('comite_gestion_id', '=', cg.id),
                ('mes', '=', mes),
                ('anio', '=', anio.id),
            ])
            for asis in asis_ids:
                sheet2.write(0, 55, asis.dias_utiles, body_format)

                total_54 = sum(ninio.asiste + ninio.enfermedad for ninio in asis.asistencia_menor_ids)
                total_55 = len(asis.asistencia_menor_ids)
                for ninio in asis.asistencia_menor_ids:
                    asist_menor = self.env['asistencia.menor'].search([
                        ('mes', '=', mes),
                        ('anio', '=', anio.id),
                        ('ninio_afiliacion_id', '=', ninio.ninio_afiliacion_id.id)
                    ])
                    ninio_id = ninio.ninio_afiliacion_id.ninios_id.integrante_id
                    sheet2.write(row1, 0, asis.id, body_format)
                    sheet2.write(row1, 1, cg.unidad_territorial_id.name, body_format)
                    sheet2.write(row1, 2, cg.unidad_territorial_id.departamento_id.name, body_format)
                    sheet2.write(row1, 3, cg.unidad_territorial_id.provincia_id.name, body_format)
                    sheet2.write(row1, 4, cg.unidad_territorial_id.distrito_id.name, body_format)
                    sheet2.write(row1, 5, cg.id, body_format)
                    sheet2.write(row1, 6, cg.name, body_format)
                    sheet2.write(row1, 7, asis.acomp_tecnico_id.id, body_format)
                    sheet2.write(row1, 8, asis.acomp_tecnico_id.person_id.firstname, body_format)
                    sheet2.write(row1, 9, asis.acomp_tecnico_id.person_id.secondname, body_format)
                    sheet2.write(row1, 10, asis.acomp_tecnico_id.person_id.name, body_format)
                    sheet2.write(row1, 11, asis.local_id.departamento_id.name, body_format)
                    sheet2.write(row1, 12, asis.local_id.provincia_id.name, body_format)
                    sheet2.write(row1, 13, asis.local_id.distrito_id.name, body_format)
                    sheet2.write(row1, 14, asis.local_id.id, body_format)
                    sheet2.write(row1, 15, asis.local_id.name, body_format)
                    sheet2.write(row1, 16, asis.local_id.tipo_local.upper() or '', body_format)
                    sheet2.write(row1, 17, asis.sala_id.id, body_format)
                    sheet2.write(row1, 18, asis.sala_id.name, body_format)
                    sheet2.write(row1, 19, asis.id, body_format)
                    sheet2.write(row1, 20, asis.mad_cuidadora_id.id, body_format)
                    sheet2.write(row1, 21, asis.mad_cuidadora_id.person_id.firstname, body_format)
                    sheet2.write(row1, 22, asis.mad_cuidadora_id.person_id.secondname, body_format)
                    sheet2.write(row1, 23, asis.mad_cuidadora_id.person_id.name, body_format)
                    sheet2.write(row1, 24, asis.mad_cuidadora_id.person_id.birthdate, body_format)
                    sheet2.write(row1, 25, asis.mad_cuidadora_id.person_id.document_number, body_format)
                    sheet2.write(row1, 26, ninio_id.firstname, body_format)
                    sheet2.write(row1, 27, ninio_id.secondname, body_format)
                    sheet2.write(row1, 28, ninio_id.name, body_format)
                    sheet2.write(row1, 29, ninio_id.gender, body_format)
                    sheet2.write(row1, 30, ninio_id.birthdate, body_format)
                    sheet2.write(row1, 31, ninio_id.document_number, body_format)
                    sheet2.write(row1, 32, ninio_id.type_document.upper() or '', body_format)
                    sheet2.write(row1, 33, ninio.totfaleda, body_format)
                    sheet2.write(row1, 34, ninio.totfalira, body_format)
                    sheet2.write(row1, 35, ninio.totfalotr, body_format)
                    sheet2.write(row1, 36, ninio.faltas, body_format)
                    sheet2.write(row1, 37, ninio.enfermedad, body_format)
                    sheet2.write(row1, 38, ninio.asiste, body_format)
                    sheet2.write(row1, 39, ninio.totconmn, body_format)
                    sheet2.write(row1, 40, ninio.cred, body_format)
                    sheet2.write(row1, 41, ninio.mn, body_format)
                    sheet2.write(row1, 42, 1 if ninio.familia_participa else '', body_format)
                    sheet2.write(row1, 43, 1 if ninio.lavado_manos else '', body_format)
                    sheet2.write(row1, 44, 1 if ninio.higiene_bucal else '', body_format)
                    sheet2.write(row1, 45, 1 if ninio.cuenta_cuento else '', body_format)
                    sheet2.write(row1, 46, 1 if ninio.juega_ninios else '', body_format)
                    sheet2.write(row1, 47, asis.usuario_creador_id.name or '', body_format)
                    sheet2.write(row1, 48, asis.usuario_mod_id.name or '', body_format)
                    sheet2.write(row1, 49, mes, body_format)
                    sheet2.write(row1, 50, anio.name, body_format)
                    sheet2.write(row1, 51, asis.fecha_creacion, body_format)
                    sheet2.write(row1, 52, ninio_id.months, body_format)
                    sheet2.write(row1, 53, ninio.asiste + ninio.faltas + ninio.enfermedad, body_format)
                    sheet2.write(row1, 54, total_54, body_format)
                    sheet2.write(row1, 55, total_55, body_format)
                    sheet2.write(row1, 56, len(asist_menor), body_format)
                    sheet2.write(row1, 57, '=--(BE' + str(row1 + 1) + '>1)', body_format)
                    sheet2.write(row1, 58, '=--(BD' + str(row1 + 1) + '>8)', body_format)
                    sheet2.write(row1, 59, '=--(BA' + str(row1 + 1) + '<6)', body_format)
                    sheet2.write(row1, 60, '=--(BA' + str(row1 + 1) + '>=37)', body_format)
                    sheet2.write(row1, 61, '=--(AM' + str(row1 + 1) + '=0 & AL' + str(row1 + 1) + '>10)', body_format)
                    sheet2.write(row1, 62, '=--(AL' + str(row1 + 1) + '=0 & AM' + str(row1 + 1) + '=0)', body_format)
                    sheet2.write(row1, 63, '=--(BB' + str(row1 + 1) + '>$BD$1)', body_format)
                    sheet2.write(row1, 64, '=--(BC' + str(row1 + 1) + '>(8*$BD$1))', body_format)
                    sheet2.write(row1, 65, '=--(SUM(BH' + str(row1 + 1) + ':BM' + str(row1 + 1) + ')>0)', body_format)
                    sheet2.write(row1, 66, '=SUM(BH' + str(row1 + 1) + ':BM' + str(row1 + 1) + ')', body_format)
                    sheet2.write(row1, 67, lines.fecha_creacion, body_format)
                    row1 += 1
        sheet2.write(row1, 56, 'TOTAL', title_blue_format)
        sheet2.write(row1, 57, '=SUM(BF3:BF' + str(row1), total_blue_format)
        sheet2.write(row1, 58, '=SUM(BG3:BG' + str(row1), total_blue_format)
        sheet2.write(row1, 59, '=SUM(BH3:BH' + str(row1), total_blue_format)
        sheet2.write(row1, 60, '=SUM(BI3:BI' + str(row1), total_blue_format)
        sheet2.write(row1, 61, '=SUM(BJ3:BJ' + str(row1), total_blue_format)
        sheet2.write(row1, 62, '=SUM(BK3:BK' + str(row1), total_blue_format)
        sheet2.write(row1, 63, '=SUM(BL3:BL' + str(row1), total_blue_format)
        sheet2.write(row1, 64, '=SUM(BM3:BM' + str(row1), total_blue_format)
        sheet2.write(row1, 65, '=SUM(BN3:BN' + str(row1), total_blue_format)
        sheet2.write(row1, 66, '=SUM(BO3:BO' + str(row1), total_blue_format)


Ficha9ReportXls('report.base_cuna.Ficha9ReportXls.xlsx', 'dynamic.report.wizard')


class DatasetSCDXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Dataset SCD')
        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('M:O', 35)

        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'NOMCOM', title_blue_format)
        sheet.write(0, 6, 'UBIGEO LOCAL', title_blue_format)
        sheet.write(0, 7, 'NOMDEPLOC', title_blue_format)
        sheet.write(0, 8, "NOMPROLOC", title_blue_format)
        sheet.write(0, 9, "NOMDISLOC", title_blue_format)
        sheet.write(0, 10, 'CCPP_LOCAL', title_blue_format)
        sheet.write(0, 11, 'NOM_CCPP', title_blue_format)
        sheet.write(0, 12, 'NOMLOC', title_blue_format)
        sheet.write(0, 13, 'APEPATUSU', title_blue_format)
        sheet.write(0, 14, 'NOMUSU', title_blue_format)
        sheet.write(0, 15, 'SEXO', title_blue_format)
        sheet.write(0, 16, 'FECNACUSU', title_blue_format)
        sheet.write(0, 17, 'EDAD_USU', title_blue_format)

        row = 1
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                for local in cg.local_ids:
                    for modulo in local.modulos_ids:
                        for ninio in modulo.ninios_ids:
                            sheet.write(row, 0, ut.name, body_format)
                            sheet.write(row, 1, ut.distrito_id.code, body_format)
                            sheet.write(row, 2, ut.departamento_id.name, body_format)
                            sheet.write(row, 3, ut.provincia_id.name, body_format)
                            sheet.write(row, 4, ut.distrito_id.name, body_format)
                            sheet.write(row, 5, cg.name, body_format)
                            sheet.write(row, 6, local.distrito_id.code, body_format)
                            sheet.write(row, 7, local.departamento_id.name, body_format)
                            sheet.write(row, 8, local.provincia_id.name, body_format)
                            sheet.write(row, 9, local.distrito_id.name, body_format)
                            sheet.write(row, 10, local.centro_poblado_id.code, body_format)
                            sheet.write(row, 11, local.centro_poblado_id.name, body_format)
                            sheet.write(row, 12, local.name, body_format)
                            sheet.write(row, 13, ninio.ninios_id.integrante_id.firstname, body_format)
                            sheet.write(row, 14, ninio.ninios_id.integrante_id.name, body_format)
                            sheet.write(row, 15, ninio.ninios_id.integrante_id.gender.upper() or '', body_format)
                            sheet.write(row, 16, ninio.ninios_id.integrante_id.birthdate, body_format)
                            sheet.write(row, 17, ninio.ninios_id.integrante_id.months, body_format)
                            row += 1


DatasetSCDXls('report.base_cuna.DatasetSCDXls.xlsx', 'dynamic.report.wizard')


class DatasetSAFXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Dataset SCD')
        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('I:L', 35)
        sheet.write(0, 0, 'NOMUT', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'NOMCOM', title_blue_format)
        sheet.write(0, 6, 'CODCENPOB', title_blue_format)
        sheet.write(0, 7, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 8, 'NOMBRE', title_blue_format)
        sheet.write(0, 9, 'APELLIDO PATERNO', title_blue_format)
        sheet.write(0, 10, 'SEXO', title_blue_format)
        sheet.write(0, 11, 'TIPO BENEFICIARIO', title_blue_format)
        sheet.write(0, 12, 'FECNACUSU', title_blue_format)
        sheet.write(0, 13, 'EDAD_USU', title_blue_format)
        row = 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            familias = self.env['unidad.familiar'].search([
                ('services', '=', 'saf'),
                ('state', '=', 'activo'),
                ('unidad_territorial_id', '=', ut.id),
            ])
            for fam in familias:
                integrantes = fam.integrantes_ids.filtered(
                    lambda x: x.integrante_id.madre_gestante or x.parentesco_id.name == u'Niño(a)'
                )
                for persona in integrantes:
                    sheet.write(row, 0, ut.name, body_format)
                    sheet.write(row, 1, ut.distrito_id.code, body_format)
                    sheet.write(row, 2, ut.departamento_id.name, body_format)
                    sheet.write(row, 3, ut.provincia_id.name, body_format)
                    sheet.write(row, 4, ut.distrito_id.name, body_format)
                    sheet.write(row, 5, fam.comite_gestion_id.name, body_format)
                    sheet.write(row, 6, fam.centro_poblado_id.code, body_format)
                    sheet.write(row, 7, fam.centro_poblado_id.name, body_format)
                    sheet.write(row, 8, persona.integrante_id.name, body_format)
                    sheet.write(row, 9, persona.integrante_id.firstname, body_format)
                    sheet.write(row, 10, persona.integrante_id.gender.upper() or '', body_format)
                    tipo = u'Niño(a)' if persona.parentesco_id.name == u'Niño(a)' else 'Madre Gestante'
                    sheet.write(row, 11, tipo, body_format)
                    sheet.write(row, 12, persona.integrante_id.birthdate, body_format)
                    sheet.write(row, 13, persona.integrante_id.months, body_format)
                    row += 1


DatasetSAFXls('report.base_cuna.DatasetSAFXls.xlsx', 'dynamic.report.wizard')


class ReporteFamiliaXls(ReportXlsx):

    def buscar_visitas_familia(self, integrantes, mes, anio):
        rec = integrantes.filtered(lambda x: x.parentesco_id.name == u'Niño(a)')
        if len(rec) > 0:
            for r in rec:
                ficha_rec = self.env['ficha.reconocimiento'].search([
                    ('gestante_id', '=', r.id),
                    ('visita_ids', '!=', False),
                    ('mes', '=', mes),
                    ('anio', '=', anio.id)
                ])
                if ficha_rec:
                    return 1
                ficha_fort = self.env['ficha.fortalecimiento'].search([
                    ('gestante_id', '=', r.id),
                    ('visita_ids', '!=', False),
                    ('mes', '=', mes),
                    ('anio', '=', anio.id)
                ])
                if ficha_fort:
                    return 1
        return 0

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Reporte Familia')

        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('I:L', 35)
        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'CODCENPOB', title_blue_format)
        sheet.write(0, 6, 'NOMCENPOB', title_blue_format)
        sheet.write(0, 7, 'DIR', title_blue_format)
        sheet.write(0, 8, 'REF', title_blue_format)
        sheet.write(0, 9, 'AT ID', title_blue_format)
        sheet.write(0, 10, 'AT PATERNO', title_blue_format)
        sheet.write(0, 11, 'AT MATERNO', title_blue_format)
        sheet.write(0, 12, 'AT NOMBRE', title_blue_format)
        sheet.write(0, 13, 'CG ID', title_blue_format)
        sheet.write(0, 14, 'CG CODCOM', title_blue_format)
        sheet.write(0, 15, 'CG NOMCOM', title_blue_format)
        sheet.write(0, 16, 'FAC ID', title_blue_format)
        sheet.write(0, 17, 'FAC PATERNO', title_blue_format)
        sheet.write(0, 18, 'FAC MATERNO', title_blue_format)
        sheet.write(0, 19, 'FAC NOMBRE', title_blue_format)
        sheet.write(0, 20, 'COD HOGAR', title_blue_format)
        sheet.write(0, 21, 'IDTPER', title_blue_format)
        sheet.write(0, 22, 'TIPO DOCUMENTO', title_blue_format)
        sheet.write(0, 23, 'N° DOC', title_blue_format)
        sheet.write(0, 24, 'VALIDADO RENIEC', title_blue_format)
        sheet.write(0, 25, 'NOMBRE', title_blue_format)
        sheet.write(0, 26, 'APEPAT', title_blue_format)
        sheet.write(0, 27, 'APEMAT', title_blue_format)
        sheet.write(0, 28, 'GENERO', title_blue_format)
        sheet.write(0, 29, 'MESES', title_blue_format)
        sheet.write(0, 30, 'FEC NAC', title_blue_format)
        sheet.write(0, 31, 'LEE ESCRIBE', title_blue_format)
        sheet.write(0, 32, 'ES JEFE HOGAR', title_blue_format)
        sheet.write(0, 33, 'ES CUID PRINCIPAL', title_blue_format)
        sheet.write(0, 34, 'PARENTESCO', title_blue_format)
        sheet.write(0, 35, 'ESTADO CIVIL', title_blue_format)
        sheet.write(0, 36, 'NIVEL EDUCATIVO', title_blue_format)
        sheet.write(0, 37, u'ULTIMO AÑO', title_blue_format)
        sheet.write(0, 38, 'SEGURO', title_blue_format)
        sheet.write(0, 39, u'SEGURO NÚMERO', title_blue_format)
        sheet.write(0, 40, 'OCUPACION', title_blue_format)
        sheet.write(0, 41, 'VISUAL', title_blue_format)
        sheet.write(0, 42, 'OIR', title_blue_format)
        sheet.write(0, 43, 'HABLAR', title_blue_format)
        sheet.write(0, 44, 'USAR_EXTREMIDADES', title_blue_format)
        sheet.write(0, 45, 'MENTAL', title_blue_format)
        sheet.write(0, 46, 'NO_TIENE', title_blue_format)
        sheet.write(0, 47, 'NUMERO_HIJOS', title_blue_format)
        sheet.write(0, 48, 'CUNAMAS', title_blue_format)
        sheet.write(0, 49, 'QALI_WARMA', title_blue_format)
        sheet.write(0, 50, 'FONCODES', title_blue_format)
        sheet.write(0, 51, 'JUNTOS', title_blue_format)
        sheet.write(0, 52, 'PENSION_65', title_blue_format)
        sheet.write(0, 53, 'OTRO', title_blue_format)
        sheet.write(0, 54, 'NINGUNO', title_blue_format)
        sheet.write(0, 55, 'RELIGION', title_blue_format)
        sheet.write(0, 56, 'LENGUA_MATERNA', title_blue_format)
        sheet.write(0, 57, 'EDAD_ANIOS', title_blue_format)
        sheet.write(0, 58, 'EDAD_MESES', title_blue_format)
        sheet.write(0, 59, 'NINIO_CON_VISITA', title_blue_format)
        sheet.write(0, 60, 'FAMILIA_CON_ASISTENCIA', title_blue_format)
        sheet.write(0, 61, 'FAM_NIN_CON_VISITA', title_blue_format)

        row = 1
        mes = lines.mes
        anio = lines.anio
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            familias = self.env['unidad.familiar'].search([
                ('services', '=', 'saf'),
                ('state', '=', 'activo'),
                ('unidad_territorial_id', '=', ut.id),
            ])
            for fam in familias:
                visitas = self.buscar_visitas_familia(fam.integrantes_ids, mes, anio)
                socializacion = self.env['socializacion.lineas'].search([
                    ('familia_id', '=', fam.id),
                    ('mes', '=', mes),
                    ('anio', '=', anio.id)
                ])
                for persona in fam.integrantes_ids:
                    sheet.write(row, 0, ut.name, body_format)
                    sheet.write(row, 1, ut.distrito_id.code, body_format)
                    sheet.write(row, 2, ut.departamento_id.name, body_format)
                    sheet.write(row, 3, ut.provincia_id.name, body_format)
                    sheet.write(row, 4, ut.distrito_id.name, body_format)
                    sheet.write(row, 5, fam.centro_poblado_id.code, body_format)
                    sheet.write(row, 6, fam.centro_poblado_id.name, body_format)
                    sheet.write(row, 7, ut.direccion, body_format)
                    sheet.write(row, 8, ut.referencia, body_format)
                    at = fam.comite_gestion_id.miembros_equipo.filtered(
                        lambda x: x.cargo == u'ACOMPAÑANTE TECNICO' and not x.fecha_fin
                    )
                    sheet.write(row, 9, at.id, body_format)
                    sheet.write(row, 10, at.person_id.firstname, body_format)
                    sheet.write(row, 11, at.person_id.secondname, body_format)
                    sheet.write(row, 12, at.person_id.name, body_format)
                    sheet.write(row, 13, fam.comite_gestion_id.id, body_format)
                    sheet.write(row, 14, fam.comite_gestion_id.codigo, body_format)
                    sheet.write(row, 15, fam.comite_gestion_id.name, body_format)
                    sheet.write(row, 16, fam.facilitador_id.id, body_format)
                    sheet.write(row, 17, fam.facilitador_id.person_id.firstname, body_format)
                    sheet.write(row, 18, fam.facilitador_id.person_id.secondname, body_format)
                    sheet.write(row, 19, fam.facilitador_id.person_id.name, body_format)
                    sheet.write(row, 20, fam.id, body_format)
                    sheet.write(row, 21, persona.integrante_id.codigo_integrante or '', body_format)
                    sheet.write(row, 22, persona.integrante_id.type_document.upper() or '', body_format)
                    sheet.write(row, 23, persona.integrante_id.document_number, body_format)
                    sheet.write(row, 24, persona.integrante_id.validado_reniec, body_format)
                    sheet.write(row, 25, persona.integrante_id.name, body_format)
                    sheet.write(row, 26, persona.integrante_id.firstname, body_format)
                    sheet.write(row, 27, persona.integrante_id.secondname, body_format)
                    sheet.write(row, 28, persona.integrante_id.gender.upper() or '', body_format)
                    sheet.write(row, 29, persona.integrante_id.months, body_format)
                    sheet.write(row, 30, persona.integrante_id.birthdate, body_format)
                    sheet.write(row, 31, persona.integrante_id.sabe_leer_y_escribir, body_format)
                    sheet.write(row, 32, 'SI' if persona.es_jefe_hogar else 'NO', body_format)
                    sheet.write(row, 33, 'SI' if persona.es_cuidador_principal else 'NO', body_format)
                    sheet.write(row, 34, persona.parentesco_id.name.upper() or '', body_format)
                    sheet.write(row, 35, persona.integrante_id.marital_status, body_format)
                    sheet.write(row, 36, persona.integrante_id.nivel_educativo_id.name.upper() or '', body_format)
                    sheet.write(row, 37, persona.integrante_id.ultimo_grado_apr_id.name.upper() or '', body_format)
                    sheet.write(row, 38, persona.integrante_id.seguro_id.name.upper() or '', body_format)
                    sheet.write(row, 39, persona.integrante_id.codigo_seguro, body_format)
                    sheet.write(row, 40, persona.integrante_id.ocupacion_id.name.upper() or '', body_format)
                    discapacidad = persona.integrante_id.discapacidad_id
                    sheet.write(row, 41, buscar_discapacidad('DISC_VISUAL', discapacidad), body_format)
                    sheet.write(row, 42, buscar_discapacidad('DISC_OIR', discapacidad), body_format)
                    sheet.write(row, 43, buscar_discapacidad('DISC_HABLAR', discapacidad), body_format)
                    sheet.write(row, 44, buscar_discapacidad('DISC_EXTREMIDADES', discapacidad), body_format)
                    sheet.write(row, 45, buscar_discapacidad('DISC_MENTAL', discapacidad), body_format)
                    sheet.write(row, 46, 'SI' if not discapacidad else 'NO', body_format)
                    sheet.write(row, 47, persona.integrante_id.nro_hijos, body_format)
                    programa_social = persona.integrante_id.programa_social_ids
                    sheet.write(row, 48, buscar_tabla_base(u'CunaMás', programa_social), body_format)
                    sheet.write(row, 49, buscar_tabla_base('Qali Warma', programa_social), body_format)
                    sheet.write(row, 50, buscar_tabla_base('Foncodes', programa_social), body_format)
                    sheet.write(row, 51, buscar_tabla_base('Juntos', programa_social), body_format)
                    sheet.write(row, 52, buscar_tabla_base(u'Pensión', programa_social), body_format)
                    sheet.write(row, 53, buscar_tabla_base('Otro', programa_social), body_format)
                    sheet.write(row, 54, buscar_tabla_base('Ninguno', programa_social), body_format)
                    sheet.write(row, 55, persona.integrante_id.religion_id.name.upper() or '', body_format)
                    sheet.write(row, 56, persona.integrante_id.lengua_materna_id.name.upper() or '', body_format)
                    sheet.write(row, 57, persona.integrante_id.anios, body_format)
                    sheet.write(row, 58, persona.integrante_id.months, body_format)
                    sheet.write(row, 59, visitas, body_format)
                    sheet.write(row, 60, 1 if socializacion else 0, body_format)
                    sheet.write(row, 61, visitas, body_format)
                    row += 1


ReporteFamiliaXls('report.base_cuna.ReporteFamiliaXls.xlsx', 'dynamic.report.wizard')


class TramaCGXls(ReportXlsx):

    def validar_cg_asistencia(self, cg, anio):
        meses = []
        if cg.fichas_rec_ids:
            x = cg.fichas_rec_ids.filtered(lambda x: x.anio == anio)
            meses += x.mapped(lambda x: int(x.mes))
        elif cg.fichas_fort_ids:
            x = cg.fichas_fort_ids.filtered(lambda x: x.anio == anio)
            meses += x.mapped(lambda x: int(x.mes))
        elif cg.ficha_gestante_ids:
            x = cg.fichas_fort_ids.filtered(lambda x: x.anio == anio)
            meses += x.mapped(lambda x: int(x.mes))
        if meses and len(set(meses)) == 12:
            return True
        return False

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Trama Comite Gestion')
        sheet.set_column('A:A', 35)
        sheet.set_column('F:F', 35)
        sheet.set_column('I:L', 35)
        sheet.write(0, 0, 'CG ID', title_blue_format)
        sheet.write(0, 1, 'COMITE CODIGO', title_blue_format)
        sheet.write(0, 2, 'NOMBRE COMITE', title_blue_format)

        row = 1
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                if cg.servicio != 'scd' and self.validar_cg_asistencia(cg, lines.anio):
                    sheet.write(row, 0, cg.id, body_format)
                    sheet.write(row, 1, cg.codigo, body_format)
                    sheet.write(row, 2, cg.name, body_format)
                    row += 1


TramaCGXls('report.base_cuna.TramaCGXls.xlsx', 'dynamic.report.wizard')


class TramaFacilitadorXls(ReportXlsx):

    def validar_facilitador_por_anio(self, arr_fac, anio):
        nuevo_arr = []
        for intergrante in arr_fac:
            if (intergrante.fecha_ingreso and convertir_str_date(intergrante.fecha_ingreso).year == int(anio.name)) or \
                    (intergrante.fecha_retiro and convertir_str_date(intergrante.fecha_retiro).year == int(anio.name)):
                nuevo_arr.append(intergrante)
        return nuevo_arr

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Trama Facilitadores')
        sheet.write(0, 0, 'FAC ID', title_blue_format)
        sheet.write(0, 1, 'NOMBRE FACILITADOR', title_blue_format)

        row = 1
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                if cg.servicio != 'scd':
                    facilitadores = cg.afiliaciones_ids.filtered(lambda x: x.tipo_guia.name == 'FACILITADOR')
                    facilitadores = self.validar_facilitador_por_anio(facilitadores, lines.anio)
                    if facilitadores:
                        for fac in facilitadores:
                            nombre = '{} {} {}'.format(
                                fac.person_id.name, fac.person_id.firstname, fac.person_id.secondname)
                            sheet.write(row, 0, fac.id, body_format)
                            sheet.write(row, 1, nombre, body_format)
                            row += 1


TramaFacilitadorXls('report.base_cuna.TramaFacilitadorXls.xlsx', 'dynamic.report.wizard')


class TramaUsuariosXls(ReportXlsx):

    def validar_asistencia(self, arr_int, anio):
        nuevo_arr = []
        flag = False
        for intergrante in arr_int:
            if intergrante.fichas_rec_ids.filtered(lambda x: x.anio == anio):
                flag = True
            elif intergrante.fichas_fort_ids.filtered(lambda x: x.anio == anio):
                flag = True
            elif intergrante.ficha_gestante_ids.filtered(lambda x: x.anio == anio):
                flag = True
            if flag:
                nuevo_arr.append(intergrante)
        return nuevo_arr

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Trama Usuarios')
        sheet.write(0, 0, 'USUARIO ID', title_blue_format)
        sheet.write(0, 1, 'USUARIO TIPO', title_blue_format)

        row = 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            familias = self.env['unidad.familiar'].search([
                ('services', '=', 'saf'),
                ('state', '=', 'activo'),
                ('unidad_territorial_id', '=', ut.id),
            ])
            for fam in familias:
                integrantes = fam.integrantes_ids.filtered(
                    lambda x: x.integrante_id.madre_gestante or x.parentesco_id.name == u'Niño(a)')
                integrantes = self.validar_asistencia(integrantes, lines.anio)
                if integrantes:
                    for persona in integrantes:
                        sheet.write(row, 0, persona.id, body_format)
                        tipo = u'Niño(a)' if persona.parentesco_id.name == u'Niño(a)' else 'Madre Gestante'
                        sheet.write(row, 1, tipo, body_format)
                        row += 1


TramaUsuariosXls('report.base_cuna.TramaUsuariosXls.xlsx', 'dynamic.report.wizard')


class SesionesSocializacionXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('Sesiones Socialización')

        sheet.write(0, 0, 'UT', title_blue_format)
        sheet.write(0, 1, 'DEPARTAMENTO', title_blue_format)
        sheet.write(0, 2, 'PROVINCIA', title_blue_format)
        sheet.write(0, 3, 'DISTRITO', title_blue_format)
        sheet.write(0, 4, 'ID CG', title_blue_format)
        sheet.write(0, 5, 'COD CG', title_blue_format)
        sheet.write(0, 6, 'CG', title_blue_format)
        sheet.write(0, 7, 'COD FAMILIA', title_blue_format)
        sheet.write(0, 8, 'COD CUIDADOR PRINCIPAL', title_blue_format)
        sheet.write(0, 9, 'NOMBRE CUIDADOR PRINCIPAL', title_blue_format)
        sheet.write(0, 10, 'COD FACILITADOR VISITA', title_blue_format)
        sheet.write(0, 11, 'FACILITADOR VISITA', title_blue_format)
        sheet.write(0, 12, 'FECHA DE LA SESION SOCIALIZACION', title_blue_format)
        sheet.write(0, 13, u'AÑO DE EJECUCION', title_blue_format)
        sheet.write(0, 14, 'MES DE EJECUCION', title_blue_format)
        sheet.write(0, 15, 'USUARIO QUE REALIZA EL REGISTRO', title_blue_format)
        sheet.write(0, 16, 'CARGO DE PERSONA QUE REALIZA REGISTRO', title_blue_format)
        sheet.write(0, 17, 'FECHA DE REGISTRO', title_blue_format)
        sheet.write(0, 18, 'Reporte Fecha', title_blue_format)

        row = 1
        anio = lines.anio
        mes = lines.mes

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            lineas = self.env['socializacion.lineas'].search([
                ('unidad_territorial_id', '=', ut.id),
                ('anio', '=', anio.id),
                ('mes', '=', mes),
            ])
            sheet.set_column('I:Q', 35)
            for sesion in lineas:
                sheet.write(row, 0, ut.name, body_format)
                sheet.write(row, 1, ut.departamento_id.name, body_format)
                sheet.write(row, 2, ut.provincia_id.name, body_format)
                sheet.write(row, 3, ut.distrito_id.name, body_format)
                sheet.write(row, 4, sesion.familia_id.comite_gestion_id.id, body_format)
                sheet.write(row, 5, sesion.familia_id.comite_gestion_id.codigo, body_format)
                sheet.write(row, 6, sesion.familia_id.comite_gestion_id.name, body_format)
                sheet.write(row, 7, sesion.familia_id.id, body_format)
                cuid = sesion.familia_id.integrantes_ids.filtered(lambda x: x.es_cuidador_principal)
                sheet.write(row, 8, cuid.id if cuid else '', body_format)
                sheet.write(row, 9, cuid.integrante_id.name if cuid else '', body_format)
                sheet.write(row, 10, sesion.familia_id.facilitador_id.id, body_format)
                sheet.write(row, 11, sesion.familia_id.facilitador_id.person_id.name, body_format)
                sheet.write(row, 12, sesion.ficha_id.fecha_sesion, body_format)
                sheet.write(row, 13, anio.name, body_format)
                sheet.write(row, 14, mes, body_format)
                cargo = ''
                if sesion.ficha_id.usuario_creador_id:
                    cargo = sesion.ficha_id.usuario_creador_id.partner_id.tipo_persona.name
                sheet.write(row, 15, sesion.ficha_id.usuario_creador_id.name, body_format)
                sheet.write(row, 16, cargo, body_format)
                sheet.write(row, 17, sesion.ficha_id.fecha_creacion, body_format)
                sheet.write(row, 18, lines.fecha_creacion, body_format)

                row += 1


SesionesSocializacionXls('report.base_cuna.SesionesSocializacionXls.xlsx', 'dynamic.report.wizard')


class LongitudinalSafXls(ReportXlsx):

    def buscar_fichas(self, persona, anio, mes):
        anio = self.env['tabla.anios'].search([
            ('name', '=', anio)
        ])
        fichas = self.env['survey.user_input'].search([
            ('persona_ficha_id', '=', persona.id),
            ('mes', '=', mes),
            ('anio', '=', anio.id)
        ])
        if len(fichas) == 0:
            return ''
        else:
            return len(fichas)

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 3

        sheet = workbook.add_worksheet('Longitudinal Saf')
        periodos = validar_periodos(lines.periodo_inicio, lines.periodo_fin)

        sheet.write(2, 0, 'USUARIO_ID', title_blue_format)
        sheet.write(2, 1, 'TPER_PATERNO', title_blue_format)
        sheet.write(2, 2, 'TPER_MATERNO', title_blue_format)
        sheet.write(2, 3, 'TPER_NOMBRES', title_blue_format)
        sheet.write(2, 4, 'TPER_FECHANAC', title_blue_format)
        sheet.write(2, 5, 'TPER_SEXO', title_blue_format)
        sheet.write(2, 6, 'TPER_TIPODOC', title_blue_format)
        sheet.write(2, 7, 'TPER_NUMDOC', title_blue_format)
        sheet.write(0, 8, 'VISITAS', title_blue_format)
        sheet.write(1, 8, 'USUARIOS', title_blue_format)
        sheet.write(2, 8, 'USUARIO', title_blue_format)

        i = 9
        for periodo in periodos:
            sheet.write(2, i, '%s%s_VISITA_TOTAL' % (periodo[0], periodo[1]), title_blue_format)
            i += 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            familias = self.env['unidad.familiar'].search([
                ('services', '=', 'saf'),
                ('state', '=', 'activo'),
                ('unidad_territorial_id', '=', ut.id),
            ])
            sheet.set_column('B:H', 20)
            sheet.set_column('I:I', 30)
            for fam in familias:
                integrantes = fam.integrantes_ids.filtered(
                    lambda x: x.integrante_id.madre_gestante or x.parentesco_id.name == u'Niño(a)'
                )
                for persona in integrantes:
                    sheet.write(row, 0, persona.id, body_format)
                    sheet.write(row, 1, persona.integrante_id.firstname, body_format)
                    sheet.write(row, 2, persona.integrante_id.secondname, body_format)
                    sheet.write(row, 3, persona.integrante_id.name, body_format)
                    sheet.write(row, 4, persona.integrante_id.birthdate, body_format)
                    sheet.write(row, 5, persona.integrante_id.gender.upper() or '', body_format)
                    sheet.write(row, 6, persona.integrante_id.type_document, body_format)
                    sheet.write(row, 7, persona.integrante_id.document_number, body_format)
                    tipo = u'Niño(a)' if persona.parentesco_id.name == u'Niño(a)' else 'Madre Gestante'
                    sheet.write(row, 8, tipo, body_format)
                    i = 9

                    for periodo in periodos:
                        cell_range = xl_range(3, i, 4, i)
                        sheet.write(row, i, self.buscar_fichas(persona, periodo[0], periodo[1]), body_format)
                        sheet.write(0, i, '=SUBTOTAL(%d,%s)' % (9, cell_range), body_format)
                        sheet.write(1, i, '=SUMPRODUCT((%s<>"")*1)' % cell_range, body_format)
                        sheet.set_column(row, i, 30)
                        i += 1
                    row += 1


LongitudinalSafXls('report.base_cuna.LongitudinalSafXls.xlsx', 'dynamic.report.wizard')


class LongitudinalSCDXls(ReportXlsx):

    def buscar_asistencia(self, persona, anio, mes):
        anio = self.env['tabla.anios'].search([
            ('name', '=', anio)
        ])
        asis = self.env['asistencia.menor'].search([
            ('afi_ninio_id', '=', persona.id),
            ('mes', '=', mes),
            ('anio', '=', anio.id)
        ])
        if len(asis) <= 0:
            return ''
        else:
            asiste = sum(line.asiste for line in asis)
            return asiste

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 2

        anio = lines.anio
        sheet = workbook.add_worksheet('Longitudinal SCD')
        periodos = validar_periodos(lines.periodo_inicio, lines.periodo_fin)

        sheet.write(1, 0, 'CODIGO DE NIÑO', title_blue_format)
        sheet.write(1, 1, 'TPER_PATERNO', title_blue_format)
        sheet.write(1, 2, 'TPER_MATERNO', title_blue_format)
        sheet.write(1, 3, 'TPER_NOMBRES', title_blue_format)
        sheet.write(1, 4, 'TPER_FECHANAC', title_blue_format)
        sheet.write(1, 5, 'TPER_SEXO', title_blue_format)
        sheet.write(1, 6, 'TIPO DOCUMENTO', title_blue_format)
        sheet.write(0, 7, 'Cantidad de niños que asistieron', title_blue_format)
        sheet.write(1, 7, 'NUMERO DE DOCUMENTO', title_blue_format)
        i = 8
        for periodo in periodos:
            sheet.write(1, i, '%s%s ASISTIO' % (periodo[0], periodo[1]), title_blue_format)
            i += 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            familias = self.env['unidad.familiar'].search([
                ('services', '=', 'scd'),
                ('state', '=', 'activo'),
                ('unidad_territorial_id', '=', ut.id),
            ])
            sheet.set_column('B:H', 20)
            sheet.set_column('I:U', 30)
            for fam in familias:
                integrantes = fam.integrantes_ids.filtered(lambda x: x.parentesco_id.name == u'Niño(a)')
                for persona in integrantes:
                    sheet.write(row, 0, persona.id, body_format)
                    sheet.write(row, 1, persona.integrante_id.firstname, body_format)
                    sheet.write(row, 2, persona.integrante_id.secondname, body_format)
                    sheet.write(row, 3, persona.integrante_id.name, body_format)
                    sheet.write(row, 4, persona.integrante_id.birthdate, body_format)
                    sheet.write(row, 5, persona.integrante_id.gender.upper() or '', body_format)
                    sheet.write(row, 6, persona.integrante_id.type_document.upper() or '', body_format)
                    sheet.write(row, 7, persona.integrante_id.document_number, body_format)
                    i = 8
                    for periodo in periodos:
                        sheet.write(row, i, self.buscar_asistencia(persona, periodo[0], periodo[1]), body_format)
                        i += 1
                    row += 1
            y = 8
            for periodo in periodos:
                cell_range = xl_range(2, y, row, y)
                sheet.write(0, y, '=SUMPRODUCT((%s<>"")*1)' % cell_range, body_format)
                sheet.set_column(row, y, 30)
                y += 1


LongitudinalSCDXls('report.base_cuna.LongitudinalSCDXls.xlsx', 'dynamic.report.wizard')


class RequerimientosCGReportXls(ReportXlsx):

    def buscar_asistencia(self, persona, anio, mes):
        asis = self.env['asistencia.menor'].search([
            ('afi_ninio_id', '=', persona.id),
            ('mes', '=', mes),
            ('anio', '=', anio.id)
        ])
        if len(asis) <= 0:
            return ''
        else:
            asiste = sum(line.asiste for line in asis)
            return asiste

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1

        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('Requerimientos CG')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOMDEP', title_blue_format)
        sheet.write(0, 3, 'NOMPRO', title_blue_format)
        sheet.write(0, 4, 'NOMDIS', title_blue_format)
        sheet.write(0, 5, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 6, 'UT', title_blue_format)
        sheet.write(0, 7, 'ID', title_blue_format)
        sheet.write(0, 8, 'NOMCOM', title_blue_format)
        sheet.write(0, 9, 'NUM_CIAI', title_blue_format)
        sheet.write(0, 10, 'NUM_HOGAR_FAMILIAR', title_blue_format)
        sheet.write(0, 11, 'NUM_COMUNALES', title_blue_format)
        sheet.write(0, 12, 'TOTAL_LOC', title_blue_format)
        sheet.write(0, 13, 'NUM_GUIA_FAMILIAR', title_blue_format)
        sheet.write(0, 14, 'ASIG_SUBMGFAM', title_blue_format)
        sheet.write(0, 15, 'NUM_MADRE_GUIA', title_blue_format)
        sheet.write(0, 16, 'NUM_MADRE_CUIDADORA_A', title_blue_format)
        sheet.write(0, 17, 'NUM_MADRE_CUIDADORA_B', title_blue_format)
        sheet.write(0, 18, 'TOTAL_CUI', title_blue_format)
        sheet.write(0, 19, 'NUM_SOCIA_COCINA', title_blue_format)
        sheet.write(0, 20, 'NUM_VIG_LIMPIEZA', title_blue_format)
        sheet.write(0, 21, 'NUM_APOYO_ADM', title_blue_format)
        sheet.write(0, 22, 'AMPLIACION_META', title_blue_format)
        sheet.write(0, 23, 'TOTAL_USU', title_blue_format)
        sheet.write(0, 24, 'NDIAS', title_blue_format)
        sheet.write(0, 25, 'MONTO_TOTAL', title_blue_format)
        sheet.write(0, 26, 'SALDO_ANTERIOR', title_blue_format)
        sheet.write(0, 27, 'DEPOSITO', title_blue_format)
        sheet.write(0, 28, 'TIENE_SCAN', title_blue_format)
        sheet.write(0, 29, 'REPORTE_FECHA', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                req = self.env['requerimiento.lines'].search([
                    ('servicio', '=', 'scd'),
                    ('comite_gestion_id', '=', cg.id),
                    ('unidad_territorial_id', '=', ut.id),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes),
                    ('estado', '!=', 'borrador')
                ])
                if req:
                    sheet.write(row, 0, anio.name, body_format)
                    sheet.write(row, 1, mes, body_format)
                    sheet.write(row, 2, ut.departamento_id.name, body_format)
                    sheet.write(row, 3, ut.provincia_id.name, body_format)
                    sheet.write(row, 4, ut.distrito_id.name, body_format)
                    sheet.write(row, 5, ut.distrito_id.code, body_format)
                    sheet.write(row, 6, ut.name, body_format)
                    sheet.write(row, 7, ut.id, body_format)
                    sheet.write(row, 8, cg.name, body_format)
                    ciais = cg.local_ids.filtered(lambda x: x.tipo_local in ['ciai', 'ambos'])
                    sheet.write(row, 9, len(ciais) if ciais else 0, body_format)
                    sheet.write(row, 10, len(cg.familias_ids), body_format)
                    actores_comunales = cg.afiliaciones_ids.filtered(lambda x: x.cargo in [
                        'PRESIDENTE DE COMITE DE GESTION',
                        'PRIMER VOCAL',
                        'SEGUNDO VOCAL',
                        'SECRETARIO DE COMITE DE GESTION',
                        'TESORERO DE COMITE DE GESTION',
                        'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
                    ])
                    sheet.write(row, 11, len(actores_comunales), body_format)
                    sheet.write(row, 12, len(cg.local_ids), body_format)

                    sheet.write(row, 13, req.desembolso_id.guias_familiares, body_format)
                    sheet.write(row, 14, req.desembolso_id.incentivo_mad_guia_base, body_format)
                    sheet.write(row, 15, req.desembolso_id.madres_guias, body_format)
                    sheet.write(row, 16, req.desembolso_id.mad_cuid_a, body_format)
                    sheet.write(row, 17, req.desembolso_id.mad_cuid_b, body_format)
                    sheet.write(row, 18, req.desembolso_id.mad_cuid_a + req.desembolso_id.mad_cuid_b, body_format)
                    sheet.write(row, 19, req.desembolso_id.socias_cocina, body_format)
                    sheet.write(row, 20, req.desembolso_id.nro_vigilantes, body_format)
                    sheet.write(row, 21, req.desembolso_id.apoyo_adm, body_format)
                    sheet.write(row, 22, req.desembolso_id.meta_cg, body_format)
                    sheet.write(row, 23, req.desembolso_id.nro_usuarios_atendidos, body_format)
                    sheet.write(row, 24, req.desembolso_id.dias_atender, body_format)
                    sheet.write(row, 25, req.desembolso_id.total_deposito, body_format)
                    sheet.write(row, 26, req.desembolso_id.saldo_anterior, body_format)
                    sheet.write(row, 27, req.desembolso_id.total_anterior, body_format)
                    sheet.write(row, 28, '1' if req.adjuntar_desembolso else '0', body_format)
                    sheet.write(row, 29, lines.fecha_creacion, body_format)
                    row += 1


RequerimientosCGReportXls('report.base_cuna.RequerimientosCGReportXls.xlsx', 'dynamic.report.wizard')


class SaldosCGReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('Saldos CG')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID_COMITE', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'SALDO_ALIMENTOS', title_blue_format)
        sheet.write(0, 9, 'SALDO_ACTORES', title_blue_format)
        sheet.write(0, 10, 'SALDO', title_blue_format)
        sheet.write(0, 11, 'ESTADO_F3', title_blue_format)
        sheet.write(0, 12, 'REPORTE_FECHA', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                gastos = self.env['justificacion.gastos'].search([
                    ('servicio', '=', 'scd'),
                    ('comite_gestion_id', '=', cg.id),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes)
                ])
                if gastos:
                    sheet.write(row, 0, anio.name, body_format)
                    sheet.write(row, 1, mes, body_format)
                    sheet.write(row, 2, ut.name, body_format)
                    sheet.write(row, 3, ut.departamento_id.name, body_format)
                    sheet.write(row, 4, ut.provincia_id.name, body_format)
                    sheet.write(row, 5, ut.distrito_id.name, body_format)
                    sheet.write(row, 6, cg.id, body_format)
                    sheet.write(row, 7, cg.name, body_format)
                    sheet.write(row, 8, gastos.atencion_alimentaria_recibido - gastos.alimento, body_format)
                    saldo_actores = gastos.incentivo_cuid_a_saldo + gastos.incentivo_cuid_b_saldo + \
                        gastos.incentivo_mad_guia_saldo + gastos.incentivo_guias_fam_saldo + \
                        gastos.incentivo_socio_cocina_saldo + gastos.incentivo_adm_saldo + \
                        gastos.incentivo_vigilancia_saldo
                    sheet.write(row, 9, saldo_actores, body_format)
                    sheet.write(row, 10, gastos.saldo, body_format)
                    sheet.write(row, 11, gastos.estado.upper() or '', body_format)
                    sheet.write(row, 12, lines.fecha_creacion, body_format)
                    row += 1


SaldosCGReportXls('report.base_cuna.SaldosCGReportXls.xlsx', 'dynamic.report.wizard')


class SociasSAPlanillaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1

        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('SOCIA SA PLANILLA')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM', title_blue_format)
        sheet.write(0, 3, 'NOMDEP', title_blue_format)
        sheet.write(0, 4, 'NOMPRO', title_blue_format)
        sheet.write(0, 5, 'NOMDIS', title_blue_format)
        sheet.write(0, 6, 'ID_COMITE', title_blue_format)
        sheet.write(0, 7, 'NOMCOM', title_blue_format)
        sheet.write(0, 8, 'IDTSERALI', title_blue_format)
        sheet.write(0, 9, 'NOMSERALI', title_blue_format)
        sheet.write(0, 10, 'TIPO_INTEGRANTE_COM', title_blue_format)
        sheet.write(0, 11, 'ID_INTEGRANTE_COM', title_blue_format)
        sheet.write(0, 12, 'APEPAT', title_blue_format)
        sheet.write(0, 13, 'APEMAT', title_blue_format)
        sheet.write(0, 14, 'NOMPER', title_blue_format)
        sheet.write(0, 15, 'FECNAC', title_blue_format)
        sheet.write(0, 16, 'NUMDOC', title_blue_format)
        sheet.write(0, 17, 'NUM_DIAS_LABORADOS', title_blue_format)
        sheet.write(0, 18, 'MONTO', title_blue_format)
        sheet.write(0, 19, 'ID_AFILIACION', title_blue_format)
        sheet.write(0, 20, 'FECFINSER', title_blue_format)
        sheet.write(0, 21, 'TIEALQ', title_blue_format)
        sheet.write(0, 22, 'FECHA_INICIO_ALQUILER', title_blue_format)
        sheet.write(0, 23, 'FECHA_FIN_ALQUILER', title_blue_format)
        sheet.write(0, 24, 'MONTO_ALQUILER', title_blue_format)
        sheet.write(0, 25, 'REPORTE_FECHA', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        tipo = self.env['tipo.persona'].search([('name', '=', 'SOCIO DE COCINA')])
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                for sa in cg.servicio_alimentario_ids.filtered(lambda x: x.state == 'activacion'):
                    for socia in sa.socias_cocina_ids:
                        gastos = self.env['justificacion.actor.lines'].search([
                            ('afiliacion_id', '=', socia.id),
                            ('comite_gestion_id', '=', cg.id),
                            ('tipo_persona', '=', tipo.id),
                            ('anio', '=', anio.id),
                            ('mes', '=', mes)
                        ])
                        sheet.write(row, 0, anio.name, body_format)
                        sheet.write(row, 1, mes, body_format)
                        sheet.write(row, 2, ut.name, body_format)
                        sheet.write(row, 3, ut.departamento_id.name, body_format)
                        sheet.write(row, 4, ut.provincia_id.name, body_format)
                        sheet.write(row, 5, ut.distrito_id.name, body_format)
                        sheet.write(row, 6, cg.id, body_format)
                        sheet.write(row, 7, cg.name, body_format)
                        sheet.write(row, 8, sa.id, body_format)
                        sheet.write(row, 9, sa.name, body_format)
                        sheet.write(row, 10, tipo.id, body_format)
                        sheet.write(row, 11, socia.id, body_format)
                        sheet.write(row, 12, socia.person_id.firstname, body_format)
                        sheet.write(row, 13, socia.person_id.secondname, body_format)
                        sheet.write(row, 14, socia.person_id.name, body_format)
                        sheet.write(row, 15, socia.person_id.birthdate, body_format)
                        sheet.write(row, 16, socia.person_id.document_number, body_format)
                        sheet.write(row, 17, gastos.dias_trabajados, body_format)
                        sheet.write(row, 18, gastos.monto, body_format)
                        sheet.write(row, 19, socia.id, body_format)
                        sheet.write(row, 20, sa.fecha_baja or '', body_format)
                        sheet.write(row, 21, 1 if sa.es_alquilado else 0, body_format)
                        sheet.write(row, 22, sa.fecha_inicio_alquiler or '', body_format)
                        sheet.write(row, 23, sa.fecha_fin_alquiler or '', body_format)
                        sheet.write(row, 24, sa.monto_alquiler or '', body_format)
                        sheet.write(row, 25, lines.fecha_creacion, body_format)
                        row += 1


SociasSAPlanillaReportXls('report.base_cuna.SociasSAPlanillaReportXls.xlsx', 'dynamic.report.wizard')


class GuiasPlanillaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1
        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('GUIAS PLANILLA')

        sheet.write(0, 0, u'AÑO', title_blue_format)
        sheet.write(0, 1, 'MES', title_blue_format)
        sheet.write(0, 2, 'NOM', title_blue_format)
        sheet.write(0, 3, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 4, 'NOMDEP', title_blue_format)
        sheet.write(0, 5, 'NOMPRO', title_blue_format)
        sheet.write(0, 6, 'NOMDIS', title_blue_format)
        sheet.write(0, 7, 'ID', title_blue_format)
        sheet.write(0, 8, 'NOMCOM', title_blue_format)
        sheet.write(0, 9, 'CODMADCUI', title_blue_format)
        sheet.write(0, 10, 'APEPAT', title_blue_format)
        sheet.write(0, 11, 'APEMAT', title_blue_format)
        sheet.write(0, 12, 'NOMPER', title_blue_format)
        sheet.write(0, 13, 'CATEGORIA', title_blue_format)
        sheet.write(0, 14, 'FECNAC', title_blue_format)
        sheet.write(0, 15, 'NUMDOC', title_blue_format)
        sheet.write(0, 16, 'TIPDOC', title_blue_format)
        sheet.write(0, 17, 'ESTCIV', title_blue_format)
        sheet.write(0, 18, 'DES', title_blue_format)
        sheet.write(0, 19, 'SEG', title_blue_format)
        sheet.write(0, 20, 'CAN_MESES', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        cargos = ['MADRE CUIDADORA', 'MADRE GUIA', 'GUIA FAMILIAR']
        tipo = self.env['tipo.persona'].search([('name', 'in', cargos)])

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            for cg in ut.ut_comites_gestion:
                gastos = self.env['justificacion.actor.lines'].search([
                    ('comite_gestion_id', '=', cg.id),
                    ('tipo_persona', 'in', tipo.ids),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes)
                ])
                for mc in gastos:
                    sheet.write(row, 0, anio.name, body_format)
                    sheet.write(row, 1, mes, body_format)
                    sheet.write(row, 2, ut.name, body_format)
                    sheet.write(row, 3, ut.distrito_id.code, body_format)
                    sheet.write(row, 4, ut.departamento_id.name, body_format)
                    sheet.write(row, 5, ut.provincia_id.name, body_format)
                    sheet.write(row, 6, ut.distrito_id.name, body_format)
                    sheet.write(row, 7, cg.id, body_format)
                    sheet.write(row, 8, cg.name, body_format)
                    sheet.write(row, 9, mc.afiliacion_id.id, body_format)
                    sheet.write(row, 10, mc.afiliacion_id.person_id.firstname, body_format)
                    sheet.write(row, 11, mc.afiliacion_id.person_id.secondname, body_format)
                    sheet.write(row, 12, mc.afiliacion_id.person_id.name, body_format)
                    sheet.write(row, 13, mc.afiliacion_id.tipo_guia.abrev, body_format)
                    sheet.write(row, 14, mc.afiliacion_id.person_id.birthdate, body_format)
                    sheet.write(row, 15, mc.afiliacion_id.person_id.document_number, body_format)
                    sheet.write(row, 16, mc.afiliacion_id.person_id.type_document, body_format)
                    sheet.write(row, 17, mc.afiliacion_id.person_id.marital_status, body_format)
                    sheet.write(row, 18, mc.afiliacion_id.person_id.nivel_educativo_id.name, body_format)
                    sheet.write(row, 19, mc.afiliacion_id.person_id.seguro_id.name, body_format)
                    sheet.write(row, 20, cantidad_meses(lines.fecha_creacion, mc.afiliacion_id.fecha_ingreso),
                                body_format)
                    row += 1


GuiasPlanillaReportXls('report.base_cuna.GuiasPlanillaReportXls.xlsx', 'dynamic.report.wizard')


class CuidadorasAsistenciaReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1

        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('CUIDADORAS ASISTENCIA')

        sheet.write(0, 0, 'NOM', title_blue_format)
        sheet.write(0, 1, 'CODUBIGEOCG', title_blue_format)
        sheet.write(0, 2, 'NOMDEPCG', title_blue_format)
        sheet.write(0, 3, 'NOMPROCG', title_blue_format)
        sheet.write(0, 4, 'NOMDISCG', title_blue_format)
        sheet.write(0, 5, 'IDCG', title_blue_format)
        sheet.write(0, 6, 'NOMCOM', title_blue_format)
        sheet.write(0, 7, 'CODUBIGEO', title_blue_format)
        sheet.write(0, 8, 'NOMDEP', title_blue_format)
        sheet.write(0, 9, 'NOMPRO', title_blue_format)
        sheet.write(0, 10, 'NOMDIS', title_blue_format)
        sheet.write(0, 11, 'IDLOC', title_blue_format)
        sheet.write(0, 12, 'NOMLOC', title_blue_format)
        sheet.write(0, 13, 'TIPLOC', title_blue_format)
        sheet.write(0, 14, 'IDTPRISALCUI', title_blue_format)
        sheet.write(0, 15, 'APEPAT', title_blue_format)
        sheet.write(0, 16, 'APEMAT', title_blue_format)
        sheet.write(0, 17, 'NOMPER', title_blue_format)
        sheet.write(0, 18, 'FECNAC', title_blue_format)
        sheet.write(0, 19, 'NUMDOC', title_blue_format)
        sheet.write(0, 20, 'IDTTIPSEX', title_blue_format)
        sheet.write(0, 21, 'EST_CIVIL', title_blue_format)
        sheet.write(0, 22, 'NIVEL_EDUCA', title_blue_format)
        sheet.write(0, 23, 'EDAD_CUI', title_blue_format)
        sheet.write(0, 24, 'CAN_MESES_PROG', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                for local in cg.local_ids:
                    for mad in local.madres_cuidadoras_ids:
                        date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
                        cant = cantidad_meses(date_str, mad.fecha_ingreso)
                        if cant >= 0:
                            sheet.write(row, 0, ut.name, body_format)
                            sheet.write(row, 1, cg.distrito_id.code, body_format)
                            sheet.write(row, 2, cg.departamento_id.name, body_format)
                            sheet.write(row, 3, cg.provincia_id.name, body_format)
                            sheet.write(row, 4, cg.distrito_id.name, body_format)
                            sheet.write(row, 5, cg.id, body_format)
                            sheet.write(row, 6, cg.name, body_format)
                            sheet.write(row, 7, local.distrito_id.code, body_format)
                            sheet.write(row, 8, local.departamento_id.name, body_format)
                            sheet.write(row, 9, local.provincia_id.name, body_format)
                            sheet.write(row, 10, local.distrito_id.name, body_format)
                            sheet.write(row, 11, local.id, body_format)
                            sheet.write(row, 12, local.name, body_format)
                            sheet.write(row, 13, local.tipo_local.upper() or '', body_format)
                            sheet.write(row, 14, mad.salas_id.id, body_format)
                            sheet.write(row, 15, mad.person_id.firstname, body_format)
                            sheet.write(row, 16, mad.person_id.secondname, body_format)
                            sheet.write(row, 17, mad.person_id.name, body_format)
                            sheet.write(row, 18, mad.person_id.birthdate, body_format)
                            sheet.write(row, 19, mad.person_id.document_number, body_format)
                            sheet.write(row, 20, mad.person_id.gender.upper() or '', body_format)
                            sheet.write(row, 21, mad.person_id.marital_status.upper() or '', body_format)
                            sheet.write(row, 22, mad.person_id.nivel_educativo_id.name.upper() or '', body_format)
                            sheet.write(row, 23, mad.person_id.age, body_format)
                            sheet.write(row, 24, cant, body_format)
                        row += 1


CuidadorasAsistenciaReportXls('report.base_cuna.CuidadorasAsistenciaReportXls.xlsx', 'dynamic.report.wizard')


class RNUReportXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 1

        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('RNU')

        sheet.write(0, 0, 'CO_PROGRAMA_SOCIAL', title_blue_format)
        sheet.write(0, 1, 'CO_GENERADO', title_blue_format)
        sheet.write(0, 2, 'TI_DOI_BENEFICIARIO', title_blue_format)
        sheet.write(0, 3, 'NU_DOI_BENEFICIARIO', title_blue_format)
        sheet.write(0, 4, u'AÑO', title_blue_format)
        sheet.write(0, 5, 'MES', title_blue_format)
        sheet.write(0, 6, 'CO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 7, 'AP_PATERNO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 8, 'AP_MATERNO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 9, 'NO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 10, 'TI_SEXO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 11, 'FE_NACIMIENTO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 12, 'UN_UBIGEO_BENEFICIARIO', title_blue_format)
        sheet.write(0, 13, 'DI_BENEFICIARIO', title_blue_format)
        sheet.write(0, 14, 'NU_LATITUD', title_blue_format)
        sheet.write(0, 15, 'NU_LONGITUD', title_blue_format)
        sheet.write(0, 16, 'CO_CENTRO_POBLADO', title_blue_format)
        sheet.write(0, 17, 'NO_CENTRO_POBLADO', title_blue_format)
        sheet.write(0, 18, 'ES_CIVIL_BENEFICIARIO', title_blue_format)
        sheet.write(0, 19, 'TI_TIENE_APODERADO', title_blue_format)
        sheet.write(0, 20, 'TI_DOI_APODERADO', title_blue_format)
        sheet.write(0, 21, 'NU_DOI_APODERADO', title_blue_format)
        sheet.write(0, 22, 'AP_PATERNO_APODERADO', title_blue_format)
        sheet.write(0, 23, 'AP_MATERNO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'NO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'TI_SEXO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'FE_NACIMIENTO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'NU_UBIGEO_APODERADO', title_blue_format)
        sheet.write(0, 24, 'TI_RELACION_BENEFICIARIO', title_blue_format)
        sheet.write(0, 24, 'ES_CIVIL_APODERADO', title_blue_format)
        sheet.write(0, 24, 'FE_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'TI_TRANSFERENCIA', title_blue_format)
        sheet.write(0, 24, 'TI_FINANCIAMIENTO', title_blue_format)
        sheet.write(0, 24, 'CO_CENTROAFILIACION', title_blue_format)
        sheet.write(0, 24, 'NO_CENTRO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'ES_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'NU_UBIGEO_CENTRO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'CO_CENTRO_POBLADO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'NO_CENTRO_POBLADO_AFILIACION', title_blue_format)
        sheet.write(0, 24, 'TI_DESAFILIACION', title_blue_format)
        sheet.write(0, 24, 'FE_DESAFILIACION', title_blue_format)

        sheet.set_column('A:H', 20)
        sheet.set_column('I:Z', 30)
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            for cg in ut.ut_comites_gestion:
                for local in cg.local_ids:
                    for mad in local.madres_cuidadoras_ids:
                        date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
                        cant = cantidad_meses(date_str, mad.fecha_ingreso)
                        if cant >= 0:
                            sheet.write(row, 0, ut.name, body_format)
                            sheet.write(row, 1, cg.distrito_id.code, body_format)
                            sheet.write(row, 2, cg.departamento_id.name, body_format)
                            sheet.write(row, 3, cg.provincia_id.name, body_format)
                            sheet.write(row, 4, cg.distrito_id.name, body_format)
                            sheet.write(row, 5, cg.id, body_format)
                            sheet.write(row, 6, cg.name, body_format)
                            sheet.write(row, 7, local.distrito_id.code, body_format)
                            sheet.write(row, 8, local.departamento_id.name, body_format)
                            sheet.write(row, 9, local.provincia_id.name, body_format)
                            sheet.write(row, 10, local.distrito_id.name, body_format)
                            sheet.write(row, 11, local.id, body_format)
                            sheet.write(row, 12, local.name, body_format)
                            sheet.write(row, 13, local.tipo_local.upper() or '', body_format)
                            sheet.write(row, 14, mad.salas_id.id, body_format)
                            sheet.write(row, 15, mad.person_id.firstname, body_format)
                            sheet.write(row, 16, mad.person_id.secondname, body_format)
                            sheet.write(row, 17, mad.person_id.name, body_format)
                            sheet.write(row, 18, mad.person_id.birthdate, body_format)
                            sheet.write(row, 19, mad.person_id.document_number, body_format)
                            sheet.write(row, 20, mad.person_id.gender.upper() or '', body_format)
                            sheet.write(row, 21, mad.person_id.marital_status.upper() or '', body_format)
                            sheet.write(row, 22, mad.person_id.nivel_educativo_id.name.upper() or '', body_format)
                            sheet.write(row, 23, mad.person_id.age, body_format)
                            sheet.write(row, 24, cant, body_format)
                        row += 1


RNUReportXls('report.base_cuna.RNUReportXls.xlsx', 'dynamic.report.wizard')


class FacilitadorasVisitasXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })
        row = 3

        anio = lines.anio
        mes = lines.mes
        sheet = workbook.add_worksheet('FACILITADORAS - VISITAS')

        sheet.merge_range('C2:G2', 'UBICACIÓN CG SEGÚN REQUERIMIENTOS', title_blue_format)
        sheet.merge_range('H2:M2', 'UBICACIÓN DE FACILITADORAS ACTUALMENTE', title_blue_format)
        sheet.merge_range('N2:P2', 'DATOS DEL COMITÉ DE GESTIÓN QUE REALIZÓ REQUERIMIENTO', title_blue_format)
        sheet.merge_range('Q2:X2', 'DATOS PERSONALES', title_blue_format)
        sheet.merge_range('Y2:AD2', 'DISCAPACIDAD', title_blue_format)
        sheet.merge_range('AE2:AK2', 'BENEFICIARIA DE ALGUN PROGRAMA SOCIAL', title_blue_format)
        sheet.merge_range('AL2:AT2', 'INFORMACIÓN COMPLEMENTARIA', title_blue_format)
        sheet.merge_range('Q1:AT1', 'DATOS DE LA FACILITADORA', title_blue_format)

        sheet.write(2, 0, u'AÑO', title_blue_format)
        sheet.write(2, 1, 'MES', title_blue_format)
        sheet.write(2, 2, 'CGREQ_UBIGEO', title_blue_format)
        sheet.write(2, 3, 'UNIDAD_TERRITORIAL_NUEVA', title_blue_format)
        sheet.write(2, 4, 'DEPARTAMENTO', title_blue_format)
        sheet.write(2, 5, 'PROVINCIA', title_blue_format)
        sheet.write(2, 6, 'DISTRITO', title_blue_format)

        sheet.write(2, 7, 'FAC_ACTUAL_UBI', title_blue_format)
        sheet.write(2, 8, 'FAC_ACTUAL_DEP', title_blue_format)
        sheet.write(2, 9, 'FAC_ACTUAL_PROV', title_blue_format)
        sheet.write(2, 10, 'FAC_ACTUAL_DIST', title_blue_format)
        sheet.write(2, 11, 'FAC_ACTUAL_CCPP', title_blue_format)
        sheet.write(2, 12, 'FAC_ACTUAL_NOMCEPOB', title_blue_format)

        sheet.write(2, 13, 'CG_ID', title_blue_format)
        sheet.write(2, 14, 'CODCOM', title_blue_format)
        sheet.write(2, 15, 'COMITE_GESTION', title_blue_format)

        sheet.write(2, 16, 'FAC_ID_VIS', title_blue_format)
        sheet.write(2, 17, 'FAC_PATERNO', title_blue_format)
        sheet.write(2, 18, 'FAC_MATERNO', title_blue_format)
        sheet.write(2, 19, 'FAC_NOMBRES', title_blue_format)
        sheet.write(2, 20, 'FAC_NACIMIENTO', title_blue_format)
        sheet.write(2, 21, 'TIPO_DOCUMENTO', title_blue_format)
        sheet.write(2, 22, 'NUMDOC', title_blue_format)
        sheet.write(2, 23, 'FAC_SEXO', title_blue_format)

        sheet.write(2, 24, 'DISC_VISUAL', title_blue_format)
        sheet.write(2, 25, 'DISC_OIR', title_blue_format)
        sheet.write(2, 26, 'DISC_HABLAR', title_blue_format)
        sheet.write(2, 27, 'DISC_EXTREMIDADES', title_blue_format)
        sheet.write(2, 28, 'DISC_MENTAL', title_blue_format)
        sheet.write(2, 29, 'NO_TIENE_DISCAPACIDAD', title_blue_format)

        sheet.write(2, 30, 'CUNAMAS', title_blue_format)
        sheet.write(2, 31, 'QALI_WARMA', title_blue_format)
        sheet.write(2, 32, 'FONCODES', title_blue_format)
        sheet.write(2, 33, 'JUNTOS', title_blue_format)
        sheet.write(2, 34, 'PENSION_65', title_blue_format)
        sheet.write(2, 35, 'OTRO', title_blue_format)
        sheet.write(2, 36, 'NO_BENEF_PROGRAMA', title_blue_format)

        sheet.write(2, 37, 'ESTADO_CIVIL', title_blue_format)
        sheet.write(2, 38, 'ES_JEFE_HOGAR', title_blue_format)
        sheet.write(2, 39, 'LEE_ESCRIBE', title_blue_format)
        sheet.write(2, 40, 'NRO_HIJOS', title_blue_format)
        sheet.write(2, 41, 'RELIGION', title_blue_format)
        sheet.write(2, 42, 'LENGUA_MATERNA', title_blue_format)
        sheet.write(2, 43, 'NIVEL_EDUCATIVO', title_blue_format)
        sheet.write(2, 44, 'ULT_ANIO_APROBADO', title_blue_format)
        sheet.write(2, 45, 'OCUPACION', title_blue_format)

        sheet.write(2, 46, 'FECHA_REPORTE', title_blue_format)

        sheet.set_column('B:H', 20)
        sheet.set_column('I:U', 30)
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')
        for ut in uts:
            for cg in ut.ut_comites_gestion:
                facilitadores = cg.afiliaciones_ids.filtered(lambda x: x.cargo == 'FACILITADOR')
                req = cg.requerimientos_ids.filtered(
                    lambda x: x.anio == anio and x.mes == mes and x.servicio == 'saf' and x.comite_gestion_id == cg
                )
                if req:
                    for fac in facilitadores:
                        validado = verificar_fechas(fac, anio, mes)  # valida facilitador activo dentro del mes/año
                        if validado:
                            sheet.write(row, 0, anio.name, body_format)
                            sheet.write(row, 1, mes, body_format)
                            sheet.write(row, 2, req.distrito_id.code, body_format)
                            sheet.write(row, 3, req.unidad_territorial_id.name, body_format)
                            sheet.write(row, 4, req.departamento_id.name, body_format)
                            sheet.write(row, 5, req.provincia_id.name, body_format)
                            sheet.write(row, 6, req.distrito_id.name, body_format)
                            sheet.write(row, 7, cg.distrito_id.code, body_format)
                            sheet.write(row, 8, cg.departamento_id.name, body_format)
                            sheet.write(row, 9, cg.provincia_id.name, body_format)
                            sheet.write(row, 10, cg.distrito_id.name, body_format)
                            sheet.write(row, 11, cg.centro_poblado_id.code, body_format)
                            sheet.write(row, 12, cg.centro_poblado_id.name, body_format)
                            sheet.write(row, 13, cg.id, body_format)
                            sheet.write(row, 14, cg.codigo, body_format)
                            sheet.write(row, 15, cg.name, body_format)
                            sheet.write(row, 16, fac.id, body_format)
                            sheet.write(row, 17, fac.person_id.firstname, body_format)
                            sheet.write(row, 18, fac.person_id.secondname, body_format)
                            sheet.write(row, 19, fac.person_id.name, body_format)
                            sheet.write(row, 20, fac.person_id.birthdate, body_format)
                            sheet.write(row, 21, fac.person_id.type_document, body_format)
                            sheet.write(row, 22, fac.person_id.document_number, body_format)
                            sheet.write(row, 23, fac.person_id.gender.upper() or '', body_format)
                            sheet.write(
                                row, 24, buscar_discapacidad('DISC_VISUAL', fac.person_id.discapacidad_id), body_format
                            )
                            sheet.write(
                                row, 25, buscar_discapacidad('DISC_OIR', fac.person_id.discapacidad_id), body_format
                            )
                            sheet.write(
                                row, 26, buscar_discapacidad('DISC_HABLAR', fac.person_id.discapacidad_id), body_format
                            )
                            sheet.write(
                                row, 27, buscar_discapacidad('DISC_EXTREMIDADES', fac.person_id.discapacidad_id),
                                body_format
                            )
                            sheet.write(
                                row, 28, buscar_discapacidad('DISC_MENTAL', fac.person_id.discapacidad_id), body_format
                            )
                            sheet.write(row, 29, 'SI' if fac.person_id.discapacidad_id else 'NO', body_format)
                            sheet.write(
                                row, 30, buscar_tabla_base(u'CunaMás', fac.person_id.programa_social_ids), body_format
                            )
                            sheet.write(
                                row, 31, buscar_tabla_base('Qali Warma', fac.person_id.programa_social_ids), body_format
                            )
                            sheet.write(
                                row, 32, buscar_tabla_base('Foncodes', fac.person_id.programa_social_ids), body_format
                            )
                            sheet.write(row, 33, buscar_tabla_base('Juntos', fac.person_id.programa_social_ids),
                                        body_format)
                            sheet.write(
                                row, 34, buscar_tabla_base(u'Pensión', fac.person_id.programa_social_ids), body_format
                            )
                            sheet.write(row, 35, buscar_tabla_base('Otro', fac.person_id.programa_social_ids),
                                        body_format)
                            sheet.write(row, 36, buscar_tabla_base('Ninguno', fac.person_id.programa_social_ids),
                                        body_format)
                            sheet.write(row, 37, fac.person_id.marital_status, body_format)
                            sheet.write(row, 38, buscar_jefe_hogar(fac.person_id.integrantes_lines_ids), body_format)
                            sheet.write(row, 39, 'SI' if fac.person_id.sabe_leer_y_escribir else 'NO', body_format)
                            sheet.write(row, 40, fac.person_id.nro_hijos, body_format)
                            sheet.write(row, 41, fac.person_id.religion_id.name, body_format)
                            sheet.write(row, 42, fac.person_id.lengua_materna_id.name, body_format)
                            sheet.write(row, 43, fac.person_id.nivel_educativo_id.name, body_format)
                            sheet.write(row, 44, fac.person_id.ultimo_grado_apr_id.name, body_format)
                            sheet.write(row, 45, fac.person_id.ocupacion_id.name, body_format)
                            sheet.write(row, 46, lines.fecha_creacion, body_format)
                            row += 1


FacilitadorasVisitasXls('report.base_cuna.FacilitadorasVisitasXls.xlsx', 'dynamic.report.wizard')


class DatosObservadosXls(ReportXlsx):

    def cantidad_visitas(self, lines):
        mas_5 = ante_fec_nac = 0
        for line in lines:
            if len(line.visita_ids) > 5:
                mas_5 += 1
            for visita in line.visita_ids:
                if visita.fecha_visita < line.fecha_nac_ges:
                    ante_fec_nac += 1
        return mas_5, ante_fec_nac

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('DATOS OBSERVADOS')

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        sheet.write(0, 0, 'N°', title_blue_format)
        sheet.write(0, 1, 'UT', title_blue_format)
        sheet.write(0, 2, 'N_CG', title_blue_format)
        sheet.write(0, 3, 'N_FAMILIAS', title_blue_format)
        sheet.write(0, 4, 'N_NINIO', title_blue_format)
        sheet.write(0, 5, 'N_REGISTROS', title_blue_format)
        sheet.write(0, 6, 'GEST_12_50', title_blue_format)
        sheet.write(0, 7, 'NIN0_FUERA_EDAD', title_blue_format)
        sheet.write(0, 8, 'SIN VISITAS', title_blue_format)
        sheet.write(0, 9, 'MAS 5 VISITAS', title_blue_format)
        sheet.write(0, 10, 'NAC ANT ATENC', title_blue_format)
        sheet.write(0, 11, 'TIENE NO TIENE DISCA', title_blue_format)
        sheet.write(0, 12, 'DISCA MAYOR 3', title_blue_format)
        sheet.write(0, 13, 'FAM_MAS_5_VIS_RECO', title_blue_format)
        sheet.write(0, 14, 'VER_OBS', title_blue_format)

        i = 1
        for ut in uts:
            familias = ninios = visita_ninio = no_visita_ninio = madre_gestante = ninio_fuera_edad = mas_5visitas = \
                ante_fec_nac = tiene_no_tiene_dis = dis_mayor_3 = mas_5visitas_rec = 0
            for cg in ut.ut_comites_gestion:
                familias += len(cg.familias_ids)
                for familia in cg.familias_ids:
                    for integrante in familia.integrantes_ids:
                        if integrante.parentesco_id.name == 'Niño(a)':
                            ninios += 1
                            if integrante.integrante_id.months > 36:
                                ninio_fuera_edad += 1
                            if integrante.fichas_rec_ids or integrante.fichas_fort_ids or integrante.ficha_gestante_ids:
                                mas_5visitas_a, ante_fec_nac_a = self.cantidad_visitas(integrante.fichas_rec_ids)
                                mas_5visitas_b, ante_fec_nac_b = self.cantidad_visitas(integrante.fichas_fort_ids)
                                mas_5visitas_c, ante_fec_nac_c = self.cantidad_visitas(integrante.ficha_gestante_ids)
                                mas_5visitas_rec += mas_5visitas_a
                                mas_5visitas += mas_5visitas_a + mas_5visitas_b + mas_5visitas_c
                                ante_fec_nac += ante_fec_nac_a + ante_fec_nac_b + ante_fec_nac_c
                                visita_ninio += 1
                            else:
                                no_visita_ninio += 1
                        if integrante.integrante_id.discapacidad_id:
                            disc_no = integrante.integrante_id.discapacidad_id.filtered(
                                lambda x: x.abrev == 'NO_TIENE_DISCAPACIDAD'
                            )
                            if len(integrante.integrante_id.discapacidad_id) > 3:
                                dis_mayor_3 += 1
                            if disc_no and len(integrante.integrante_id.discapacidad_id) > 1:
                                tiene_no_tiene_dis += 1
                        if integrante.madre_gestante and 12 <= integrante.integrante_id.months / 12 <= 50:
                            madre_gestante += 1

            sheet.write(i, 0, i, body_format)
            sheet.write(i, 1, ut.name, body_format)
            sheet.write(i, 2, len(ut.ut_comites_gestion), body_format)
            sheet.write(i, 3, familias, body_format)
            sheet.write(i, 4, ninios, body_format)
            sheet.write(i, 5, visita_ninio, body_format)
            sheet.write(i, 6, madre_gestante, body_format)
            sheet.write(i, 7, ninio_fuera_edad, body_format)
            sheet.write(i, 8, ninio_fuera_edad, body_format)
            sheet.write(i, 9, no_visita_ninio, body_format)
            sheet.write(i, 10, mas_5visitas, body_format)
            sheet.write(i, 11, tiene_no_tiene_dis, body_format)
            sheet.write(i, 12, dis_mayor_3, body_format)
            sheet.write(i, 13, mas_5visitas_rec, body_format)
            sheet.write(i, 14, '=SUM(G%d:N%d)' % (i + 1, i + 1), body_format)
            i += 1


DatosObservadosXls('report.base_cuna.DatosObservadosXls.xlsx', 'dynamic.report.wizard')


class SaludXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#3f93aa',
            'border': True,
        })

        sheet = workbook.add_worksheet('DATOS')

        anio = lines.anio
        mes = lines.mes
        servicio = lines.servicio

        sheet.write(0, 0, 'UNIDAD TERRITORIAL', title_blue_format)
        sheet.write(0, 1, 'CODIGO CENTRO POBLADO', title_blue_format)
        sheet.write(0, 2, 'CODIGO COMITE', title_blue_format)
        sheet.write(0, 3, 'NOMBRE COMITE', title_blue_format)
        sheet.write(0, 4, 'NINIO_ID', title_blue_format)
        sheet.write(0, 5, 'APELLIDO_PAT', title_blue_format)
        sheet.write(0, 6, 'APELLIDO_MAT', title_blue_format)
        sheet.write(0, 7, 'NOMBRE_NINIO', title_blue_format)
        sheet.write(0, 8, 'NRO DNI', title_blue_format)
        sheet.write(0, 9, 'FECHA NAC', title_blue_format)
        sheet.write(0, 10, 'NAC EDADNNMES', title_blue_format)
        sheet.write(0, 11, 'CODIGO_FACILITADOR', title_blue_format)
        sheet.write(0, 12, 'CODIGO_FAMILIA', title_blue_format)
        sheet.write(0, 13, 'USUARIO_REGISTRO', title_blue_format)
        sheet.write(0, 14, 'FECHA_EXPORTACION', title_blue_format)
        sheet.write(0, 15, 'ESTABLECIMIENTO_SALUD', title_blue_format)
        sheet.write(0, 16, 'PESO_AL_NACER', title_blue_format)
        sheet.write(0, 17, 'TALLA_AL_NACER', title_blue_format)
        sheet.write(0, 18, 'TIPO_ESTABLECIMIENTO', title_blue_format)
        sheet.write(0, 19, 'FECHA_RECIBIO_MICRONUTRIENTES', title_blue_format)
        sheet.write(0, 20, 'NRO_MESES_MICRONUTRIENTES', title_blue_format)
        sheet.write(0, 21, 'TIENE_ALERGFIA', title_blue_format)
        sheet.write(0, 22, 'ALERGIA', title_blue_format)
        sheet.write(0, 23, 'TIENE_ENFERMEDAD_NACIMIENTO', title_blue_format)
        sheet.write(0, 24, 'ENFERMEDAD_NACIMIENTO', title_blue_format)
        sheet.write(0, 25, 'DIAGNOSTICO_NUTRICIONAL_TE', title_blue_format)
        sheet.write(0, 26, 'DIAGNOSTICO_NUTRICIONAL_PT', title_blue_format)
        sheet.write(0, 27, 'DIAGNOSTICO_NUTRICIONAL_PE', title_blue_format)
        sheet.write(0, 28, 'TALLA', title_blue_format)
        sheet.write(0, 29, 'PARASITOSIS_EXAMEN', title_blue_format)
        sheet.write(0, 30, 'PARASITOSIS_RESULTADO_TIENE', title_blue_format)
        sheet.write(0, 31, 'PARASITOSIS_TRATAMIENTO', title_blue_format)
        sheet.write(0, 32, 'PESO', title_blue_format)
        sheet.write(0, 33, 'TIPO_RIESGO_NUTRICIONAL_RT', title_blue_format)
        sheet.write(0, 34, 'TIPO_RIESGO_NUTRICIONAL_RP', title_blue_format)
        sheet.write(0, 35, 'TIPO_RIESGO_NUTRICIONAL_RD', title_blue_format)
        sheet.write(0, 36, 'PENULTIMOCONTROL', title_blue_format)
        sheet.write(0, 37, 'ULTIMOCONTROL', title_blue_format)
        sheet.write(0, 38, 'BCG', title_blue_format)
        sheet.write(0, 39, 'HVB', title_blue_format)
        sheet.write(0, 40, 'ANTIAMARILICA', title_blue_format)
        sheet.write(0, 41, 'INFLUENZA_1', title_blue_format)
        sheet.write(0, 42, 'INFLUENZA_2', title_blue_format)
        sheet.write(0, 43, 'INFLUENZA_3', title_blue_format)
        sheet.write(0, 44, 'IPV_APO_1', title_blue_format)
        sheet.write(0, 45, 'IPV_APO_2', title_blue_format)
        sheet.write(0, 46, 'NEUMOCOCO_1', title_blue_format)
        sheet.write(0, 47, 'NEUMOCOCO_2', title_blue_format)
        sheet.write(0, 48, 'NEUMOCOCO_3', title_blue_format)
        sheet.write(0, 49, 'NEUMOCOCO_4', title_blue_format)
        sheet.write(0, 50, 'PENTAVALENTE_1', title_blue_format)
        sheet.write(0, 51, 'PENTAVALENTE_2', title_blue_format)
        sheet.write(0, 52, 'PENTAVALENTE_3', title_blue_format)
        sheet.write(0, 53, 'REFUERZO_APO_1', title_blue_format)
        sheet.write(0, 54, 'REFUERZO_APO_2', title_blue_format)
        sheet.write(0, 55, 'REFUERZO_DPT', title_blue_format)
        sheet.write(0, 56, 'ROTAVIRUS_1', title_blue_format)
        sheet.write(0, 57, 'ROTAVIRUS_2', title_blue_format)
        sheet.write(0, 58, 'SPR_1', title_blue_format)
        sheet.write(0, 59, 'SPR_2', title_blue_format)
        sheet.write(0, 60, 'ANIO', title_blue_format)
        sheet.write(0, 61, 'MES', title_blue_format)
        sheet.write(0, 62, 'FECHA REPORTE', title_blue_format)

        row = 1

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            fichas_salud = self.env['ficha.salud'].search([
                ('anio', '=', anio.id),
                ('mes', '=', mes),
                ('unidad_territorial_id', '=', ut.id),
                ('servicio', '=', servicio)
            ])
            for rec in fichas_salud:
                sheet.write(row, 0, rec.unidad_territorial_id.name, body_format)
                sheet.write(row, 1, rec.centro_poblado_id.name, body_format)
                sheet.write(row, 2, rec.comite_gestion_id.codigo, body_format)
                sheet.write(row, 3, rec.comite_gestion_id.name, body_format)
                sheet.write(row, 4, rec.ninio_id.id, body_format)
                sheet.write(row, 5, rec.ninio_id.integrante_id.firstname, body_format)
                sheet.write(row, 6, rec.ninio_id.integrante_id.secondname, body_format)
                sheet.write(row, 7, rec.ninio_id.integrante_id.name, body_format)
                sheet.write(row, 8, rec.ninio_id.integrante_id.document_number, body_format)
                sheet.write(row, 9, rec.ninio_id.integrante_id.birthdate, body_format)
                sheet.write(row, 10, rec.ninio_id.integrante_id.months, body_format)
                sheet.write(row, 11, rec.facilitador_id.name, body_format)
                sheet.write(row, 12, rec.familia_id.id, body_format)
                sheet.write(row, 13, rec.usuario_creador_id.name, body_format)
                sheet.write(row, 14, rec.fecha_creacion, body_format)
                sheet.write(row, 15, rec.nombre_establecimiento, body_format)
                sheet.write(row, 16, rec.peso_nacer, body_format)
                sheet.write(row, 17, rec.talla_nacer, body_format)
                sheet.write(row, 18, rec.tipo_establecimiento, body_format)
                sheet.write(row, 19, rec.fecha_ini_micro or '', body_format)
                sheet.write(row, 20, rec.nro_meses_mcn, body_format)
                sheet.write(row, 21, 'SI' if rec.alergia else 'NO', body_format)
                sheet.write(row, 22, rec.clase_alergia or '', body_format)
                sheet.write(row, 23, 'SI' if rec.enfermedad_nac else 'NO', body_format)
                sheet.write(row, 24, rec.clase_enfermedad or '', body_format)
                ultimo = penultimo = peso = para_tratamiento = para_tiene = para_examen = talla = te = pt = pe = False
                if rec.control_cred_ids:
                    ultimo = len(rec.control_cred_ids) - 1
                    te = rec.control_cred_ids[ultimo].talla_edad
                    pt = rec.control_cred_ids[ultimo].peso_talla
                    pe = rec.control_cred_ids[ultimo].peso_edad
                    peso = rec.control_cred_ids[ultimo].peso_kg
                    para_examen = rec.control_cred_ids[ultimo].examen_descarte
                    para_tiene = rec.control_cred_ids[ultimo].tiene_parasitosis
                    para_tratamiento = rec.control_cred_ids[ultimo].recibe_tratamiento
                    talla = rec.control_cred_ids[ultimo].talla_cm
                    penultimo = rec.control_cred_ids[ultimo - 1].fecha_control if ultimo > 1 else False
                    ultimo = rec.control_cred_ids[ultimo].fecha_control

                sheet.write(row, 25, te if te else '', body_format)
                sheet.write(row, 26, pt if pt else '', body_format)
                sheet.write(row, 27, pe if pe else '', body_format)
                sheet.write(row, 28, talla if peso else '', body_format)
                sheet.write(row, 29, 'SI' if para_examen else '', body_format)
                sheet.write(row, 30, 'SI' if para_tiene else '', body_format)
                sheet.write(row, 31, 'SI' if para_tratamiento else '', body_format)
                sheet.write(row, 32, peso if peso else '', body_format)
                sheet.write(row, 33, 'SI' if te and te != 'NORMAL' else 'NO', body_format)
                sheet.write(row, 34, 'SI' if pt and pt != 'NORMAL' else 'NO', body_format)
                sheet.write(row, 35, 'SI' if pe and pe != 'NORMAL' else 'NO', body_format)
                sheet.write(row, 36, penultimo if penultimo else '', body_format)
                sheet.write(row, 37, ultimo if ultimo else '', body_format)

                bcg = hvb = antiamarilica = influenza1 = influenza2 = influenza3 = ipv1 = ipv2 = neumococo1 = \
                    neumococo2 = neumococo3 = neumococo4 = pentavalente1 = pentavalente2 = pentavalente3 = apo1 = apo2\
                    = dpt = rotavirus1 = rotavirus2 = spr1 = spr2 = False
                if rec.inmunizacion_ids:
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'bcg'):
                        bcg = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'bcg')
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'hvb'):
                        hvb = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'hvb')
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'antiamarilica'):
                        antiamarilica = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'antiamarilica')
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'influenza'):
                        inf = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'influenza')
                        influenza1 = inf[0]
                        influenza2 = inf[1] if inf[1] else False
                        influenza3 = inf[2] if inf[2] else False
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'ipv'):
                        ipv = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'ipv')
                        ipv1 = ipv[0]
                        ipv2 = ipv[1] if ipv[1] else False
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'neumococo'):
                        neumococo = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'neumococo')
                        neumococo1 = neumococo[0]
                        neumococo2 = neumococo[1] if neumococo[1] else False
                        neumococo3 = neumococo[1] if neumococo[1] else False
                        neumococo4 = neumococo[1] if neumococo[1] else False
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'pentavalente'):
                        pentavalente = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'pentavalente')
                        pentavalente1 = pentavalente[0]
                        pentavalente2 = pentavalente[1] if pentavalente[1] else False
                        pentavalente3 = pentavalente[1] if pentavalente[1] else False
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'apo'):
                        apo = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'apo')
                        apo1 = apo[0]
                        apo2 = apo[1] if apo[1] else False
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'dpt'):
                        dpt = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'dpt')
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'rotavirus'):
                        rotavirus = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'rotavirus')
                        rotavirus1 = rotavirus[0]
                        rotavirus2 = rotavirus[1] if rotavirus[1] else False
                    if rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'spr'):
                        spr = rec.inmunizacion_ids.filtered(lambda x: x.vacunas == 'spr')
                        spr1 = spr[0]
                        spr2 = spr[1] if spr[1] else False

                sheet.write(row, 38, bcg.fecha_vacuna if bcg else '', body_format)
                sheet.write(row, 39, hvb.fecha_vacuna if hvb else '', body_format)
                sheet.write(row, 40, antiamarilica.fecha_vacuna if antiamarilica else '', body_format)
                sheet.write(row, 41, influenza1.fecha_vacuna if influenza1 else '', body_format)
                sheet.write(row, 42, influenza2.fecha_vacuna if influenza2 else '', body_format)
                sheet.write(row, 43, influenza3.fecha_vacuna if influenza3 else '', body_format)
                sheet.write(row, 44, ipv1.fecha_vacuna if ipv1 else '', body_format)
                sheet.write(row, 45, ipv2.fecha_vacuna if ipv2 else '', body_format)
                sheet.write(row, 46, neumococo1.fecha_vacuna if neumococo1 else '', body_format)
                sheet.write(row, 47, neumococo2.fecha_vacuna if neumococo2 else '', body_format)
                sheet.write(row, 48, neumococo3.fecha_vacuna if neumococo3 else '', body_format)
                sheet.write(row, 49, neumococo4.fecha_vacuna if neumococo4 else '', body_format)
                sheet.write(row, 50, pentavalente1.fecha_vacuna if pentavalente1 else '', body_format)
                sheet.write(row, 51, pentavalente2.fecha_vacuna if pentavalente2 else '', body_format)
                sheet.write(row, 52, pentavalente3.fecha_vacuna if pentavalente3 else '', body_format)
                sheet.write(row, 53, apo1.fecha_vacuna if apo1 else '', body_format)
                sheet.write(row, 54, apo2.fecha_vacuna if apo2 else '', body_format)
                sheet.write(row, 55, dpt.fecha_vacuna if dpt else '', body_format)
                sheet.write(row, 56, rotavirus1.fecha_vacuna if rotavirus1 else '', body_format)
                sheet.write(row, 57, rotavirus2.fecha_vacuna if rotavirus2 else '', body_format)
                sheet.write(row, 58, spr1.fecha_vacuna if spr1 else '', body_format)
                sheet.write(row, 59, spr2.fecha_vacuna if spr2 else '', body_format)
                sheet.write(row, 60, lines.anio.name, body_format)
                sheet.write(row, 61, lines.mes, body_format)
                sheet.write(row, 62, lines.fecha_creacion, body_format)
                row += 1


SaludXls('report.base_cuna.SaludXls.xlsx', 'dynamic.report.wizard')


class ReporteVisitasXls(ReportXlsx):

    def _obtener_edad_meses_usuario(self, bithdate, fecha):
        birthdate = fields.Datetime.from_string(bithdate)
        now = fields.Datetime.from_string(fecha)
        delta = relativedelta(now, birthdate)
        return delta.months

    def _verificar_rol(self, groups):
        cargo = ''
        if groups:
            cat_cunamas = self.env['ir.module.category'].sudo().search([('name', '=', 'Usuario Cunamas')])
            jefe_ut = self.env['res.groups'].sudo().search(
                [('name', '=', 'Jefe de Unidad Territorial'), ('category_id', '=', cat_cunamas.id)]
            )
            coordinador = self.env['res.groups'].sudo().search(
                [('name', '=', 'Coordinador'), ('category_id', '=', cat_cunamas.id)]
            )
            asistente_adm = self.env['res.groups'].sudo().search(
                [('name', '=', 'Administrador/ Asistente Administrativo'), ('category_id', '=', cat_cunamas.id)]
            )
            tec_informatico = self.env['res.groups'].sudo().search(
                [('name', '=', u'Técnico Informático'), ('category_id', '=', cat_cunamas.id)]
            )
            especialista_integrales = self.env['res.groups'].sudo().search(
                [('name', '=', 'Especialista Integrales'), ('category_id', '=', cat_cunamas.id)]
            )
            acompaniante_tecnico = self.env['res.groups'].sudo().search(
                [('name', '=', u'Acompañante Técnico'), ('category_id', '=', cat_cunamas.id)]
            )
            servicios_terceros = self.env['res.groups'].sudo().search(
                [('name', '=', u'Servicios por Terceros'), ('category_id', '=', cat_cunamas.id)]
            )
            especialista_nutricion = self.env['res.groups'].sudo().search(
                [('name', '=', u'Especialista en Nutrición'), ('category_id', '=', cat_cunamas.id)]
            )
            gestion_comunal = self.env['res.groups'].sudo().search(
                [('name', '=', u'Gestión Comunal'), ('category_id', '=', cat_cunamas.id)]
            )
            cunamas_groups = [
                jefe_ut.id, coordinador.id, asistente_adm.id, tec_informatico.id, especialista_integrales.id,
                acompaniante_tecnico.id, servicios_terceros.id, especialista_nutricion.id, gestion_comunal.id
            ]
            new = [c for c in groups if c.id in cunamas_groups]
            for group in new:
                car = self.env['res.groups'].sudo().search([('id', '=', group.id)])
                cargo += car.name + '|'
        return cargo

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_green_forest_font_black_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#5cb85c',
            'border': True
        })
        title_blue_dark_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#7289da',
            'border': True,
            'font_color': '#ffffff'
        })
        title_purple_font_white_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#673888',
            'border': True,
            'font_color': '#ffffff'
        })
        title_green_font_white_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#00b159',
            'border': True,
            'font_color': '#ffffff'
        })
        title_green_font_black_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#00b159',
            'border': True
        })
        title_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#428bca',
            'border': True,
            'font_color': '#ffffff'
        })
        title_brown_font_white_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#a47c48',
            'border': True,
            'font_color': '#ffffff'
        })
        title_skin_font_white_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#eea990',
            'border': True,
            'font_color': '#ffffff'
        })
        title_light_blue_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#bae1ff',
            'border': True,
        })
        title_light_yellow_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#ffffba',
            'border': True,
        })

        sheet = workbook.add_worksheet('DATOS')

        anio = lines.anio
        mes = lines.mes
        servicio = lines.servicio

        sheet.write(2, 0, 'Código de ubicación de Distrito del CG', title_blue_dark_format)
        sheet.write(2, 1, 'Nombre de la Unidad Territorial', title_blue_dark_format)
        sheet.write(2, 2, 'Nombre de  Cordinación de la Unidad Territorial', title_blue_dark_format)
        sheet.write(2, 3, 'Nombre del departamento del CG', title_blue_dark_format)
        sheet.write(2, 4, 'Nombre de provincia del CG', title_blue_dark_format)
        sheet.write(2, 5, 'Nombre de distrito', title_blue_dark_format)
        sheet.write(2, 6, 'Código de Comité de Gestión requerimiento', title_blue_dark_format)
        sheet.write(2, 7, 'Número de Facilitadores según requerimiento', title_blue_dark_format)
        sheet.write(2, 8, 'Número de familias de requerimiento', title_blue_dark_format)
        sheet.merge_range('A2:I2', 'INFORMACIÓN UBIGEO UT REQUERIMIENTO', title_green_forest_font_black_format)

        sheet.write(2, 9, 'Nombre de la Unidad Territorial', title_blue_format)
        sheet.write(1, 9, 'INFORMACIÓN UT', title_green_font_white_format)

        sheet.write(2, 10, 'Código de ubigeo distrital de la familia', title_blue_format)
        sheet.write(2, 11, 'Nombre de Departamento de la familia', title_blue_format)
        sheet.write(2, 12, 'Nombre de Provincia de la familia', title_blue_format)
        sheet.write(2, 13, 'Nombre del distrito de la familia', title_blue_format)
        sheet.write(2, 14, 'Código de Acompañante Técnico', title_blue_format)
        sheet.write(2, 15, 'Numero de DNI del Acompañante técnico', title_blue_format)
        sheet.write(2, 16, 'Nombre del Acompañante Técnico', title_blue_format)
        sheet.write(2, 17, 'Apellido paterno del Acompañante Técnico', title_blue_format)
        sheet.write(2, 18, 'Apellido materno del Acompañante Técnico', title_blue_format)
        sheet.write(2, 19, 'Sexo del Acompañante Técnico', title_blue_format)
        sheet.write(2, 20, 'Fecha de nacimiento del Acompañante Técnico', title_blue_format)
        sheet.write(2, 21, 'Código del Acompañante Técnico 2', title_blue_format)
        sheet.write(2, 22, 'DNI del Acompañante  Técnico 2', title_blue_format)
        sheet.write(2, 23, 'Nombre del Acompañante  Técnico 2', title_blue_format)
        sheet.write(2, 24, 'Apellido Paterno del del Acompañante  Técnico 2', title_blue_format)
        sheet.write(2, 25, 'Apellido Materno del Acompañante  Técnico 2', title_blue_format)
        sheet.write(2, 26, 'Sexo del del Acompañante  Técnico 2', title_blue_format)
        sheet.write(2, 27, 'Fecha de Nacimiento del del Acompañante  Técnico 2', title_blue_format)
        sheet.write(2, 28, 'Código de identificación única del Comité de Gestión', title_green_font_black_format)
        sheet.write(2, 29, 'Código de Comité de gestión', title_blue_format)
        sheet.write(2, 30, 'Nombre de Comité de Gestión', title_blue_format)
        sheet.merge_range('K2:AE2', 'INFORMACIÓN CG', title_green_font_white_format)

        sheet.write(2, 31, 'ID CENTRO POBLADO', title_blue_format)
        sheet.write(2, 32, 'Código de ubigeo del centro poblado de la familia', title_blue_format)
        sheet.write(2, 33, 'Nombre del Centro poblado de la familia', title_blue_format)
        sheet.merge_range('AF2:AH2', 'UBICACIÓN DEL HOGAR', title_green_font_white_format)

        sheet.write(2, 34, u'Año de visitas', title_blue_format)
        sheet.write(2, 35, 'Mes de visitas', title_blue_format)
        sheet.merge_range('AI2:AJ2', 'INFORMACION PERIODO REGISTRO VISITAS', title_blue_format)

        sheet.write(2, 36, u'Código de Familia', title_blue_format)
        sheet.write(2, 37, 'Dirección de vivienda de la familia', title_blue_format)
        sheet.write(2, 38, 'Código de Facilitador', title_blue_format)
        sheet.write(2, 39, 'Nombre completo del Facilitador', title_blue_format)
        sheet.merge_range('AK2:AN2', 'INFORMACION DEL HOGAR', title_blue_format)

        sheet.write(2, 40, 'Tipo documento facilitador visita', title_purple_font_white_format)
        sheet.write(2, 41, u'Número documento facilitador visita', title_purple_font_white_format)
        sheet.write(2, 42, 'SEXO FACILITADOR VISITA', title_purple_font_white_format)
        sheet.write(2, 43, 'FECHA NACIMIENTO FACILITADOR VISITA', title_purple_font_white_format)
        sheet.write(2, 44, 'EDAD FACILITADOR VISITA EN AÑOS AL CIERRE DEL MES', title_purple_font_white_format)
        sheet.merge_range('AO2:AS2', 'INFORMACIÓN DE FALICITADOR VISITA', title_green_forest_font_black_format)

        sheet.write(2, 45, 'Código de cuidador principal', title_blue_format)
        sheet.write(2, 46, 'Nombre del Cuidador Principal', title_blue_format)
        sheet.write(2, 47, 'Apellido paterno de cuidador principal', title_blue_format)
        sheet.write(2, 48, 'Apellido Materno del Cuidador Principal', title_blue_format)
        sheet.write(2, 49, 'Nombre completo del Cuidador principal', title_blue_format)
        sheet.write(2, 50, 'Sexo del Cuidador Principal', title_blue_format)
        sheet.write(2, 51, 'Si el cuidador principal es Gestante', title_blue_format)
        sheet.write(2, 52, 'Tipo de documento del Cuidador Principal', title_blue_format)
        sheet.write(2, 53, u'Número de documento del Cuidador Principal', title_blue_format)
        sheet.write(2, 54, u'Número de documento del Cuidador Principal Validado En Reniec', title_blue_format)
        sheet.write(2, 55, 'Tipo de Seguro del Cuidador Principal', title_blue_format)
        sheet.write(2, 56, 'Estado civil del cuidador principal', title_blue_format)
        sheet.write(2, 57, 'Fecha de nacimiento del Cuidador Principal', title_blue_format)
        sheet.write(2, 58, 'Fecha de registro al programa del Cuidador Principal', title_blue_format)
        sheet.write(2, 59, 'Edad del cuidador principal', title_blue_format)
        sheet.merge_range('AT2:BH2', 'INFORMACIÓN DEL CUIDADOR PRINCIPAL', title_green_forest_font_black_format)

        sheet.write(2, 60, 'Código de Usuario', title_blue_format)
        sheet.write(2, 61, 'Nombres completos del Usuario', title_blue_format)
        sheet.write(2, 62, 'Nombre del Usuario', title_blue_format)
        sheet.write(2, 63, 'Apellido Paterno del Usuario', title_blue_format)
        sheet.write(2, 64, 'Apellido Materno del Usuario', title_blue_format)
        sheet.write(2, 65, 'Sexo del Usuario', title_blue_format)
        sheet.write(2, 66, 'Fecha de registro del usuario al programa', title_blue_format)
        sheet.write(2, 67, 'Tipo de Beneficiario', title_blue_format)
        sheet.write(2, 68, 'Estado civil Beneficiario', title_blue_format)
        sheet.write(2, 69, 'Tipo de documento del usuario', title_blue_format)
        sheet.write(2, 70, 'Numero de documento del usuario', title_blue_format)
        sheet.write(2, 71, 'Número de documento del Usuario Validado En Reniec', title_blue_format)
        sheet.write(2, 72, 'Tipo de seguro de salud del usuario', title_blue_format)
        sheet.write(2, 73, 'Número de seguro de salud del usuario', title_blue_format)
        sheet.write(2, 74, 'Fecha de Nacimiento del usuario', title_blue_format)
        sheet.write(2, 75, 'Edad en meses del usuario al inicio del mes', title_blue_format)
        sheet.write(2, 76, 'Edad en meses del usuario en su última visita', title_blue_format)
        sheet.write(2, 77, 'PRIMERA VISITA', title_blue_format)
        sheet.write(2, 78, u'ÚLTIMA VISITA', title_blue_format)
        sheet.write(2, 79, u'PERMANENCIA EN MESES DESDE LA PRIMERA VISITA HASTA LA ÚLTIMA', title_blue_format)
        sheet.write(2, 80, u'CATEGORIA EDAD NIÑOS - MESES', title_blue_format)
        sheet.write(2, 81, 'Discapacidad visual del usuario', title_blue_format)
        sheet.write(2, 82, 'Discapacidad auditiva del usuario', title_blue_format)
        sheet.write(2, 83, 'Discapacidad física del usuario', title_blue_format)
        sheet.write(2, 84, 'Discapacidad física del usuario', title_blue_format)
        sheet.write(2, 85, 'Discapacidad intelectual del usuario', title_blue_format)
        sheet.write(2, 86, 'Si el usuario no tiene ninguna discapacidad', title_blue_format)
        sheet.merge_range('BI2:CI2', 'INFORMACIÓN DEL USUARIO', title_brown_font_white_format)

        sheet.write(2, 87, u'Número de visitas F5', title_blue_format)
        sheet.write(2, 88, u'Número de visitas efectivas F5', title_blue_format)
        sheet.write(2, 89, 'Consumo de MMNv F5', title_blue_format)
        sheet.write(2, 90, u'Número de MMN que cuenta el hogar F5', title_blue_format)
        sheet.write(2, 91, 'Fechas de las visitas F5', title_blue_format)
        sheet.write(2, 92, 'Recibe Sulfato CS F5', title_blue_format)
        sheet.write(2, 93, u'Número de Sulfato que cuenta el hogar F5', title_blue_format)
        sheet.merge_range('CJ2:CP2', 'INFORMACIÓN VISITA FORMATO F5', title_skin_font_white_format)

        sheet.write(2, 94, u'Número de fichas F6', title_blue_format)
        sheet.write(2, 95, 'Consumo de MMNv F6', title_blue_format)
        sheet.write(2, 96, 'Número de MMN que cuenta el hogar F6', title_blue_format)
        sheet.write(2, 97, 'Fechas de las visitas F6', title_blue_format)
        sheet.write(2, 98, 'Recibe Sulfato CS F6', title_blue_format)
        sheet.write(2, 99, 'Número de Sulfato que cuenta el hogar F6', title_blue_format)
        sheet.write(2, 100, 'Consume Sulfato visita F6', title_blue_format)
        sheet.merge_range('CQ2:CW2', 'INFORMACIÓN VISITA FORMATO F6', title_skin_font_white_format)

        sheet.write(2, 101, 'Total visitas (F5+F6)', title_blue_format)
        sheet.write(2, 102, 'N° Familias por Facilitador', title_blue_format)
        sheet.write(2, 103, 'FAM TIENE LOS SOBRES DE MMN SUMA MNN cuenta en hogar (F5+F6)', title_blue_format)
        sheet.merge_range('CX2:CZ2', 'INFORMACIÓN DE VISITAS TOTALES', title_light_blue_format)

        sheet.write(2, 104, 'Fecha de registro de F5', title_blue_format)
        sheet.write(2, 105, 'Usuario (AT/TI) quien creó el registro', title_blue_format)
        sheet.write(2, 106, 'Máxima fecha de visita F5', title_blue_format)
        sheet.write(2, 107, 'Nombre del responsable de registro de F5', title_blue_format)
        sheet.write(2, 108, 'Cargo del responsable del registro de F5', title_blue_format)
        sheet.merge_range('DA2:DE2', 'INFORMACIÓN DEL REGISTRO Y VISITAS DE FICHA F5',
                          title_green_forest_font_black_format)

        sheet.write(2, 109, 'Fecha de registro de F6', title_blue_format)
        sheet.write(2, 110, 'Usuario (AT/TI) quien creó el registro', title_blue_format)
        sheet.write(2, 111, 'Máxima fecha de visita F6', title_blue_format)
        sheet.write(2, 112, 'Nombre del responsable de registro de F6', title_blue_format)
        sheet.write(2, 113, 'Cargo del responsable del registro de F6', title_blue_format)
        sheet.merge_range('DF2:DJ2', 'INFORMACIÓN DEL REGISTRO Y VISITAS DE FICHA F6',
                          title_green_forest_font_black_format)

        sheet.write(2, 114, 'Niñas/os <0 y >= 37 meses.', title_light_yellow_format)
        sheet.write(2, 115, 'Usuarios sin visitas', title_light_yellow_format)
        sheet.write(2, 116, 'Niños/as nacidos posteriores al mes de atención', title_light_yellow_format)
        sheet.write(2, 117, 'Niños/as registrados en el SISAF antes de nacer', title_light_yellow_format)
        sheet.write(2, 118, 'Se registra usuario con más de 3,4,5 discapacidades reportadas', title_light_yellow_format)
        sheet.write(2, 119, 'Familia registrada en CG que no cuenta con Requerimiento', title_light_yellow_format)
        sheet.write(2, 120, 'Usuarios que recibieron más de una 5 visitas de fortalecimiento en el mes ',
                    title_light_yellow_format)
        sheet.write(2, 121, 'Fecha de emisión del reporte', title_light_yellow_format)
        sheet.write(2, 122, 'Registro Ficha Salud Id', title_light_yellow_format)
        sheet.write(2, 123, 'CANTIDAD DE FICHAS DE SALUD DEL NIÑO CON TAMIZAJE', title_light_yellow_format)
        sheet.write(2, 124, 'Origen Datos', title_light_yellow_format)
        sheet.write(2, 125, 'Cantidad de Tamizajes', title_light_yellow_format)
        sheet.write(2, 126, 'ID EXPORTACIÓN', title_light_yellow_format)
        sheet.write(2, 127, 'Fecha Tamizaje', title_light_yellow_format)
        sheet.write(2, 128, 'Edad Examen', title_light_yellow_format)
        sheet.write(2, 129, 'Diagnostico del tamizaje de anemia', title_light_yellow_format)
        sheet.write(2, 130, 'Valor de hemoglobina ', title_light_yellow_format)
        sheet.write(2, 131, 'Fecha de Exportación', title_light_yellow_format)
        sheet.write(2, 132, 'Fecha Inicial de Tratamiento', title_light_yellow_format)
        sheet.write(2, 133, 'Cantidad de Controles', title_light_yellow_format)
        sheet.write(2, 134, 'TAMIZADO [SI/NO]', title_light_yellow_format)
        sheet.write(2, 135, 'Observación Tamizaje', title_light_yellow_format)

        row = 3
        sheet.set_row(2, 40)
        sheet.set_column('A:HE', 12)
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        if not uts:
            raise ValidationError('No se encontraron Unidades Territoriales')

        for ut in uts:
            fichas_rec = self.env['ficha.reconocimiento'].search([
                ('anio', '=', anio.id),
                ('mes', '=', mes),
                ('unidad_territorial_id', '=', ut.id)
            ])

            for rec in fichas_rec:
                fichas_fort_n = self.env['ficha.fortalecimiento'].search([
                    ('anio', '=', rec.anio.id),
                    ('mes', '=', rec.mes),
                    ('unidad_territorial_id', '=', ut.id),
                    ('gestante_id', '=', rec.gestante_id.id)
                ], limit=1)
                sheet.write(row, 0, rec.comite_gestion_id.distrito_id.code if rec.comite_gestion_id.distrito_id else
                            '', body_format)
                sheet.write(row, 1, rec.unidad_territorial_id.name, body_format)
                sheet.write(row, 2, rec.unidad_territorial_id.parentUT.name if rec.unidad_territorial_id.esCT else '',
                            body_format)
                sheet.write(row, 3, rec.comite_gestion_id.name, body_format)
                sheet.write(row, 4, rec.comite_gestion_id.provincia_id.name if rec.comite_gestion_id.provincia_id else
                            '', body_format)
                sheet.write(row, 5, rec.comite_gestion_id.distrito_id.name if rec.comite_gestion_id.distrito_id else
                            '', body_format)
                req = rec.comite_gestion_id.requerimientos_ids.filtered(
                    lambda x: x.mes == mes and x.anio == anio and servicio == 'saf')
                sheet.write(row, 6, req.id if req else '', body_format)
                sheet.write(row, 7, req.desembolso_id.nro_facilitadores if req else '', body_format)
                sheet.write(row, 8, req.desembolso_id.nro_familias if req else '', body_format)

                sheet.write(row, 9, req.unidad_territorial_id.name or '', body_format)
                sheet.write(row, 10, rec.comite_gestion_id.distrito_id.code if rec.comite_gestion_id.distrito_id else
                            '', body_format)
                sheet.write(row, 11, rec.comite_gestion_id.departamento_id.name if rec.comite_gestion_id.departamento_id
                            else '', body_format)
                sheet.write(row, 12, rec.comite_gestion_id.provincia_id.name if rec.comite_gestion_id.provincia_id else
                            '', body_format)
                sheet.write(row, 13, rec.comite_gestion_id.distrito_id.name if rec.comite_gestion_id.distrito_id else
                            '', body_format)
                at = rec.comite_gestion_id.miembros_equipo.filtered(
                    lambda x: x.cargo == u'ACOMPAÑANTE TECNICO' and not x.fecha_fin)
                sheet.write(row, 14, at[0].id if at else '', body_format)
                sheet.write(row, 15, at[0].empleado_cunamas_id.document_number if at else '', body_format)
                sheet.write(row, 16, at[0].empleado_cunamas_id.firstname if at else '', body_format)
                sheet.write(row, 17, at[0].empleado_cunamas_id.lastname if at else '', body_format)
                sheet.write(row, 18, at[0].empleado_cunamas_id.secondname if at else '', body_format)
                sheet.write(row, 19, at[0].empleado_cunamas_id.gender if at else '', body_format)
                sheet.write(row, 20, at[0].empleado_cunamas_id.birthday if at else '', body_format)
                sheet.write(row, 21, at[1].id if len(at) > 1 else '', body_format)
                sheet.write(row, 22, at[1].empleado_cunamas_id.document_number if len(at) > 1 else '', body_format)
                sheet.write(row, 23, at[1].empleado_cunamas_id.firstname if len(at) > 1 else '', body_format)
                sheet.write(row, 24, at[1].empleado_cunamas_id.lastname if len(at) > 1 else '', body_format)
                sheet.write(row, 25, at[1].empleado_cunamas_id.secondname if len(at) > 1 else '', body_format)
                sheet.write(row, 26, at[1].empleado_cunamas_id.gender if len(at) > 1 else '', body_format)
                sheet.write(row, 27, at[1].empleado_cunamas_id.birthday if len(at) > 1 else '', body_format)

                sheet.write(row, 28, rec.comite_gestion_id.id, body_format)
                sheet.write(row, 29, rec.comite_gestion_id.codigo, body_format)
                sheet.write(row, 30, rec.comite_gestion_id.name, body_format)

                sheet.write(row, 31, rec.centro_poblado_id.id, body_format)
                sheet.write(row, 32, rec.centro_poblado_id.code, body_format)
                sheet.write(row, 33, rec.centro_poblado_id.name, body_format)

                sheet.write(row, 34, anio.name, body_format)
                sheet.write(row, 35, mes, body_format)

                sheet.write(row, 36, rec.familia_id.id, body_format)
                vivienda = rec.familia_id.vivienda_ids.filtered(lambda x: not x.fecha_fin)
                sheet.write(row, 37, vivienda[0].direccion if vivienda else '', body_format)
                sheet.write(row, 38, rec.facilitador_id.id, body_format)
                sheet.write(row, 39, rec.facilitador_id.name, body_format)
                sheet.write(row, 40, rec.facilitador_id.person_id.type_document, body_format)
                sheet.write(row, 41, rec.facilitador_id.person_id.document_number, body_format)
                sheet.write(row, 42, rec.facilitador_id.person_id.gender, body_format)
                sheet.write(row, 43, rec.facilitador_id.person_id.birthdate, body_format)
                sheet.write(row, 44, rec.facilitador_id.person_id.anios, body_format)

                sheet.write(row, 45, rec.cuid_principal_id.id, body_format)
                sheet.write(row, 46, rec.cuid_principal_id.integrante_id.name, body_format)
                sheet.write(row, 47, rec.cuid_principal_id.integrante_id.firstname, body_format)
                sheet.write(row, 48, rec.cuid_principal_id.integrante_id.secondname, body_format)
                sheet.write(row, 49, rec.cuid_principal_id.integrante_id.display_name, body_format)
                sheet.write(row, 50, rec.cuid_principal_id.integrante_id.gender, body_format)
                sheet.write(row, 51, 'Es gentante' if rec.cuid_principal_id.integrante_id.madre_gestante else
                            'No es gestante', body_format)
                sheet.write(row, 52, rec.cuid_principal_id.integrante_id.type_document, body_format)
                sheet.write(row, 53, rec.cuid_principal_id.integrante_id.document_number, body_format)
                sheet.write(row, 54, 'SI' if rec.cuid_principal_id.integrante_id.validado_reniec else 'NO', body_format)
                sheet.write(row, 55, rec.cuid_principal_id.integrante_id.seguro_id.name
                            if rec.cuid_principal_id.integrante_id.seguro_id else '', body_format)
                sheet.write(row, 56, rec.cuid_principal_id.integrante_id.marital_status, body_format)
                sheet.write(row, 57, rec.cuid_principal_id.integrante_id.birthdate, body_format)
                sheet.write(row, 58, rec.cuid_principal_id.integrante_id.fecha_ingreso_pncm, body_format)
                sheet.write(row, 59, rec.cuid_principal_id.integrante_id.anios, body_format)

                sheet.write(row, 60, rec.gestante_id.integrante_id.id, body_format)
                sheet.write(row, 61, rec.gestante_id.integrante_id.display_name, body_format)
                sheet.write(row, 62, rec.gestante_id.integrante_id.name, body_format)
                sheet.write(row, 63, rec.gestante_id.integrante_id.firstname, body_format)
                sheet.write(row, 64, rec.gestante_id.integrante_id.secondname, body_format)
                sheet.write(row, 65, rec.gestante_id.integrante_id.gender, body_format)
                sheet.write(row, 66, rec.gestante_id.integrante_id.fecha_ingreso_pncm, body_format)
                sheet.write(row, 67, 'Niño' if rec.gestante_id.integrante_id.gender == 'm' else 'Niña', body_format)
                sheet.write(row, 68, rec.gestante_id.integrante_id.marital_status, body_format)
                sheet.write(row, 69, rec.gestante_id.integrante_id.type_document, body_format)
                sheet.write(row, 70, rec.gestante_id.integrante_id.document_number, body_format)
                sheet.write(row, 71, 'SI' if rec.gestante_id.integrante_id.validado_reniec else 'NO', body_format)
                sheet.write(row, 72, rec.gestante_id.integrante_id.seguro_id.name
                            if rec.gestante_id.integrante_id.seguro_id else '', body_format)
                sheet.write(row, 73, rec.gestante_id.integrante_id.codigo_seguro or '', body_format)
                sheet.write(row, 74, rec.gestante_id.integrante_id.birthdate, body_format)
                date_str = '%s-%s-%s' % (anio.name, str(mes), '01')
                sheet.write(row, 75, self._obtener_edad_meses_usuario(rec.gestante_id.integrante_id.birthdate,
                                                                      date_str), body_format)
                visitas_len = len(rec.visita_ids)
                nacimiento_validar = fecha_2 = permanenecia_meses = False
                fechas = ''
                consumo_sf = consumo_sf_h = consumo_mn = consumo_mn_h = 0

                if visitas_len > 0:
                    fecha_2 = rec.visita_ids[visitas_len - 1].fecha_visita
                    permanenecia_meses = self._obtener_edad_meses_usuario(rec.visita_ids[0].fecha_visita,
                                                                          rec.visita_ids[visitas_len - 1].fecha_visita)
                    for visita in rec.visita_ids:
                        if visita.fecha_visita:
                            fechas += visita.fecha_visita
                        if visita.fecha_visita < rec.gestante_id.integrante_id.birthdate:
                            nacimiento_validar = True
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value and x.value_suggested.value.upper() == 'SI' and
                                x.question_id.question
                                in '¿Tiene los sobres de micronutrientes en el hogar?'):
                            consumo_mn += 1
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value and x.value_suggested.value.upper() == 'SI' and
                                x.question_id.question
                                in '¿El niño/a consumió los micronutrientes en el hogar?'):
                            consumo_mn_h += 1
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value and x.value_suggested.value.upper() == 'SI' and
                                x.question_id.question in
                                '¿El niño/a tiene el sulfato ferroso en el hogar?'):
                            consumo_sf_h += 1
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value and x.value_suggested.value.upper() == 'SI' and
                                x.question_id.question
                                in '¿El niño/a consumió el sulfato ferroso durante la visita?'):
                            consumo_sf += 1
                sheet.write(row, 76, self._obtener_edad_meses_usuario(rec.gestante_id.integrante_id.birthdate, fecha_2)
                            if fecha_2 else '', body_format)

                sheet.write(row, 77, rec.visita_ids[0].fecha_visita if rec.visita_ids else '', body_format)
                sheet.write(row, 78, rec.visita_ids[visitas_len - 1].fecha_visita if rec.visita_ids else '',
                            body_format)
                sheet.write(row, 79, permanenecia_meses if permanenecia_meses else '', body_format)
                sheet.write(row, 80, rec.gestante_id.integrante_id.cat_ninio if rec.gestante_id.integrante_id.cat_ninio
                            else '', body_format)
                sheet.write(
                    row, 81, buscar_discapacidad('DISC_VISUAL', rec.gestante_id.integrante_id.discapacidad_id),
                    body_format)
                sheet.write(
                    row, 82, buscar_discapacidad('DISC_OIR', rec.gestante_id.integrante_id.discapacidad_id),
                    body_format)
                sheet.write(
                    row, 83, buscar_discapacidad('DISC_HABLAR', rec.gestante_id.integrante_id.discapacidad_id),
                    body_format)
                sheet.write(
                    row, 84, buscar_discapacidad('DISC_EXTREMIDADES', rec.gestante_id.integrante_id.discapacidad_id),
                    body_format)
                sheet.write(
                    row, 85, buscar_discapacidad('DISC_MENTAL', rec.gestante_id.integrante_id.discapacidad_id),
                    body_format)
                sheet.write(row, 86, 'SI' if not rec.gestante_id.integrante_id.discapacidad_id else 'NO', body_format)
                sheet.write(row, 87, visitas_len, body_format)
                sheet.write(row, 88, rec.nro_visitas_efectivas, body_format)
                sheet.write(row, 89, consumo_mn if consumo_mn else 0, body_format)
                sheet.write(row, 90, consumo_mn_h if consumo_mn_h else 0, body_format)
                sheet.write(row, 91, fechas if fechas else '', body_format)
                sheet.write(row, 92, consumo_sf if consumo_sf else 0, body_format)
                sheet.write(row, 93, consumo_sf_h if consumo_sf_h else 0, body_format)

                total_v = consumo_sf_fort = consumo_sf_h_fort = consumo_mn_fort = consumo_mn_h_fort = 0
                fechas_f = max_fec = ''
                if fichas_fort_n and fichas_fort_n.visita_ids:
                    total_v = len(fichas_fort_n.visita_ids)
                    for visita in fichas_fort_n.visita_ids:
                        max_fec = fichas_fort_n.visita_ids[len(fichas_fort_n.visita_ids) - 1].fecha_visita
                        fechas_f += datetime.strptime(visita.fecha_visita, '%Y-%m-%d')
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value.upper() == 'SI' and x.value_suggested_name.value
                                in '¿Tiene los sobres de micronutrientes en el hogar?'):
                            consumo_mn_fort += 1
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value.upper() == 'SI' and x.value_suggested_name.value
                                in '¿El niño/a consumió los micronutrientes en el hogar?'):
                            consumo_mn_h_fort += 1
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value.upper() == 'SI' and x.value_suggested_name.value
                                in '¿El niño/a tiene el sulfato ferroso en el hogar?'):
                            consumo_sf_h_fort += 1
                        if visita.user_input_line_ids.filtered(
                                lambda x: x.value_suggested.value.upper() == 'SI' and x.value_suggested_name.value
                                in '¿El niño/a consumió el sulfato ferroso durante la visita?'):
                            consumo_sf_fort += 1

                sheet.write(row, 94, 1 if fichas_fort_n else 0, body_format)
                sheet.write(row, 95, consumo_mn_fort if consumo_mn_fort else 0, body_format)
                sheet.write(row, 96, consumo_mn_h_fort if consumo_mn_h_fort else 0, body_format)
                sheet.write(row, 97, fechas_f, body_format)
                sheet.write(row, 98, consumo_sf_fort if consumo_sf_fort else 0, body_format)
                sheet.write(row, 99, consumo_sf_h_fort if consumo_sf_h_fort else 0, body_format)
                sheet.write(row, 100, consumo_sf_h if consumo_sf_h else 0, body_format)
                sheet.write(row, 101, total_v + visitas_len, body_format)
                sheet.write(row, 102, len(rec.facilitador_id.unidad_familiar_fac_id), body_format)
                sheet.write(row, 103, consumo_mn_h_fort + consumo_mn_h, body_format)

                sheet.write(row, 104, rec.fecha_creacion or '', body_format)
                sheet.write(row, 105, rec.usuario_creador_id.login or '', body_format)
                sheet.write(row, 106, fecha_2 or '', body_format)
                sheet.write(row, 107, rec.usuario_creador_id.partner_id.display_name or '', body_format)
                sheet.write(row, 108, self._verificar_rol(rec.usuario_creador_id.groups_id), body_format)

                sheet.write(row, 109, fichas_fort_n.fecha_creacion if fichas_fort_n else '', body_format)
                sheet.write(row, 110, fichas_fort_n.usuario_creador_id.login if fichas_fort_n else '', body_format)
                sheet.write(row, 111, max_fec or '', body_format)
                sheet.write(row, 112, fichas_fort_n.usuario_creador_id.partner_id.display_name if fichas_fort_n else '',
                            body_format)
                sheet.write(row, 113, self._verificar_rol(fichas_fort_n.usuario_creador_id.groups_id if fichas_fort_n
                                                          else False), body_format)
                sheet.write(row, 114, 1 if rec.gestante_id.integrante_id.months >= 37 or
                            rec.gestante_id.integrante_id.months < 0 else '', body_format)
                sheet.write(row, 115, 1 if total_v + visitas_len == 0 else '', body_format)
                sheet.write(row, 116, 1 if visitas_len > 5 else '', body_format)
                sheet.write(row, 117, nacimiento_validar, body_format)
                sheet.write(row, 117, 1 if rec.gestante_id.integrante_id.birthdate > rec.fecha_creacion else '',
                            body_format)
                sheet.write(row, 118, 1 if len(rec.gestante_id.integrante_id.discapacidad_id) > 2 else '', body_format)
                sheet.write(row, 119, 1 if req else '', body_format)
                sheet.write(row, 120, 1 if total_v > 5 else '', body_format)
                sheet.write(row, 121, lines.fecha_creacion, body_format)
                ficha_salud = self.env['ficha.salud'].search([
                    ('anio', '=', rec.anio.id),
                    ('mes', '=', rec.mes),
                    ('unidad_territorial_id', '=', ut.id),
                    ('ninio_id', '=', rec.gestante_id.id)
                ], limit=1)
                sheet.write(row, 122, ficha_salud.id if ficha_salud else '', body_format)
                sheet.write(row, 123, 1 if ficha_salud.tamizaje_ids else '' if ficha_salud else '', body_format)
                sheet.write(row, 124, ficha_salud.app_origen if ficha_salud else '', body_format)
                sheet.write(row, 125, len(ficha_salud.tamizaje_ids) if ficha_salud else '', body_format)
                sheet.write(row, 126, ficha_salud.id_exportacion or '' if ficha_salud else '', body_format)
                controles = tamizaje_fecha = edad_ex = diag = valor = fecha_exportacion = fecha_inicial = ''
                if ficha_salud:
                    for r in ficha_salud.tamizaje_ids:
                        tamizaje_fecha += str(r.fecha_examen) + ' '
                        fecha_exportacion += str(r.fecha_exportacion) + ' ' if r.fecha_exportacion else ''
                        edad_ex += str(r.edad_examen) + ' '
                        diag += r.diagnostico + ' '
                        valor += r.resultado + ' '
                    fecha_inicial = ficha_salud.tamizaje_ids[0].fecha_vacuna if ficha_salud.tamizaje_ids else ''
                    controles = len(ficha_salud.control_cred_ids)
                sheet.write(row, 127, tamizaje_fecha, body_format)
                sheet.write(row, 128, edad_ex, body_format)
                sheet.write(row, 129, diag, body_format)
                sheet.write(row, 130, valor, body_format)
                sheet.write(row, 131, fecha_exportacion, body_format)
                sheet.write(row, 132, fecha_inicial, body_format)
                sheet.write(row, 133, controles, body_format)
                sheet.write(row, 134, 'SI' if tamizaje_fecha else 'NO', body_format)
                sheet.write(row, 135, 'CON TAMIZAJE' if tamizaje_fecha else 'SIN TAMIZAJE', body_format)

                row += 1


ReporteVisitasXls('report.base_cuna.ReporteVisitasXls.xlsx', 'dynamic.report.wizard')


class RequerimientosVisitasXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        body_red_percentage = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'bg_color': '#ff0000',
            'border': True,
            'num_format': '0.00%',
            'font_color': '#ffffff'
        })
        body_green_percentage = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'bg_color': '#33cc33',
            'border': True,
            'num_format': '0.00%',
            'font_color': '#ffffff'
        })
        title_green_forest_font_black_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#5cb85c',
            'border': True
        })
        title_blue_dark_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#7289da',
            'border': True,
            'font_color': '#ffffff'
        })

        sheet = workbook.add_worksheet('RESUMEN')
        sheet.set_row(2, 60)
        sheet.merge_range('A2:A3', 'N', title_green_forest_font_black_format)
        sheet.merge_range('B2:B3', 'UNIDAD TERRITORIAL', title_green_forest_font_black_format)
        sheet.merge_range('C2:C3', 'DEPARTAMENTO', title_green_forest_font_black_format)
        sheet.merge_range('D2:G2', 'COMITE DE GESTION', title_green_forest_font_black_format)
        sheet.write(2, 3, 'N° DE CG REQ. ', title_blue_dark_format)
        sheet.write(2, 4, 'N° CG CON FICHAS REGISTRADAS', title_blue_dark_format)
        sheet.write(2, 5, 'FALTA X CG departamento del CG', title_blue_dark_format)
        sheet.write(2, 6, '% AVANCE', title_blue_dark_format)

        sheet.merge_range('H2:L2', 'FAMILIAS', title_green_forest_font_black_format)
        sheet.write(2, 7, 'META ANUAL DE FAM X UT 2018', title_blue_dark_format)
        sheet.write(2, 8, 'N° FAMILIAS REQ', title_blue_dark_format)
        sheet.write(2, 9, 'N° DE HOGARES CON FICHAS REGISTRADAS', title_blue_dark_format)
        sheet.write(2, 10, 'DIF FAMILIAS (FIC REG - META)', title_blue_dark_format)
        sheet.write(2, 11, '% AVANCE (FIC REG / META)', title_blue_dark_format)

        sheet.merge_range('M2:P2', 'FACILITADOR', title_green_forest_font_black_format)
        sheet.write(2, 12, 'N° FAC REQ.', title_blue_dark_format)
        sheet.write(2, 13, 'N° DE FAC CON VISITAS', title_blue_dark_format)
        sheet.write(2, 14, 'DIF FACICLITADOR', title_blue_dark_format)
        sheet.write(2, 15, '% AVANCE', title_blue_dark_format)

        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        anio = lines.anio
        mes = lines.mes

        sheet2 = workbook.add_worksheet('X_CG')
        sheet2.set_row(3, 40)
        nombre_mes = str(dict(lines._fields['mes'].selection).get(lines.mes))
        sheet2.merge_range('A2:Y2', 'SEGUIMIENTO AL REGISTRO DE VISITAS AL HOGAR - %s %s' % (nombre_mes, anio.name),
                           title_green_forest_font_black_format)
        sheet2.write(3, 0, 'N°', title_blue_dark_format)
        sheet2.write(3, 1, 'UNIDAD TERRITORIAL', title_blue_dark_format)
        sheet2.write(3, 2, 'CORD_UNIDAD TERRITORIAL', title_blue_dark_format)
        sheet2.write(3, 3, 'UBIGEO DISTRITAL', title_blue_dark_format)
        sheet2.write(3, 4, 'DEPARTAMENTO', title_blue_dark_format)
        sheet2.write(3, 5, 'PROVINCIA', title_blue_dark_format)
        sheet2.write(3, 6, 'DISTRITO', title_blue_dark_format)
        sheet2.write(3, 7, 'ID COMITÉ DE GESTIÓN', title_blue_dark_format)
        sheet2.write(3, 8, 'CÓDIGO COMITÉ DE GESTIÓN', title_blue_dark_format)
        sheet2.write(3, 9, 'COMITÉ DE GESTIÓN', title_blue_dark_format)
        sheet2.write(3, 10, 'N° DE CG SEGÚN REQUERIMIENTOS', title_blue_dark_format)
        sheet2.write(3, 11, 'N° CG CON FICHAS REGISTRADAS', title_blue_dark_format)
        sheet2.write(3, 12, '% AVANCE DE REGISTRO SI-SAF POR CG', title_blue_dark_format)
        sheet2.write(3, 13, 'N° FAMILIAS SEGÚN REQUERIMIENTOS', title_blue_dark_format)
        sheet2.write(3, 14, 'N° DE HOGARES CON FICHAS REGISTRADAS', title_blue_dark_format)
        sheet2.write(3, 15, '% AVANCE DE REGISTRO SI-SAF POR FAMILIA', title_blue_dark_format)
        sheet2.write(3, 16, 'N° FAC REQUERIMEINTOS', title_blue_dark_format)
        sheet2.write(3, 17, 'N° DE FAC CON VISITAS', title_blue_dark_format)
        sheet2.write(3, 18, '% AVANCE DE REGISTRO SI-SAF POR FAC', title_blue_dark_format)
        sheet2.write(3, 19, 'Nº FAMILIAS CON VISITA REGISTRADO POR TABLET', title_blue_dark_format)
        sheet2.write(3, 20, 'Nº FAMILIAS CON VISITA REGISTRADO POR SISAF WEB', title_blue_dark_format)
        sheet2.write(3, 21, 'ALERTA FAMILIAS', title_blue_dark_format)
        sheet2.write(3, 22, 'ALERTA FACILITADORES', title_blue_dark_format)
        sheet2.write(3, 23, 'FECHA REPORTE', title_blue_dark_format)
        i = 4
        for ut in uts:
            for cg in ut.ut_comites_gestion.filtered(lambda x: x.state == 'activo' and x.servicio == 'saf'):
                sheet2.write(i, 0, i - 3, body_format)
                sheet2.write(i, 1, ut.name, body_format)
                sheet2.write(i, 2, ut.parentUT.name or '', body_format)
                sheet2.write(i, 3, ut.distrito_id.code or '', body_format)
                sheet2.write(i, 4, ut.departamento_id.name or '', body_format)
                sheet2.write(i, 5, ut.provincia_id.name or '', body_format)
                sheet2.write(i, 6, ut.distrito_id.name or '', body_format)
                sheet2.write(i, 7, cg.id, body_format)
                sheet2.write(i, 8, cg.codigo, body_format)
                sheet2.write(i, 9, cg.name, body_format)
                sheet2.write(i, 10, 1, body_format)
                req = 1 if cg.requerimientos_ids.filtered(lambda x: x.anio.id == anio.id and x.mes == mes) else 0
                nro_fam_req = cg.requerimientos_ids.filtered(lambda x: x.anio.id == anio.id and x.mes == mes)
                sheet2.write(i, 11, req , body_format)
                if req == 0:
                    sheet2.write(i, 12, 0, body_red_percentage)
                else:
                    sheet2.write(i, 12, 1, body_green_percentage)
                nro_familias = nro_fam_req.nro_familias if nro_fam_req else 0
                nro_fac = nro_fam_req.facilitadores if nro_fam_req else 0
                sheet2.write(i, 13, nro_familias, body_format)
                reco = self.env['ficha.reconocimiento'].search([
                    ('comite_gestion_id', '=', cg.id),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes)
                ])
                fort = self.env['ficha.fortalecimiento'].search([
                    ('comite_gestion_id', '=', cg.id),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes)
                ])
                gest = self.env['ficha.gestante'].search([
                    ('comite_gestion_id', '=', cg.id),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes)
                ])
                ficha_h = len(fort) + len(reco) + len(gest)
                sheet2.write(i, 14, ficha_h, body_format)
                sheet2.write(i, 15, (ficha_h / nro_familias) if nro_familias > 0 else 0, body_green_percentage)
                sheet2.write(i, 16, nro_fac, body_format)
                fac_ficha = 0
                fac_ficha += len(set(reco.mapped(lambda x: x.facilitador_id.id)))
                fac_ficha += len(set(fort.mapped(lambda x: x.facilitador_id.id)))
                fac_ficha += len(set(gest.mapped(lambda x: x.facilitador_id.id)))
                sheet2.write(i, 17, fac_ficha, body_format)
                sheet2.write(i, 18, (fac_ficha / nro_fac) if nro_fac > 0 else 0, body_green_percentage)
                ficha_tablet = 0
                ficha_tablet += len(reco.filtered(lambda x: x.app_origen == 'T'))
                ficha_tablet += len(fort.filtered(lambda x: x.app_origen == 'T'))
                ficha_tablet += len(gest.filtered(lambda x: x.app_origen == 'T'))
                sheet2.write(i, 19, ficha_tablet, body_format)
                ficha_web = 0
                ficha_web += len(reco.filtered(lambda x: x.app_origen == 'W'))
                ficha_web += len(fort.filtered(lambda x: x.app_origen == 'W'))
                ficha_web += len(gest.filtered(lambda x: x.app_origen == 'W'))
                sheet2.write(i, 20, ficha_web, body_format)
                x = (ficha_h / nro_familias) * 100 if nro_familias > 0 else 0
                if x == 0:
                    x_string = 'SIN REGISTROS'
                elif 70 >= x >= 51:
                    x_string = 'REGISTRO ENTRE 51% - 70 %'
                elif 50 >= x >= 30:
                    x_string = 'REGISTRO ENTRE 30% - 50 %'
                elif 29 >= x >= 1:
                    x_string = 'REGISTRO ENTRE 20% - 1 %'
                else:
                    x_string = ''
                sheet2.write(i, 21, x_string, body_format)
                sheet2.write(i, 22, 'N° DE FACILITADORES EXCEDE AL REQUERIMIENTO' if fac_ficha > nro_fac
                            else '', body_format)
                sheet2.write(i, 23, lines.fecha_creacion, body_format)
                i += 1

        j = 3
        for ut in uts:
            sheet.write(j, 0, j - 2, body_format)
            sheet.write(j, 1, ut.name, body_format)
            sheet.write(j, 2, ut.departamento_id.name or '', body_format)
            sheet.write(j, 3, "=SUMIFS('X_CG'!K5:K" + str(i) +  ",'X_CG'!C5:C" + str(i) + ',RESUMEN!C' + str(j + 1),
                        body_format)
            sheet.write(j, 4, "=SUMIFS('X_CG'!L5:L" + str(i) +  ",'X_CG'!C5:C" + str(i) + ',RESUMEN!C' + str(j + 1),
                        body_format)
            sheet.write(j, 5, "=F%d-E%d" % (j + 1, j + 1), body_format)
            sheet.write(j, 6, "=F%d/E%d" % (j + 1, j + 1), body_green_percentage)
            sheet.write(j, 7, sum(int(cg.meta_actual) for cg in ut.ut_comites_gestion.filtered(
                        lambda x: x.state == 'activo' and x.servicio == 'saf')), body_format)
            sheet.write(j, 8, "=SUMIFS('X_CG'!N5:N" + str(i) + ",'X_CG'!C5:C" + str(i) + ',RESUMEN!C' + str(j + 1),
                        body_format)
            sheet.write(j, 9, "=SUMIFS('X_CG'!O5:O" + str(i) + ",'X_CG'!C5:C" + str(i) + ',RESUMEN!C' + str(j + 1),
                        body_format)
            sheet.write(j, 10, "=K%d-I%d" % (j + 1, j + 1), body_format)
            sheet.write(j, 11, "=K%d/I%d" % (j + 1, j + 1), body_green_percentage)
            sheet.write(j, 12, "=SUMIFS('X_CG'!Q5:Q" + str(i) + ",'X_CG'!C5:C" + str(i) + ',RESUMEN!C' + str(j + 1),
                        body_format)
            sheet.write(j, 13, "=SUMIFS('X_CG'!R5:R" + str(i) + ",'X_CG'!C5:C" + str(i) + ',RESUMEN!C' + str(j + 1),
                        body_format)
            sheet.write(j, 14, "=O%d-N%d" % (j + 1, j + 1), body_format)
            sheet.write(j, 15, "=O%d/N%d" % (j + 1, j + 1), body_green_percentage)
            j += 1


RequerimientosVisitasXls('report.base_cuna.RequerimientosVisitasXls.xlsx', 'dynamic.report.wizard')


class AvancePlanillaXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_dark_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#7289da',
            'border': True,
            'font_color': '#ffffff'
        })
        body_percentage = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
            'num_format': '0.00%',
        })

        sheet = workbook.add_worksheet('DATA')
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        anio = lines.anio
        mes = lines.mes

        sheet.write(1, 0, 'UT', title_blue_dark_format)
        sheet.write(1, 1, 'ANIO', title_blue_dark_format)
        sheet.write(1, 2, 'MES', title_blue_dark_format)
        sheet.write(1, 3, 'CAN_CG', title_blue_dark_format)
        sheet.write(1, 4, 'CON_PL_MC', title_blue_dark_format)
        sheet.write(1, 5, 'SIN_PL_MC', title_blue_dark_format)
        sheet.write(1, 6, 'P_A_MC', title_blue_dark_format)
        sheet.write(1, 7, 'CON_PL_SC', title_blue_dark_format)
        sheet.write(1, 8, 'SIN_PL_SC', title_blue_dark_format)
        sheet.write(1, 9, 'P_A_SC', title_blue_dark_format)
        sheet.write(1, 10, 'CON_PL_AA', title_blue_dark_format)
        sheet.write(1, 11, 'SIN_PL_AA', title_blue_dark_format)
        sheet.write(1, 12, 'P_A_AA', title_blue_dark_format)
        sheet.write(1, 13, 'CON_PL_VG', title_blue_dark_format)
        sheet.write(1, 14, 'SIN_PL_VG', title_blue_dark_format)
        sheet.write(1, 15, 'P_A_VG', title_blue_dark_format)

        i = 2
        for ut in uts:
            planilla = self.env['justificacion.actor'].search([
                ('unidad_territorial_id', '=', ut.id),
                ('mes', '=', mes),
                ('anio', '=', anio.id),
                ('servicio', '=', 'scd')
            ])
            sheet.write(i, 0, ut.name, body_format)
            sheet.write(i, 1, anio.name, body_format)
            sheet.write(i, 2, int(mes), body_format)
            cgs = len(ut.ut_comites_gestion.filtered(lambda x: x.state == 'activo' and x.servicio == 'scd'))
            sheet.write(i, 3, cgs, body_format)

            mc_rec = len(planilla.filtered(lambda x: x.tipo_persona.name == 'MADRE CUIDADORA'))
            sin_pl_mc = cgs - mc_rec
            p_a_mc = mc_rec / cgs if cgs > 0 else 0
            sheet.write(i, 4, mc_rec, body_format)
            sheet.write(i, 5, sin_pl_mc, body_format)
            sheet.write(i, 6, p_a_mc, body_percentage)

            con_pl_sc = len(planilla.filtered(lambda x: x.tipo_persona.name == 'SOCIO DE COCINA'))
            sin_pl_sc = cgs - con_pl_sc
            p_a_sc = con_pl_sc / cgs if cgs > 0 else 0
            sheet.write(i, 7, con_pl_sc, body_format)
            sheet.write(i, 8, sin_pl_sc, body_format)
            sheet.write(i, 9, p_a_sc, body_percentage)

            con_pl_aa = len(planilla.filtered(lambda x: x.tipo_persona.name ==
                                                        'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'))
            sin_pl_aa = cgs - con_pl_aa
            p_a_aa = con_pl_aa / cgs if cgs > 0 else 0
            sheet.write(i, 10, con_pl_aa, body_format)
            sheet.write(i, 11, sin_pl_aa, body_format)
            sheet.write(i, 12, p_a_aa, body_percentage)

            con_pl_vg = len(planilla.filtered(lambda x: x.tipo_persona.name == 'CONSEJO DE VIGILANCIA'))
            sin_pl_vg = cgs - con_pl_vg
            p_a_vg = con_pl_vg / cgs if cgs > 0 else 0
            sheet.write(i, 13, con_pl_vg, body_format)
            sheet.write(i, 14, sin_pl_vg, body_format)
            sheet.write(i, 15, p_a_vg, body_percentage)

            i += 1

        chart1 = workbook.add_chart({'type': 'column'})
        nombre_mes = str(dict(lines._fields['mes'].selection).get(lines.mes))
        title = 'Avance Planilla Cuidadoras - %s %s' % (nombre_mes, anio.name)
        chart1.set_title({'name': title})
        chart1.add_series({
            'values': '=DATA!$C$3:$C$%s' % i,
            'categories': '=DATA!$A$3:$A$%s' % i,
            'name': 'CAN_CG',
        })
        chart1.add_series({
            'values': '=DATA!$E$3:$E$%s' % i,
            'name': 'CON_PL_MA',
        })
        chart1.set_x_axis({
            'text_axis': True,
            'values': '=DATA!$A$3:$A$%s' % i,
        })

        sheet.insert_chart('Q1', chart1)


AvancePlanillaXls('report.base_cuna.AvancePlanillaXls.xlsx', 'dynamic.report.wizard')


class AsistenciaXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_dark_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#7289da',
            'border': True,
            'font_color': '#ffffff'
        })

        sheet = workbook.add_worksheet('DATA')
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        anio = lines.anio
        mes = lines.mes

        sheet.write(1, 0, 'NUMANI', title_blue_dark_format)
        sheet.write(1, 1, 'NUMMES', title_blue_dark_format)
        sheet.write(1, 2, 'FICHA_ID', title_blue_dark_format)
        sheet.write(1, 3, 'UT', title_blue_dark_format)
        sheet.write(1, 4, 'CODUBIGEO', title_blue_dark_format)
        sheet.write(1, 5, 'NOMDEP', title_blue_dark_format)
        sheet.write(1, 6, 'NOMPRO', title_blue_dark_format)
        sheet.write(1, 7, 'NOMDIS', title_blue_dark_format)
        sheet.write(1, 8, 'IDCOM', title_blue_dark_format)
        sheet.write(1, 9, 'NOMCOM', title_blue_dark_format)
        sheet.write(1, 10, 'AT_CODIGO', title_blue_dark_format)
        sheet.write(1, 11, 'AT_PATERNO', title_blue_dark_format)
        sheet.write(1, 12, 'AT_MATERNO', title_blue_dark_format)
        sheet.write(1, 13, 'AT_NOMBRES', title_blue_dark_format)
        sheet.write(1, 14, 'NOMDEPLOC', title_blue_dark_format)
        sheet.write(1, 15, 'NOMPROLOC', title_blue_dark_format)
        sheet.write(1, 16, 'NOMDISLOC', title_blue_dark_format)
        sheet.write(1, 17, 'CCPP_LOCAL', title_blue_dark_format)
        sheet.write(1, 18, 'NOM_CCPP', title_blue_dark_format)
        sheet.write(1, 19, 'LOCAL_ID', title_blue_dark_format)
        sheet.write(1, 20, 'NOMLOC', title_blue_dark_format)
        sheet.write(1, 21, 'TIPOLOCAL', title_blue_dark_format)
        sheet.write(1, 22, 'IDTSALA', title_blue_dark_format)
        sheet.write(1, 23, 'NOMSAL', title_blue_dark_format)
        sheet.write(1, 24, 'ASISTENCIA_CONTROL_ID', title_blue_dark_format)
        sheet.write(1, 25, 'CUIDADOR_CODIGO', title_blue_dark_format)
        sheet.write(1, 26, 'CUIDADOR_PATERNO', title_blue_dark_format)
        sheet.write(1, 27, 'CUIDADOR_MATERNO', title_blue_dark_format)
        sheet.write(1, 28, 'CUIDADOR_NOMBRES', title_blue_dark_format)
        sheet.write(1, 29, 'CUIDADOR_FECNAC', title_blue_dark_format)
        sheet.write(1, 30, 'CUIDADOR_NUMDOC', title_blue_dark_format)
        sheet.write(1, 31, 'GUIA_CODIGO', title_blue_dark_format)
        sheet.write(1, 32, 'CUIDADORA_CODIGO', title_blue_dark_format)
        sheet.write(1, 33, 'COD_USUARIO', title_blue_dark_format)
        sheet.write(1, 34, 'APEPATUSU', title_blue_dark_format)
        sheet.write(1, 35, 'APEMATUSU', title_blue_dark_format)
        sheet.write(1, 36, 'NOMUSU', title_blue_dark_format)
        sheet.write(1, 37, 'SEXO', title_blue_dark_format)
        sheet.write(1, 38, 'FECNACUSU', title_blue_dark_format)
        sheet.write(1, 39, 'EDAD_USU', title_blue_dark_format)
        sheet.write(1, 40, 'NUMDOCUSU', title_blue_dark_format)
        sheet.write(1, 41, 'USU_TIPDOC', title_blue_dark_format)
        sheet.write(1, 42, 'NO_TIENE_283', title_blue_dark_format)
        sheet.write(1, 43, 'VISUAL_278', title_blue_dark_format)
        sheet.write(1, 44, 'OIR_279', title_blue_dark_format)
        sheet.write(1, 45, 'HABLAR_280', title_blue_dark_format)
        sheet.write(1, 46, 'MANOS_281', title_blue_dark_format)
        sheet.write(1, 47, 'MENTAL_282', title_blue_dark_format)
        sheet.write(1, 48, 'SEGURO', title_blue_dark_format)
        sheet.write(1, 49, 'RENIEC', title_blue_dark_format)
        sheet.write(1, 50, 'FECHA_REGISTRO_USUARIO', title_blue_dark_format)
        sheet.write(1, 51, 'TOTAL_ASI', title_blue_dark_format)
        sheet.write(1, 52, 'TOTAL_ENF', title_blue_dark_format)
        sheet.write(1, 53, 'TOTAL_FAL', title_blue_dark_format)
        sheet.write(1, 54, 'TOT_FAL_EDA', title_blue_dark_format)
        sheet.write(1, 55, 'TOT_FAL_IRA', title_blue_dark_format)
        sheet.write(1, 56, 'TOT_FAL_OTR', title_blue_dark_format)
        sheet.write(1, 57, 'TRA_CRED', title_blue_dark_format)
        sheet.write(1, 58, 'TRA_MMNN', title_blue_dark_format)
        sheet.write(1, 59, 'USUCRE', title_blue_dark_format)
        sheet.write(1, 60, 'USUMOD', title_blue_dark_format)
        sheet.write(1, 61, 'REPORTE_FECHA', title_blue_dark_format)
        sheet.write(1, 62, 'COD_FIC_DETA', title_blue_dark_format)
        sheet.write(1, 63, 'ID_SALA', title_blue_dark_format)
        sheet.write(1, 64, 'FEC_AFI_SAL', title_blue_dark_format)
        sheet.write(1, 65, 'USUARIO_CODIGO', title_blue_dark_format)
        sheet.write(1, 66, 'F_TIPO', title_blue_dark_format)
        sheet.write(1, 67, 'F_FECHA', title_blue_dark_format)
        sheet.write(1, 68, 'F_EDAD_EXAMEN', title_blue_dark_format)
        sheet.write(1, 69, 'F_DIAGNOSTICO', title_blue_dark_format)
        sheet.write(1, 70, 'F_VALOR', title_blue_dark_format)
        sheet.write(1, 71, 'F_FECHA_REGISTRO', title_blue_dark_format)
        sheet.write(1, 72, 'F_INITRATAMIENTO', title_blue_dark_format)
        sheet.write(1, 73, 'F_CANT_CONTROL', title_blue_dark_format)
        sheet.write(1, 74, 'TAMIZADOS', title_blue_dark_format)
        sheet.write(1, 75, 'TALLA', title_blue_dark_format)
        sheet.write(1, 76, 'PESO', title_blue_dark_format)
        sheet.write(1, 77, 'D_NUTRICIONAL_TE_FINAL', title_blue_dark_format)
        sheet.write(1, 78, 'D_NUTRICIONAL_PT_FINAL', title_blue_dark_format)
        sheet.write(1, 79, 'D_NUTRICIONAL_PE_FINAL', title_blue_dark_format)
        sheet.write(1, 80, 'COD_P', title_blue_dark_format)
        sheet.write(1, 81, 'SEG_P', title_blue_dark_format)
        sheet.write(1, 82, 'TPDOC_P', title_blue_dark_format)
        sheet.write(1, 83, 'NUMDOC', title_blue_dark_format)
        sheet.write(1, 84, 'APE_PAT_P', title_blue_dark_format)
        sheet.write(1, 85, 'APE_MAT_P', title_blue_dark_format)
        sheet.write(1, 86, 'NOM_PER_P', title_blue_dark_format)
        sheet.write(1, 87, 'SEX_P', title_blue_dark_format)
        sheet.write(1, 88, 'FECNAC', title_blue_dark_format)
        sheet.write(1, 89, 'TI_RELACION_BENEFICIARIO', title_blue_dark_format)
        sheet.write(1, 90, 'EST_CIV', title_blue_dark_format)
        sheet.write(1, 91, 'FECCRE_PADRE', title_blue_dark_format)
        sheet.set_column('U:CN', 25)
        i = 2
        for ut in uts:
            for cg in ut.ut_comites_gestion.filtered(lambda x: x.state == 'activo' and x.servicio == 'scd'):
                asistencias = self.env['asistencia.control'].search([
                    ('unidad_territorial_id', '=', ut.id),
                    ('comite_gestion_id', '=', cg.id),
                    ('mes', '=', mes),
                    ('anio', '=', anio.id)
                ])
                for asis in asistencias:
                    for menor in asis.asistencia_menor_ids:
                        sheet.write(i, 0, anio.name, body_format)
                        sheet.write(i, 1, mes, body_format)
                        sheet.write(i, 2, asis.id, body_format)
                        sheet.write(i, 3, ut.name, body_format)
                        sheet.write(i, 4, ut.distrito_id.code, body_format)
                        sheet.write(i, 5, ut.departamento_id.name, body_format)
                        sheet.write(i, 6, ut.provincia_id.name, body_format)
                        sheet.write(i, 7, ut.distrito_id.name, body_format)
                        sheet.write(i, 8, cg.id, body_format)
                        sheet.write(i, 9, cg.name, body_format)
                        sheet.write(i, 10, asis.acomp_tecnico_id.id, body_format)
                        sheet.write(i, 11, asis.acomp_tecnico_id.lastname, body_format)
                        sheet.write(i, 12, asis.acomp_tecnico_id.secondname, body_format)
                        sheet.write(i, 13, asis.acomp_tecnico_id.firstname, body_format)
                        sheet.write(i, 14, asis.local_id.departamento_id.name, body_format)
                        sheet.write(i, 15, asis.local_id.provincia_id.name, body_format)
                        sheet.write(i, 16, asis.local_id.distrito_id.name, body_format)
                        sheet.write(i, 17, asis.local_id.centro_poblado_id.code, body_format)
                        sheet.write(i, 18, asis.local_id.centro_poblado_id.name, body_format)
                        sheet.write(i, 19, asis.local_id.id, body_format)
                        sheet.write(i, 20, asis.local_id.name, body_format)
                        sheet.write(i, 21, asis.local_id.tipo_local, body_format)
                        sheet.write(i, 22, asis.sala_id.id, body_format)
                        sheet.write(i, 23, asis.sala_id.name, body_format)
                        sheet.write(i, 24, asis.id, body_format)
                        cuid = menor.afi_ninio_id.familia_id.integrantes_ids.filtered(lambda x: x.es_cuidador_principal)
                        sheet.write(i, 25, cuid.integrante_id.id, body_format)
                        sheet.write(i, 26, cuid.integrante_id.firstname, body_format)
                        sheet.write(i, 27, cuid.integrante_id.secondname, body_format)
                        sheet.write(i, 28, cuid.integrante_id.name, body_format)
                        sheet.write(i, 29, cuid.integrante_id.birthdate, body_format)
                        sheet.write(i, 30, cuid.integrante_id.document_number, body_format)
                        sheet.write(i, 31, asis.guia_madre_1_id.person_id.id, body_format)
                        sheet.write(i, 32, asis.mad_cuidadora_id.person_id.id, body_format)
                        sheet.write(i, 33, menor.afi_ninio_id.integrante_id.id, body_format)
                        sheet.write(i, 34, menor.afi_ninio_id.integrante_id.firstname, body_format)
                        sheet.write(i, 35, menor.afi_ninio_id.integrante_id.secondname, body_format)
                        sheet.write(i, 36, menor.afi_ninio_id.integrante_id.name, body_format)
                        sheet.write(i, 37, menor.afi_ninio_id.integrante_id.gender, body_format)
                        sheet.write(i, 38, menor.afi_ninio_id.integrante_id.birthdate, body_format)
                        sheet.write(i, 39, menor.afi_ninio_id.integrante_id.months, body_format)
                        sheet.write(i, 40, menor.afi_ninio_id.integrante_id.document_number, body_format)
                        sheet.write(i, 41, menor.afi_ninio_id.integrante_id.type_document, body_format)
                        discapacidad = menor.afi_ninio_id.integrante_id.discapacidad_id
                        sheet.write(i, 42, 1 if not discapacidad else 0,body_format)
                        sheet.write(i, 43, buscar_discapacidad('DISC_VISUAL', discapacidad),body_format)
                        sheet.write(i, 44, buscar_discapacidad('DISC_OIR', discapacidad), body_format)
                        sheet.write(i, 45, buscar_discapacidad('DISC_HABLAR', discapacidad), body_format)
                        sheet.write(i, 46, buscar_discapacidad('DISC_EXTREMIDADES', discapacidad), body_format)
                        sheet.write(i, 47, buscar_discapacidad('DISC_MENTAL', discapacidad), body_format)
                        seguro = menor.afi_ninio_id.integrante_id.seguro_id
                        sheet.write(i, 48, seguro.name if seguro else '' , body_format)
                        sheet.write(i, 49, menor.afi_ninio_id.integrante_id.validado_reniec, body_format)
                        sheet.write(i, 50, menor.afi_ninio_id.integrante_id.usuario_creador_id.name, body_format)
                        sheet.write(i, 51, menor.asiste, body_format)
                        sheet.write(i, 52, menor.enfermedad, body_format)
                        sheet.write(i, 53, menor.faltas, body_format)
                        sheet.write(i, 54, menor.totfaleda, body_format)
                        sheet.write(i, 55, menor.totfalira, body_format)
                        sheet.write(i, 56, menor.totfalotr, body_format)
                        sheet.write(i, 57, 1 if menor.cred else 0, body_format)
                        sheet.write(i, 58, 1 if menor.mn else 0, body_format)
                        sheet.write(i, 59, asis.fecha_creacion, body_format)
                        sheet.write(i, 60, asis.fecha_mod, body_format)
                        sheet.write(i, 61, lines.fecha_creacion, body_format)
                        tamizajes = self.env['tamizaje.anemia'].search([
                            ('ninio_id', '=', menor.afi_ninio_id.integrante_id.id)], order='fecha_examen desc')
                        sheet.write(i, 62, tamizajes[0].id if tamizajes else '', body_format)
                        sheet.write(i, 63, asis.sala_id.id, body_format)
                        sheet.write(i, 64, menor.ninio_afiliacion_id.fecha_inicio, body_format)
                        sheet.write(i, 65, menor.afi_ninio_id.integrante_id.id, body_format)
                        sheet.write(i, 66, 'TA', body_format)
                        sheet.write(i, 67, tamizajes[0].fecha_examen if tamizajes else '', body_format)
                        sheet.write(i, 68, tamizajes[0].edad.examen if tamizajes else '', body_format)
                        sheet.write(i, 69, tamizajes[0].diagnostico if tamizajes else '', body_format)
                        sheet.write(i, 70, tamizajes[0].resultado if tamizajes else '', body_format)
                        sheet.write(i, 71, tamizajes[0].fecha_creacion if tamizajes else '', body_format)
                        tot_ta = len(tamizajes)
                        sheet.write(i, 72, tamizajes[tot_ta - 1].fecha_creacion if tamizajes else '', body_format)
                        sheet.write(i, 73, tot_ta, body_format)
                        sheet.write(i, 74, 'SI' if tamizajes else '', body_format)
                        control_cred = tamizajes[0].ficha_salud_id.control_cred_ids if tamizajes else ''
                        sheet.write(i, 75, control_cred[len(control_cred) - 1].talla_cm if control_cred else '',
                                    body_format)
                        sheet.write(i, 76, control_cred[len(control_cred) - 1].peso_kg if control_cred else '',
                                    body_format)
                        sheet.write(i, 77, control_cred[len(control_cred) - 1].talla_edad if control_cred else '',
                                    body_format)
                        sheet.write(i, 78, control_cred[len(control_cred) - 1].peso_talla if control_cred else '',
                                    body_format)
                        sheet.write(i, 79, control_cred[len(control_cred) - 1].peso_edad if control_cred else '',
                                    body_format)
                        pad = menor.afi_ninio_id.familia_id.integrantes_ids.filtered(lambda x: x.parentesco_id.name in
                                                                                     ['PADRE', 'MADRE'])
                        sheet.write(i, 80, pad[0].integrante_id.id if pad else '', body_format)
                        sheet.write(i, 81, pad[0].integrante_id.seguro_id.name if pad else '', body_format)
                        sheet.write(i, 82, pad[0].integrante_id.type_document if pad else '', body_format)
                        sheet.write(i, 83, pad[0].integrante_id.document_number if pad else '', body_format)
                        sheet.write(i, 84, pad[0].integrante_id.firstname if pad else '', body_format)
                        sheet.write(i, 85, pad[0].integrante_id.secondname if pad else '', body_format)
                        sheet.write(i, 86, pad[0].integrante_id.name if pad else '', body_format)
                        sheet.write(i, 87, pad[0].integrante_id.gender if pad else '', body_format)
                        sheet.write(i, 88, pad[0].integrante_id.birthdate if pad else '', body_format)
                        sheet.write(i, 89, pad[0].parentesco_id.name if pad else '', body_format)
                        sheet.write(i, 90, pad[0].integrante_id.marital_status if pad else '', body_format)
                        sheet.write(i, 91, pad[0].integrante_id.fecha_creacion if pad else '', body_format)
                        i += 1


AsistenciaXls('report.base_cuna.AsistenciaXls.xlsx', 'dynamic.report.wizard')


class RequerimientoAsistenciaXls(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        body_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'border': True,
        })
        title_blue_dark_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#7289da',
            'border': True,
            'font_color': '#ffffff'
        })
        title_green_forest_font_black_format = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'bold': True,
            'size': 7,
            'bg_color': '#5cb85c',
            'border': True
        })
        body_green_percentage = workbook.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': 'vjustify',
            'size': 5,
            'bg_color': '#33cc33',
            'border': True,
            'num_format': '0.00%',
            'font_color': '#ffffff'
        })
        sheet = workbook.add_worksheet('RESUMEN')
        if lines.unidad_territorial_id:
            uts = lines.unidad_territorial_id
        else:
            uts = obtener_todas_las_ut(self, lines.servicio)
        anio = lines.anio
        mes = lines.mes
        nombre_mes = str(dict(lines._fields['mes'].selection).get(lines.mes))
        sheet.merge_range('A1:K1', 'SEGUIMIENTO AL REGISTRO DE USUARIOS DEL SERVICIO DE CUIDADO DIURNO- %s %s' %
                          (nombre_mes, anio.name), title_green_forest_font_black_format)
        sheet.merge_range('A2:A3', 'N', title_blue_dark_format)
        sheet.merge_range('B2:B3', 'UNIDAD TERRITORIAL', title_blue_dark_format)
        sheet.merge_range('C2:F2', 'COMITÉ DE GESTION', title_blue_dark_format)
        sheet.write(2, 2, 'N° DE CG REQ.', title_blue_dark_format)
        sheet.write(2, 3, 'N° CG CON ASISTENCIA REGISTRADA', title_blue_dark_format)
        sheet.write(2, 4, 'FALTA X CG', title_blue_dark_format)
        sheet.write(2, 5, '% AVANCE', title_blue_dark_format)
        sheet.merge_range('G2:K2', 'USUARIOS', title_blue_dark_format)
        sheet.write(2, 6, 'META %s %s' % (nombre_mes, anio.name), title_blue_dark_format)
        sheet.write(2, 7, 'N° USUARIOS REQ', title_blue_dark_format)
        sheet.write(2, 8, 'N° DE USUARIOS REGISTRADOS', title_blue_dark_format)
        sheet.write(2, 9, 'DIF USU (REQ - REG)', title_blue_dark_format)
        sheet.write(2, 10, '% AVANCE (REG / REQ)', title_blue_dark_format)

        sheet2 = workbook.add_worksheet('DATA')
        sheet2.merge_range('A2:S2', 'SEGUIMIENTO AL REGISTRO DE USUARIOS DEL SERVICIO DE CUIDADO DIURNO- %s %s' %
                           (nombre_mes, anio.name), title_green_forest_font_black_format)
        sheet2.merge_range('A3:B3', 'Reporte Fecha: %s' % lines.fecha_creacion, title_blue_dark_format)
        sheet2.merge_range('A4:H4', 'TOTAL', title_green_forest_font_black_format)
        sheet2.write(4, 0, 'N', title_blue_dark_format)
        sheet2.write(4, 1, 'UT', title_blue_dark_format)
        sheet2.write(4, 2, 'UBIGUEO', title_blue_dark_format)
        sheet2.write(4, 3, 'DEPARTAMENTO', title_blue_dark_format)
        sheet2.write(4, 4, 'PROVINCIA', title_blue_dark_format)
        sheet2.write(4, 5, 'DISTRITO', title_blue_dark_format)
        sheet2.write(4, 6, 'CODIGO CUNANET', title_blue_dark_format)
        sheet2.write(4, 7, 'NOMBRE DEL COMITE', title_blue_dark_format)
        sheet2.write(4, 8, 'USUARIOS REQUERIMIENTOS', title_blue_dark_format)
        sheet2.write(4, 9, 'USUARIOS REGISTRADOS', title_blue_dark_format)
        sheet2.write(4, 10, 'DIFERENCIAS USUARIOS', title_blue_dark_format)
        sheet2.write(4, 11, '% USUARIOS AVANCE (REG./REQ.)', title_blue_dark_format)
        sheet2.write(4, 12, 'CG CON REQ.', title_blue_dark_format)
        sheet2.write(4, 13, 'CG CON ASISTENCIA', title_blue_dark_format)
        sheet2.write(4, 14, '% CG AVANCE (REG/REQ)', title_blue_dark_format)
        sheet2.write(4, 15, 'AÑO', title_blue_dark_format)
        sheet2.write(4, 16, 'MES', title_blue_dark_format)
        sheet2.write(4, 17, 'FECHA REPORTE', title_blue_dark_format)
        sheet2.set_row(2, 25)
        sheet2.set_row(4, 30)
        i = 5
        for ut in uts:
            for cg in ut.ut_comites_gestion.filtered(lambda x: x.state == 'activo' and x.servicio == 'scd'):
                nro_ninios = 0
                for local in cg.local_ids:
                    for modulo in local.modulos_ids:
                        for ninio in modulo.ninios_ids:
                            dias = obtener_dias_asistencia(ninio, anio, mes)
                            if dias:
                                nro_ninios += 1
                sheet2.write(i, 0, i - 4, body_format)
                sheet2.write(i, 1, ut.name, body_format)
                sheet2.write(i, 2, cg.centro_poblado_id.code, body_format)
                sheet2.write(i, 3, cg.departamento_id.name, body_format)
                sheet2.write(i, 4, cg.provincia_id.name, body_format)
                sheet2.write(i, 5, cg.distrito_id.name, body_format)
                sheet2.write(i, 6, cg.id, body_format)
                sheet2.write(i, 7, cg.name, body_format)
                req = cg.requerimientos_ids.filtered(lambda x: x.anio.id == anio.id and x.mes == mes)
                nro_atendidos = req.desembolso_id.nro_usuarios_atendidos if req else 0
                sheet2.write(i, 8, nro_atendidos, body_format)
                sheet2.write(i, 9, nro_ninios, body_format)
                sheet2.write(i, 10, nro_atendidos - nro_ninios, body_format)
                sheet2.write(i, 11, nro_ninios / nro_atendidos if nro_atendidos > 0 else 0, body_green_percentage)
                x_req = 1 if req else 0
                sheet2.write(i, 12, x_req, body_format)
                asist = self.env['asistencia.control'].search([
                    ('comite_gestion_id', '=', cg.id),
                    ('anio', '=', anio.id),
                    ('mes', '=', mes)
                ])
                x_asis = 1 if asist else 0
                sheet2.write(i, 13, x_asis, body_format)
                sheet2.write(i, 14, x_asis / x_req if x_req > 0 else 0, body_green_percentage)
                sheet2.write(i, 15, anio.name, body_format)
                sheet2.write(i, 16, mes, body_format)
                sheet2.write(i, 17, lines.fecha_creacion, body_format)
                i += 1

        j = 3
        sheet.set_row(2, 25)
        for ut in uts:
            sheet.write(j, 0, j - 2, body_format)
            sheet.write(j, 1, ut.name, body_format)
            for ut in uts:
                req = asist_c = meta = 0
                for cg in ut.ut_comites_gestion.filtered(lambda x: x.state == 'activo' and x.servicio == 'scd'):
                    meta += int(cg.meta_actual)
                    if cg.requerimientos_ids.filtered(lambda x: x.anio.id == anio.id and x.mes == mes):
                        req += 1
                    asist = self.env['asistencia.control'].search([
                        ('comite_gestion_id', '=', cg.id),
                        ('anio', '=', anio.id),
                        ('mes', '=', mes)
                    ])
                    if asist:
                        asist_c += 1
                sheet.write(j, 0, j - 2, body_format)
                sheet.write(j, 1, ut.name, body_format)
                sheet.write(j, 2, req, body_format)
                sheet.write(j, 3, asist_c, body_format)
                sheet.write(j, 4, asist_c - req, body_format)
                sheet.write(j, 5, asist_c / req if req > 0 else 0, body_green_percentage)
                sheet.write(j, 6, meta, body_format)
                sheet.write(j, 7, "=SUMIFS('DATA'!I:I,'DATA'!B:B,B%s" % str(j + 1), body_format)
                sheet.write(j, 8, "=SUMIFS('DATA'!J:J,'DATA'!B:B,B%s" % str(j + 1), body_format)
                sheet.write(j, 9, "=H%d-I%d" % (j + 1, j + 1), body_format)
                sheet.write(j, 10, "=IFERROR(I%d/H%d,0)" % (j + 1, j + 1), body_format)
                j += 1


RequerimientoAsistenciaXls('report.base_cuna.RequerimientoAsistenciaXls.xlsx', 'dynamic.report.wizard')
