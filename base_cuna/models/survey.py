# -*- coding: utf-8 -*-

from odoo import api, fields, models
import logging

_logger = logging.getLogger(__name__)


class SurveyQuestion(models.Model):
    _inherit = 'survey.question'

    conditional = fields.Boolean(
        string='¿Pregunta Condicional?',
        copy=False,
        # we add copy = false to avoid wrong link on survey copy,
        # should be improoved
    )
    question_conditional_id = fields.Many2one(
        comodel_name='survey.question',
        string='Preguntas',
        copy=False,
        help="In order to edit this field you should first save the question"
    )
    answer_id = fields.Many2one(
        comodel_name='survey.label',
        string='Respuesta',
        copy=False,
    )

    @api.multi
    def validate_question(self, post, answer_tag):
        try:
            checker = getattr(self, 'validate_' + self.type)
        except AttributeError:
            _logger.warning(self.type + ": This type of question has no validation method")
            return {}
        else:
            if not self.question_conditional_id:
                return checker(post, answer_tag)
            input_answer_id = self.env['survey.user_input_line'].search([
                ('user_input_id.token', '=', post.get('token')),
                ('question_id', '=', self.question_conditional_id.id)
            ])
            for answers in input_answer_id:
                value_suggested = answers.value_suggested
                if self.conditional and self.answer_id != value_suggested:
                    return {}
                else:
                    return checker(post, answer_tag)


class SurveyUserInput(models.Model):

    _inherit = 'survey.user_input'

    def get_list_questions(self, cr, uid, survey, user_input_id):

        obj_questions = self.env['survey.question']
        obj_user_input_line = self.env['survey.user_input_line']
        questions_to_hide = []
        question_ids = obj_questions.search([('survey_id', '=', survey.id)])
        for question in question_ids:
            if question.conditional:
                for question2 in question_ids:
                    if question2 == question.question_conditional_id:
                        input_answer_id = obj_user_input_line.search([
                            ('user_input_id', '=', user_input_id.id),
                            ('question_id', '=', question2.id)
                        ])
                        for answers in input_answer_id:
                            value_suggested = answers.value_suggested
                            if question.answer_id != value_suggested:
                                questions_to_hide.append(question.id)
        return questions_to_hide
