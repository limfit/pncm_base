odoo.define('base_cuna.SurveyWidget', function(require) {
"use strict";

var core = require('web.core');
var form_common = require('web.form_common');
var Widget = require('web.Widget');
var Model = require('web.Model');
var QWeb = core.qweb;

var SurveyWidget = form_common.AbstractField.extend({
    template: 'SurveyWidget',

    init: function() {
        var self = this;
        this._super.apply(this, arguments);
        this.pregunta_validadas = false;
        this.field_manager.on("view_content_has_changed", this, function () {
            this.model_name = self.get_model_name();
            if(this.model_name == 'ficha.fortalecimiento'){
                this.FichaModel = new Model('ficha.fortalecimiento');
            }
            if(this.model_name == 'ficha.reconocimiento'){
                this.FichaModel = new Model('ficha.reconocimiento');
            }
            if(this.model_name == 'ficha.gestante'){
                this.FichaModel = new Model('ficha.gestante');
            }
            this.record_id = self.get_record_id();

            if(this.get("effective_readonly")){
                self.get_plantilla(this.record_id, false);
            }
            else{
                self.get_plantilla(this.record_id, true);
            }

            var save_button = document.getElementsByClassName('o_form_button_save');
            var new_save = $('<button class="btn btn-primary btn-sm especial" valor="new_save" type="button"/>');
            if(save_button && $(save_button).is(':visible')){
                $(save_button).hide();
                $(new_save).insertBefore($(save_button));
                $(new_save).html('Guardar');
                $(new_save).show();
                $(new_save).bind("click", function(){
                    self.on_button_save();
                    if(self.get_pregunta_validadas()){
                        $(save_button).trigger("click");
                        self.set("effective_readonly", true);
                        $(".especial").remove();
                    }
                });
            }
        });
    },
    start: function() {
        var self = this;
        var crear_button = document.getElementsByClassName('nuevo');
        var save_button = document.getElementsByClassName('o_form_button_save');
        this.on("change:effective_readonly", this, function() {
            if(!this.get("effective_readonly")){
                $(save_button).hide();
                var new_save = $('<button class="btn btn-primary btn-sm especial" valor="new_save" type="button"/>');
                $(new_save).insertBefore($(save_button));
                $(new_save).html('Guardar');
                $(new_save).show();
                $(new_save).bind("click", function(){
                    self.on_button_save();
                    if(self.get_pregunta_validadas()){
                        $(save_button).trigger("click");
                        self.set("effective_readonly", true);
                    }
                });
                self.set_pregunta_validadas(false);
            }else{
                $(".especial").remove();
            }
            self.render_value();
        });
        return this._super();
    },
    set_pregunta_validadas: function(valor){
        this.pregunta_validadas = valor;
    },
    get_pregunta_validadas: function(){
        return this.pregunta_validadas;
    },
    render_value: function() {
        var estado = this.get_record_estado();
        if(this.get("effective_readonly")) {
            this.$("input").each(function(index, element){
                $(element).prop('disabled', true);
            });
            this.$("select").each(function(index, element){
                $(element).prop('disabled', true);
            });
        }else{
            this.$("input").each(function(index, element){
                if(estado == 'abierto'){
                    $(element).prop('disabled', false);
                }
            });
            this.$("select").each(function(index, element){
                if(estado == 'abierto'){
                    $(element).prop('disabled', false);
                }
            });
        }
    },
    get_record_id: function () {
        var active_id = this.view.datarecord.id;
        return active_id;
    },
    get_model_name: function () {
        var active_id = this.view.datarecord.model_name;
        return active_id;
    },
    get_record_estado: function () {
        var active_id = this.view.datarecord.estado;
        return active_id;
    },
    get_plantilla: function(record, editable){
        var res;
        var self = this;
        var estado = self.get_record_estado();
        this.FichaModel.call("get_record",[record]).then(function(result){
            res = result[0];
            $('#tablas-visita-div').empty().append(res.plantilla).removeClass('o_form_field_empty');
            $('#tablas-visita-preg-div').empty().append(res.preguntas);
            if(res.nro_visitas > 0){
                for (var y = 0; y < res.nro_visitas; y++) {
                    var tblBodyObj = document.getElementById('tbl_visitas').tBodies[0];
                    var tblBodyObj1 = document.getElementById('tbl_visitas_respuestas').tBodies[0];
                    var row = tblBodyObj1.rows.length;

                    for (var x = 0; x < row; x++) {
                        var newchkbxcell = tblBodyObj.rows[x].insertCell();
                        var $cloned = $(tblBodyObj1.rows[x]).clone();

                        if($cloned.attr('id') == "cabecera"){
                            $cloned.attr('id', `visita_${y}`);
                            $cloned.html(`VISITA N° ${y+1}&nbsp;`);
                        }

                        if($cloned.find('input')){
                            $cloned.find('input').each(function(index, element){
                                var name = $(element).attr('name');
                                $(element).attr('name', `${name}${y}`);

                                if($(element).attr('padre')){
                                    var pname = $(element).attr('padre');
                                    $(element).attr('padre', `${pname}${y}`);
                                    self.PreguntaDinamica($(element).attr('padre'), 'padre');
                                }
                                if($(element).attr('subp')){
                                    var pname = $(element).attr('subp');
                                    $(element).attr('subp', `${pname}${y}`);
                                    self.PreguntaDinamica($(element).attr('subp'), 'subp');
                                }
                                if(editable === false){
                                    $(element).prop('disabled', true);
                                }
                            });
                        }
                        if($cloned.find('select')){
                            $cloned.find('select').each(function(index, element){
                                var name = $(element).attr('name');
                                $(element).attr('name', `${name}${y}`);

                                if($(element).attr('padre')){
                                    var pname = $(element).attr('padre');
                                    $(element).attr('padre', `${pname}${y}`);
                                    self.PreguntaDinamica($(element).attr('padre'), 'padre');
                                }
                                if(editable === false){
                                    $(element).prop('disabled', true);
                                }
                            });
                        }
                        $cloned.appendTo(newchkbxcell);
                    }
                    $(".cab-ficha").each(function(index, element){
                        if($(element).attr('cabecera') == 'S' )
                            $(element).parent().attr('align', 'center');
                    });
                }

                for (var z = 0; z < res.respuestas.length; z++) {
                    var input_line = $('[name="' + res.respuestas[z]['nombre'] + '"]');
                    $(input_line).each(function(index, element){
                        if(res.respuestas[z]['tipo'] == 'dato'){
                            $(element).val(res.respuestas[z]['value'])
                        }else if(res.respuestas[z]['tipo'] == 'select'){
                            $(element).trigger('change');
                            $(element).val(res.respuestas[z]['value']).prop('selected', true);
                            if(editable === false){
                                    $(element).prop('disabled', true);
                                }
                        }else if(res.respuestas[z]['tipo'] == 'matrix'){
                            $(element).trigger('change');
                            if($(element).attr('value') == res.respuestas[z]['value'])
                                $(element).prop('checked', true);
                                if(editable === false){
                                    $(element).prop('disabled', true);
                                }
                        }else{
                            $(element).trigger('change');
                            if($(element).attr('value') == res.respuestas[z]['value'])
                                $(element).prop('checked', true);
                                if(editable === false){
                                    $(element).prop('disabled', true);
                                }
                        }
                    });
                }
            }
        });
    },
    PreguntaDinamica: function(nombre, valueb){
		// Pregunta padre
        var pregunta = $('[name="' + nombre + '"]');

        $(pregunta).change(function(){
            var valorPregunta1 = $('[name="' + nombre + '"]:checked').attr('value');
            if(valorPregunta1 == undefined || valorPregunta1 == null || valorPregunta1 == ''){
                return false;
            }
            var value = $(pregunta).attr('value');
            if(valueb == 'padre'){
                $('[padre="' + nombre + '"]').each(function(index, element){
                    if($(element).attr('value_r') != valorPregunta1){
                        $(element).parent().parent().parent().addClass('tdFocus');
                        $(element).prop('disabled', true);
                        if($(element).attr('tipo') == 'selection'){
                            $(element).val('').prop('selected', true);
                        }else
                            $(element).prop('checked', false);
                    }
                    else{
                        $(element).parent().parent().parent().removeClass('tdFocus');
                        $(element).prop('disabled', false);
                    }
                });
            }
            else{
                $('[subp="' + nombre + '"]').each(function(index, element){
                    if($(element).attr('value').toUpperCase() != valorPregunta1.toUpperCase()){
                        $(element).prop('checked', true);
                        $(element).prop('disabled', false);
                    }else{
                        $(element).prop('disabled', true);
                    }
                });
            }
        });
	},
	on_button_save: function() {
        var save_data = [];
        var fechas_visitas = [];
        var self = this;
        var nro_visitas = document.getElementById('tbl_visitas').rows[0].cells.length;
        if(nro_visitas > 1){
            for (var y = 0; y < (nro_visitas - 1); y++){
                this.$("input").each(function(index, element){
                    var name = $(element).attr('name');
                    var pos_name = name.lastIndexOf("_");
                    var visita_n = name.substring(pos_name + 1);

                    if(visita_n != '' && visita_n == y){
                        if($(element).attr('tipo') == 'date'){
                            if($(element).attr('fechav') && $(element).val()){
                                fechas_visitas.push($(element).val());
                            }
                            if($(element).val()){
                                save_data.push({
                                    'nro_visita': y,
                                    'id_pregunta': $(element).attr('idp'),
                                    'value': $(element).val(),
                                    'tipo': 'date'
                                });
                            }
                        }
                        if($(element).attr('tipo') == 'numerical_box'){
                            if($(element).val()){
                                save_data.push({
                                    'nro_visita': y,
                                    'id_pregunta': $(element).attr('idp'),
                                    'value': $(element).val(),
                                    'tipo': 'numerical_box'
                                });
                            }
                        }
                        if($(element).attr('tipo') == 'textbox'){
                            if($(element).val()){
                                save_data.push({
                                    'nro_visita': y,
                                    'id_pregunta': $(element).attr('idp'),
                                    'value': $(element).val(),
                                    'tipo': 'textbox'
                                });
                            }
                        }
                        if($(element).attr('tipo') == 'simple_choice' && $(element).prop('checked')){
                            save_data.push({
                                'nro_visita': y,
                                'id_pregunta': $(element).attr('idp'),
                                'id_seleccion': $(element).attr('idh'),
                                'tipo': 'simple_choice'
                            });
                        }
                        if($(element).attr('tipo') == 'matrix' && $(element).prop('checked')){
                            save_data.push({
                                'nro_visita': y,
                                'id_pregunta': $(element).attr('idp'),
                                'id_seleccion': $(element).attr('idh'),
                                'value_row': $(element).attr('idr'),
                                'tipo': 'matrix'
                            });
                        }
                        if($(element).attr('tipo') == 'multiple_choice' && $(element).prop('checked')){
                            save_data.push({
                                'nro_visita': y,
                                'id_pregunta': $(element).attr('idp'),
                                'id_seleccion': $(element).attr('idh'),
                                'tipo': 'multiple_choice'
                            });
                        }

                    }
                });
                this.$("select").each(function(index, element){
                    var name = $(element).attr('name');
                    var pos_name = name.lastIndexOf("_");
                    var visita_n = name.substring(pos_name + 1);

                    if(visita_n ! = '' && visita_n == y){
                        if($(element).attr('tipo') == 'selection'){
                            if($(element).find(':selected')){
                                if($(element).find(':selected').attr('idh')){
                                    save_data.push({
                                        'nro_visita': y,
                                        'id_pregunta': $(element).find(':selected').attr('idp'),
                                        'id_seleccion': $(element).find(':selected').attr('idh'),
                                        'tipo': 'simple_choice'
                                    });
                                }
                            }
                        }
                    }
                });
            }

            var rec = this.get_record_id();
            var ficha = this.FichaModel;
        
            ficha.call("validar_fecha_visitas",[rec, fechas_visitas]).done(function(result){
                if(!result['value']){
                    self.set_pregunta_validadas(false);
                    alert(result['res']);
                }
                else{
                    ficha.call("guardar_datos_preguntas",[rec, save_data]);
                    self.set_pregunta_validadas(true);
                }
            });
        }else{
            self.set_pregunta_validadas(true);
        }
    },
});
    core.form_widget_registry.add('survey', SurveyWidget);
    return {
        SurveyWidget: SurveyWidget,
    };
});


