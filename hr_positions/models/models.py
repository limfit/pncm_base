# -*- coding: utf-8 -*-

from openerp import fields, models


class CMPositions(models.Model):
    _name = 'hr.positions'

    name = fields.Char(u'Puesto de Trabajo')



class HREmployee(models.Model):
    _inherit = 'hr.employee'
    position_id = fields.Many2one(
        'hr.positions',
        u'Puesto de Trabajo'
    )
    equipo_tecnico = fields.Boolean(string=u'Equipo Tecnico')
