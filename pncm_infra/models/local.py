# -*- coding: utf-8 -*-
from openerp import fields, models, api
from openerp.exceptions import ValidationError
from odoo.addons.pncm_comite_gestion.models import utils
import json


class CMLocal(models.Model):
    _name = 'pncm.infra.local'
    _prefijo_codigo = 'LC'
    _completar_con_zeros = '05'

    # Identificacion generales
    ut_id = fields.Many2one(
        'hr.department',
        u'Unidad Territorial',
        domain="[('services', '=', 'scd'), ('is_office', '=', True)]",
        required=True
    )
    comite_id = fields.Many2one(
        'pncm.comitegestion',
        string=u'Comité de gestión',
        required=True,
        domain="[('unidad_territorial_id', '=', ut_id), "
               "('state', '=', 'activo')]")
    code = fields.Char(
        string=u'Código de Local',
        required=True,
        size=40,
        readonly=True,
        default=lambda self: self._generar_codigo_local(),
        store=True
    )
    name = fields.Char(string=u'Nombre', size=250)
    state = fields.Selection(
        selection=[
            ('identificacion', u'Identificación'),
            ('saneamiento', u'Saneamiento'),
            ('activacion', u'Activo'),
            ('suspension', u'Suspensión'),
            ('baja', u'Baja')
        ],
        default="identificacion",
        required=True,
        string=u'Estado'
    )
    atiende_ninios = fields.Boolean(
        string=u'Atiende Niños',
        required=True,
        readonly=True
    )
    tipo_local = fields.Selection(
        selection=[
            ('ciai', u'CIAI'),
            ('sa', u'SA'),
            ('ambos', u'Ambos (CIAI y SA)')
        ],
        required=True
    )
    modalidad_intervencion = fields.Selection(
        selection=[
            ('amplicacion_cob', u'Amplicación de Cobertura'),
            ('migracion', u'Migración'),
            ('emergencia', u'Emergencia'),
        ],
        required=True,
        string=u'Modalidad de Intervención'
    )
    procedencia = fields.Selection(
        selection=[
            ('municipal', u'Municipal'),
            ('comunal', u'Comunal'),
            ('privado', u'Privado'),
            ('parroquial', u'Parroquial'),
            ('educativo', u'Educativo'),
            ('pncm', u'PNCM'),
            ('otro', u'Otro')
        ],
        required=True
    )

    propietario_id = fields.Many2one(
        'res.partner',
        string=u'Propietario/Poseedor del Local',
        required=True)

    # Identificacion ubicación
    pais_id = fields.Many2one(
        'res.country',
        'País',
        default=lambda self: self.env.user.company_id.country_id
    )
    departamento_id = fields.Many2one(
        'res.country.state', 'Departamento',
        required=True,
        domain="[('country_id', '=', pais_id), ('state_id', '=', False), "
               "('province_id', '=', False)]"
    )
    provincia_id = fields.Many2one(
        'res.country.state', 'Provincia',
        required=True,
        domain="[('state_id', '=', departamento_id), ('province_id', '=', "
               "False)]"
    )
    distrito_id = fields.Many2one(
        'res.country.state', 'Distrito',
        required=True,
        domain="[('state_id', '=', departamento_id), ('province_id', '=', "
               "provincia_id)]"
    )
    centro_poblado_id = fields.Many2one(
        'res.country.state',
        u'Centro Poblado',
        domain="[('state_id', '=', departamento_id), ('province_id', '=', "
               "provincia_id), ('district_id', '=', distrito_id)]",
        required=True
    )
    direccion = fields.Char(string=u'Dirección', required=True, size=255)
    referencia = fields.Text(string=u'Referencia')
    lat = fields.Char(string=u'Latitud')
    lng = fields.Char(string=u'Longitud')
    map = fields.Char(
        string=u'Mapa',
        compute='_obtener_punto_mapa',
        store=False
    )

    # Archivos del Local
    fotos = fields.One2many(
        'pncm.infra.local.foto',
        'local_id',
        string=u'Fotos del Local'
    )
    fichas_preevaluacion = fields.One2many(
        'pncm.infra.local.preevaluacion',
        'local_id',
        string=u'Fichas de Pre-Evaluación',
        required=True
    )
    fichas_informe = fields.One2many(
        'pncm.infra.local.informes',
        'local_id',
        string=u'Fichas de Informe',
        required=True
    )

    # Medida Perimetricas
    area_total_terreno = fields.Float(string=u'Área total terreno (m2)')
    area_cedida = fields.Float(string=u'Área cedida (m2)')
    area_construida = fields.Float(string=u'Area construida (m2)')
    anio_construccion = fields.Integer(string=u'Año construcción')
    nro_sshh = fields.Integer(string=u'N° de SSHH')

    # Linderos
    lindero_frente = fields.Char(string=u'Por el frente', size=255)
    lindero_derecha = fields.Char(
        string=u'Entrando por la derecha',
        size=255
    )
    lindero_izquierda = fields.Char(
        string=u'Entrando por la izquierda',
        size=255
    )
    lindero_fondo = fields.Char(
        string=u'Por el fondo',
        size=255
    )

    # Otros/
    posibilid_ampliacion = fields.Boolean(
        string=u'Posibilidad de ampliación del local'
    )
    es_servicio_alimentario = fields.Boolean(string=u'Es servicio alimentario')

    # Datos RD Comite Gestión (Tentativo)
    rde_cg = fields.Char(string=u'N° RDE de la JD del CG', store=False)
    rde_fecha_inicio = fields.Date(string=u'Fecha inicio de vigencia del RDE',
                                   store=False)
    rde_fecha_fin = fields.Date(string=u'Fecha termino de vigencia del RDE',
                                store=False)
    rde_vigencia = fields.Char(string=u'Vigencia de la JD del CG (meses)',
                               store=False)

    # Documento sustentatorio lectura
    tipo_doc_sustentatorio = fields.Selection(
        string=u'Tipo Doc. Sustentatorio',
        selection=[
            ('afectacion', u'Acta de Afectación'),
            ('convenio', u'Convenio')
        ]
    )

    # Local Alquilado
    es_alquilado = fields.Boolean(
        string=u'¿Es local alquilado?'
    )
    fecha_inicio_alquiler = fields.Date(string=u'Fecha inicio contrato')
    fecha_fin_alquiler = fields.Date(string=u'Fecha termino contrato')
    monto_alquiler = fields.Float(string=u'Monto contrato')

    # Documento Sustentatorio escritura
    convenios = fields.One2many(
        'pncm.infra.local.convenios_sustentatorio',
        'local_id',
        string=u'Convenios',
        required=True)

    afectaciones = fields.One2many(
        'pncm.infra.local.actas_afectacion',
        'local_id',
        string=u'Afectaciones',
        required=True)

    # Presupuesto
    presupuestos = fields.One2many(
        'pncm.infra.local.presupuesto',
        'local_id',
        string=u'Presupuesto'
    )

    # activacion
    fecha_activacion = fields.Date(u'Fecha de Activación')
    es_activado = fields.Boolean(
        string=u'Local activado'
    )

    # Suspension
    motivo_suspension = fields.Char(
        string=u'Motivo'
    )
    fecha_inicio_suspension = fields.Date(
        string=u'Fecha de inicio'
    )
    fecha_fin_suspension = fields.Date(
        string=u'Fecha de Fin'
    )
    suspensiones = fields.One2many(
        'pncm.infra.suspension.local',
        'local_id',
        string=u'Suspensiones',
        store=True
    )

    # Baja de locales
    fecha_baja = fields.Date(u'Fecha de Baja')
    motivo_baja = fields.Selection(
        selection=[
            ('migracion', u'MIGRACIÓN'),
            ('poca_demanda', u'POCA DEMANDA DE NIÑOS'),
            ('no_viable', u'LOCAL NO VIABLE'),
            ('finaliza_convenio', u'FINALIZACIÓN DE CONVENIO Y/O CONTRATO'),
            ('otros', u'OTROS')
        ]
    )
    archivo_sustentatorio = fields.Binary(
        string=u'Documento Sustentatorio',
        help=u'Buscar documento en mi computadora',
        size_limit=4

    )
    nombre_archivo_sustentatorio_baja = fields.Char(
        string=u'Archivo',
        store=True
    )

    # Codigo de almacen que se genera en el modulo de menu para los
    # servicios alimentarios
    codigo_almacen = fields.Char(
        string=u'Código de Almacen',
        size=5,
        store=True
    )

    _sql_constraint = [
        ('code_unique', 'unique(code)', u'El código ya existe')
    ]

    @api.model
    def create(self, values):
        values['code'] = self._generar_codigo_local(values)
        if values['name']:
            values['name'] = values['name'].upper()
        if values['direccion']:
            values['direccion'] = values['direccion'].upper()
        if values['referencia']:
            values['referencia'] = values['referencia'].upper()
        return super(CMLocal, self).create(values)

    @api.multi
    def write(self, values):
        ctx = dict(self._context or {})
        if ('name' in values) and values['name']:
            values['name'] = values['name'].upper()
        if ('direccion' in values) and values['direccion']:
            values['direccion'] = values['direccion'].upper()
        if ('referencia' in values) and values['referencia']:
            values['referencia'] = values['referencia'].upper()

        if 'fecha_activacion' in values:
            result = self._validar_fecha_activacion(values['fecha_activacion'])
            if result['success']:
                values['es_activado'] = True
            else:
                values['es_activado'] = False

        if self.state == 'suspension':
            if not ('no_validar_suspension' in values):
                if self._validar_ultima_suspension(values):
                    self._agregar_al_historico_ultima_suspension(values)

        return super(CMLocal, self.with_context(ctx)).write(values)

    def _agregar_al_historico_ultima_suspension(self, values):
        motivo = fi = ff = False
        if 'motivo_suspension' in values\
                and values['motivo_suspension']:
            motivo = values['motivo_suspension'].upper()
            values['motivo_suspension'] = False

        if 'fecha_inicio_suspension' in values\
                and values['fecha_inicio_suspension']:
            fi = values['fecha_inicio_suspension']
            values['fecha_inicio_suspension'] = False

        if 'fecha_fin_suspension' in values\
                and values['fecha_fin_suspension']:
            ff = values['fecha_fin_suspension']
            values['fecha_fin_suspension'] = False

        values['suspensiones'] = [(0, 0, {
            'local_id': self.id,
            'motivo': motivo,
            'fecha_inicio': fi,
            'fecha_fin': ff
        })]

    def _validar_ultima_suspension(self, values):
        message = u'Ingrese todos los datos de la ultima suspensión'
        if not ('motivo_suspension' in values):
            raise ValidationError(message)
            return False
        if not ('fecha_inicio_suspension' in values):
            raise ValidationError(message)
            return False
        if not ('fecha_fin_suspension' in values):
            raise ValidationError(message)
            return False

        dhelper = utils.DateHelper()
        if not dhelper._validar_fecha_mayor(self.fecha_activacion,
                                            values['fecha_inicio_suspension'],
                                            u'La fecha de inicio '
                                            u'de suspensión debe ser mayor '
                                            u'a la fecha de activación del '
                                            u'local'):
            return False

        if not dhelper._validar_fecha_mayor(values['fecha_inicio_suspension'],
                                            values['fecha_fin_suspension'],
                                            u'La fecha de fin debe ser mayor '
                                            u'a la fecha de inicio, en la '
                                            u'ultima suspensión'):
            return False

        if self.suspensiones and len(self.suspensiones) > 0:
            for r in self.suspensiones:
                if not dhelper.\
                        _validar_fecha_mayor(r.fecha_fin,
                                             values['fecha_inicio_suspension'],
                                             u'La fecha de inicio '
                                             u'de la ultima '
                                             u'suspensión tiene '
                                             u'que ser mayor '
                                             u'a la fecha de fin de '
                                             u'la anterior suspensión'):

                    return False
        return True

    def _generar_codigo_local(self, values=None):
        codigo_generado = ""
        ut_id = False
        if not values:
            if self.ut_id:
                ut_id = self.ut_id.id
        else:
            ut_id = values['ut_id']

        if ut_id:
            ut = self.env['hr.department']. \
                search([('id', '=', ut_id)])
            if ut:
                departamento = self.env['res.country.state'].\
                    search([('id', '=', ut.departamento_id.id)])
                total_locales = self.search_count([('id', '!=', False)])
                total_por_ut = self.search_count([('ut_id',
                                                   '=', ut_id)])
                if departamento.code_pe:
                    codigo_generado = '-'.join([self._prefijo_codigo,
                                                departamento.code_pe,
                                                format(total_por_ut + 1,
                                                       self.
                                                       _completar_con_zeros),
                                                str(total_locales + 1)])
                else:
                    codigo_generado = '-'.join([self._prefijo_codigo,
                                                format(total_por_ut + 1,
                                                       self.
                                                       _completar_con_zeros),
                                                str(total_locales + 1)])
        return codigo_generado

    @api.onchange('ut_id')
    def _al_cambiar_ut(self):
        self.code = self._generar_codigo_local(None)

    @api.onchange('comite_id')
    def _al_cambiar_comite(self):
        if self.comite_id:
            intervenciones_comite = \
                self.env['pncm.comitegestion.intervencion'].search(
                    [('comite_gestion_id', '=', self.comite_id.id)])
            departamentos_ids = [obj.departamento_id.id for
                                 obj in intervenciones_comite]
            return {'domain': {'departamento_id': ['&',
                                                   ('country_id.code',
                                                    '=', 'PE'),
                                                   ('id', 'in',
                                                    departamentos_ids)]}}

    @api.onchange('departamento_id')
    def _al_cambiar_departamento(self):
        if self.departamento_id:
            intervenciones = self.env['pncm.comitegestion.intervencion'].\
                search([('comite_gestion_id', '=', self.comite_id.id)])
            provincia_ids = [obj.provincia_id.id for obj in intervenciones]
            return {'domain': {'provincia_id': [('id', 'in', provincia_ids)]}}

    @api.onchange('provincia_id')
    def _al_cambiar_provincia(self):
        if self.provincia_id:
            intervenciones = self.env['pncm.comitegestion.intervencion'].\
                search([('comite_gestion_id', '=', self.comite_id.id)])
            desitritos_ids = [obj.distrito_id.id for obj in intervenciones]
            return {'domain': {'distrito_id': [('id', 'in', desitritos_ids)]}}

    @api.onchange('distrito_id')
    def _al_cambiar_distrito(self):
        if self.distrito_id:
            intervenciones = self.env['pncm.comitegestion.intervencion']. \
                search([('comite_gestion_id', '=', self.comite_id.id)])
            centro_poblado_ids = [obj.centro_poblado_id.id
                                  for obj in intervenciones]
            return {'domain': {'centro_poblado_id': [('id', 'in',
                                                      centro_poblado_ids)]}}

    @api.multi
    def _obtener_punto_mapa(self):
        for rec in self:
            if rec.lat and rec.lng:
                rec.map = json.dumps(
                    {u'position': {u'lat': float(rec.lat),
                                   u'lng': float(rec.lng)}, u'zoom': 10})

    @api.constrains('fichas_informe')
    def _validar_informes(self):
        if not self.fichas_informe:
            raise ValidationError(u"Debe registrar al menos "
                                  u"una ficha de informe de local")

    @api.constrains('fichas_preevaluacion')
    def _validar_preevaluacion(self):
        if not self.fichas_preevaluacion:
            raise ValidationError(u"Debe registrar al menos "
                                  u"una ficha de pre-evaluación de local")

    @api.constrains('afectaciones')
    def _contrain_afectaciones(self):
        return self._validar_afectaciones()

    def _validar_afectaciones(self):
        result = True
        if self.afectaciones:
            dhelper = utils.DateHelper()
            for r in self.afectaciones:
                if result:
                    result = dhelper.\
                        _validar_fecha_mayor(r.fecha_inicio, r.fecha_fin,
                                             u'Para todas las afectaciones'
                                             u'la fecha de termino debe ser'
                                             u'mayor a la fecha de inicio')

                    if result:
                        if not r.archivo_acta or not r.archivo_declaracion:
                            raise ValidationError(u'Debe adjuntar el '
                                                  u'acta y la declaración '
                                                  u'de las afectaciones')
                            result = False
                    if not result:
                        break
        return result

    @api.constrains('convenios')
    def _contrain_convenios(self):
        return self._validar_convenios()

    def _validar_convenios(self):
        result = True
        if self.convenios:
            dhelper = utils.DateHelper()
            for r in self.convenios:
                if result:
                    result = dhelper. \
                        _validar_fecha_mayor(r.fecha_inicio, r.fecha_fin,
                                             u'Para todos los convenios'
                                             u'la fecha de termino debe ser '
                                             u'mayor a la fecha de inicio')
                    if result:
                        if not r.archivo:
                            raise ValidationError(u'Debe adjuntar el '
                                                  u'documento de los '
                                                  u'convenios')
                            result = False
                    if not result:
                        break
        return result

    @api.multi
    def editar_identificacion(self):
        vhelper = utils.ModelHelper()
        view_form = self.env.ref('pncm_infra.pncm_infra_local_'
                                 'view_edit_identificaion_form', False)
        context = self._context.copy()
        vhelper.set_defaults_values_for_view(self, context, [
            'name', 'tipo_local', 'modalidad_intervencion', 'procedencia',
            'propietario_id', 'direccion', 'referencia', 'tipo_local',
            'pais_id', 'departamento_id', 'provincia_id', 'distrito_id',
            'centro_poblado_id', 'lat', 'lng', 'code', 'comite_id'
        ])

        context['default_ut_id'] = self.ut_id.name
        context['default_comite_name'] = self.comite_id.name
        return {
            'name': 'Editar Identificación de Local',
            'type': 'ir.actions.act_window',
            'res_model': 'pncm.infra.wizard.local.editar_identificacion',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'views': [(view_form.id, 'form')],
            'view_id': view_form.id,
            'context': context
        }

    @api.multi
    def visualizar_fotos(self):
        if len(self.fotos) > 0:
            view_form = self.env.ref('pncm_infra.wizard_form_fotos_view',
                                     False)
            context = self._context.copy()
            return {
                'name': 'Fotos del local',
                'type': 'ir.actions.act_window',
                'res_model': 'pncm.infra.local.fotos.wizard',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'views': [(view_form.id, 'form')],
                'view_id': view_form.id,
                'context': context
            }
        else:
            raise ValidationError(u'No hay fotos para mostrar')

    @api.multi
    def saneamiento_inicial(self):
        strhelper = utils.StringHelper()
        if self.name and not strhelper._is_white_space(self.name):
            self.write(dict(state='saneamiento'))
        else:
            raise ValidationError(u'Primero debe completar el nombre del '
                                  u'local para pasar a saneamiento')

    def _validar_saneamiento_inicial(self):
        result = {'success': False, 'code': 'sin_acondicionamiento'}
        if self.presupuestos and self.state == 'saneamiento':
            for r in self.presupuestos:
                if r.tipo_transferencia == 'acondicionamiento':
                    if r.fecha_culmina_obra:
                        result['success'] = True
                        break
                    else:
                        result['code'] = 'sin_fecha_culmina'
                        break
        return result

    @api.multi
    def activar_local(self):
        if len(self.afectaciones) > 0 or len(self.convenios) > 0:
            if self._validar_convenios() and \
                    self._validar_afectaciones():
                result = self._validar_saneamiento_inicial()
                if result['success']:
                    self.write(dict(state='activacion'))
                elif result['code'] == 'sin_acondicionamiento':
                    raise ValidationError(u'No se ha registrado el '
                                          u'presupuesto de saneamiento '
                                          u'inicial')
                elif result['code'] == 'sin_fecha_culmina':
                    raise ValidationError(u'No se ha registrado la fecha de '
                                          u'culminación de obra en la etapa '
                                          u'de ejecución del presupuesto')
        else:
            raise ValidationError(u'Debe ingresar al menos un documento '
                                  u'de sustentacion, por ejemplo: '
                                  u'afectaciones o convenios')

    @api.multi
    def suspender_local(self):
        if self.es_activado:
            self.write(dict(state='suspension'))
        else:
            raise ValidationError(u'El local debe estar activo para que '
                                  u'se pueda suspender')

    @api.multi
    def dar_de_baja(self):
        if self.es_activado:
            if self.state == 'suspension':
                if len(self.suspensiones) > 0:
                    self.write(dict(state='baja',
                                    no_validar_suspension=True))
                else:
                    raise ValidationError(u'No ha registrado ninguna '
                                          u'suspensión')
            elif self.state == 'activacion':
                self.write(dict(state='baja'))
        else:
            raise ValidationError(u'El local debe estar activo para que '
                                  u'se pueda dar de baja')

    @api.multi
    def reactivar_local(self):
        if self.state == 'suspension':
            if len(self.suspensiones) > 0:
                self.write(dict(state='activacion',
                                no_validar_suspension=True))
            else:
                raise ValidationError(u'No ha registrado ninguna '
                                      u'suspensión')

    @api.onchange('fecha_activacion')
    def al_cambiar_fecha_activacion(self):
        if self.fecha_activacion:
            result = self._validar_fecha_activacion(self.fecha_activacion)
            if result['success']:
                self.es_activado = True
            else:
                self.es_activado = False
                raise ValidationError(result['message'])

    def _validar_fecha_activacion(self, fecha_activacion):
        result = {'success': True, 'message': ''}
        if fecha_activacion:
            dhelper = utils.DateHelper()
            fecha_recepcion_obra = False
            for r in self.presupuestos:
                if r.tipo_transferencia == 'acondicionamiento':
                    fecha_recepcion_obra = r.fecha_recepcion_obra
                    break

            if fecha_recepcion_obra:
                if not dhelper._validar_fecha_cierre_sin_mensaje(
                        fecha_recepcion_obra,
                        fecha_activacion):
                    result['message'] = u'La fecha de activación ' \
                                         u'tiene que ser mayor o ' \
                                         u'igual que la fecha de ' \
                                         u'recepción de obra'
                    result['success'] = False
            else:
                result['message'] = u'No se ha ingresado fecha de recepción ' \
                                    u'del proyecto en el presupuesto de ' \
                                    u'acondicionamiento inicial, ' \
                                    u'por lo tanto ' \
                                    u'no puede activar el local'
                result['success'] = False
        return result

    def _get_default_activo(self):
        self.es_activo_read = self.es_activado

    @api.constrains(
        'fecha_baja'
    )
    def _contrain_fecha_baja(self):
        if self.state and self.state == 'baja':
            if self.fecha_baja:
                return self._validar_fecha_baja(self.fecha_baja)

    @api.onchange('fecha_baja')
    def _al_cambiar_fecha_baja(self):
        if self.fecha_baja:
            self._validar_fecha_baja(self.fecha_baja)

    def _validar_fecha_baja(self, fecha_baja):
        dhelper = utils.DateHelper()
        return dhelper._validar_fecha_mayor(self.fecha_activacion, fecha_baja,
                                            u'La fecha de baja debe ser '
                                            u'mayor a la fecha de activación '
                                            u'del local')


class CMFotosLocal(models.Model):
    _name = 'pncm.infra.local.foto'

    tipo_foto_local = fields.Selection(
        selection=[
            ('exterior_a', u'Vista exterior del local'),
            ('exterior_b',
             u'Vista exterior del local '
             u'de tener mas de un lado o vista interior'),
            ('exterior_c',
             u'Vista exterior del local de tener mas de dos lados o vista '
             u'interior'),
            ('croquis', u'Vista croquis'),
            ('sshh', u'Vista de servicios higiénicos'),
            ('otros', u'Vista de otros ambientes'),
            ('interior',
             u'Vista interior de la sala con pared, '
             u'piso y techo'),
            ('sa',
             u'Vista interior del servicio alimentario '
             u'en caso el CCD cuente. De no contar, '
             u'vista interior de la sala'),
            ('exterior_d',
             u'Vista exterior del local de '
             u'tener mas de tres lados o vista interior')
        ],
        string=u'Tipo Foto del Local',
        required=True
    )
    name = fields.Char(string=u'Nombre de foto', required=True)
    foto = fields.Binary(
        string=u'Archivo',
        required=True,
        help=u'Buscar foto de Local en mi computadora',
        filename=name
    )
    descripcion = fields.Char(string=u'Descripción')
    local_id = fields.Many2one(
        'pncm.infra.local',
        string=u'Local',
        required=True
    )


class CMPreEvaluacion(models.Model):
    _name = 'pncm.infra.local.preevaluacion'

    archivo = fields.Binary(
        string=u'Archivo',
        required=True,
        help=u'Buscar documento en mi computadora'
    )
    name = fields.Char(string=u'Nombre de Archivo', required=True)
    descripcion = fields.Char(string=u'Descripción')
    local_id = fields.Many2one(
        'pncm.infra.local',
        string=u'Local',
        required=True
    )


class CMFichaInforme(models.Model):
    _name = 'pncm.infra.local.informes'

    archivo = fields.Binary(
        string=u'Archivo',
        required=True,
        help=u'Buscar documento en mi computadora'
    )
    name = fields.Char(string=u'Nombre de Archivo', required=True)
    descripcion = fields.Char(string=u'Descripción')
    local_id = fields.Many2one(
        'pncm.infra.local',
        string=u'Local',
        required=True
    )


class CMVisualizarFotos(models.TransientModel):
    _name = 'pncm.infra.local.fotos.wizard'

    fotos = fields.One2many(
        'pncm.infra.local.foto',
        'local_id',
        stored=False,
        default=lambda self: self._default_lista_fotos()
    )

    def _default_lista_fotos(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            local = self.env['pncm.infra.local'].browse(active_id)
            return local.fotos
        return False
