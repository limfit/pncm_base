# coding: utf-8

from odoo import api, models


def get_convenios(rec):
    convenios = rec.comite_gestion_id.convenios_ids
    for c in convenios:
        if c.estado == 'vigente':
            return c.name
    return ''


def get_presidente(rec):
    presidente = rec.comite_gestion_id.afiliaciones_ids.filtered(
        lambda x: not x.fecha_retiro and x.cargo == 'PRESIDENTE DE COMITE DE GESTION')
    nombre = ' {} {}, {}'.format(
        presidente.person_id.firstname, presidente.person_id.secondname, presidente.person_id.name) \
        if presidente else ''
    return {
        'name': nombre.upper() if nombre else '',
        'dni': presidente.person_id.document_number if presidente else ''
    }


def get_tesorero(rec):
    tesorero = rec.comite_gestion_id.afiliaciones_ids.filtered(
        lambda x: not x.fecha_retiro and x.cargo == 'TESORERO DE COMITE DE GESTION')
    nombre = ' {} {}, {}'.format(
        tesorero.person_id.firstname, tesorero.person_id.secondname, tesorero.person_id.name) \
        if tesorero else ''
    return {
        'name': nombre.upper() if nombre else '',
        'dni': tesorero.person_id.document_number if tesorero else ''
    }


def get_fac_nro(docs):
    if docs.facilitador_planilla_id:
        return docs.facilitador_planilla_id.nro_persona
    return 0


def get_adm_nro(docs):
    if docs.apoyo_adm_planilla_id:
        return docs.apoyo_adm_planilla_id.nro_persona
    return 0


def get_importe_utilizado_fac(docs):
    if docs.facilitador_planilla_id:
        if docs.facilitador_planilla_id.actor_lines_ids:
            return sum(rec.monto for rec in docs.facilitador_planilla_id.actor_lines_ids)
    return 0


def get_importe_utilizado_adm(docs):
    if docs.apoyo_adm_planilla_id:
        if docs.apoyo_adm_planilla_id.actor_lines_ids:
            return sum(rec.monto for rec in docs.apoyo_adm_planilla_id.actor_lines_ids)
    return 0


class ReportFormatoDesembolso(models.AbstractModel):
    _name = 'report.transferencias_devoluciones.formato_desembolso_template'

    @api.multi
    def render_html(self, req_id, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('transferencias_devoluciones.formato_desembolso_template')
        docs = self.env['requerimiento.lines'].browse(req_id)
        ano, mes, dia = docs.desembolso_id.fecha_actual.split('-')
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': docs,
            'fecha': self.get_fecha(docs),
            'convenio': get_convenios(docs),
            'presidente': get_presidente(docs),
            'tesorero': get_tesorero(docs),
        }
        return report_obj.render('transferencias_devoluciones.formato_desembolso_template', docargs)

    @api.multi
    def get_data(self, rec):
        print(rec.comite_gestion_id.name)

    @api.multi
    def get_fecha(self, rec):
        for r in rec:
            desembolso = rec.desembolso_id
            fecha = desembolso.fecha_actual.split("-")
            ano, mes, dia = fecha
            return {
                'dia': dia,
                'mes': mes,
                'ano': ano
            }
        return {}


class ReportComprobanteJustificaciones(models.AbstractModel):
    _name = 'report.transferencias_devoluciones.justificacion_template'

    @api.multi
    def render_html(self, req_id, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('transferencias_devoluciones.justificacion_template')
        docs = self.env['justificaciones.lines'].browse(req_id)
        docargs = {
            'doc_ids' : self._ids,
            'doc_model' : report.model,
            'docs' : docs,
            'convenio' : get_convenios(docs),
            'presidente' : get_presidente(docs),
            'tesorero': get_tesorero(docs)
        }
        if (docs.servicio == 'scd'):
            return report_obj.render('transferencias_devoluciones.justificacion_template_scd', docargs)
        if (docs.servicio == 'saf'):
            return report_obj.render('transferencias_devoluciones.justificacion_template_saf', docargs)
        return None


class ReportPlanillaJustificaciones(models.AbstractModel):
    _name = 'report.transferencias_devoluciones.planilla_template'

    @api.multi
    def render_html(self, req_id, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('transferencias_devoluciones.planilla_template')
        docs = self.env['justificacion.actor'].browse(req_id)
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': docs,
            'presidente': get_presidente(docs),
            'tesorero': get_tesorero(docs)
        }

        if docs.servicio == 'scd':
            return report_obj.render('transferencias_devoluciones.planilla_template_scd', docargs)
        if docs.servicio == 'saf':
            return report_obj.render('transferencias_devoluciones.planilla_template_saf', docargs)
        return None


class ReportGastosJustificacionesSCD(models.AbstractModel):
    _name = 'report.transferencias_devoluciones.gastos_template'
    
    @api.multi
    def render_html(self, req_id, data=None):
        report_obj= self.env['report']
        report = report_obj._get_report_from_name('transferencias_devoluciones.gastos_template')
        docs = self.env['justificacion.gastos'].browse(req_id)
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': docs,
            'presidente': get_presidente(docs),
            'tesorero': get_tesorero(docs),
            'fac_nro': get_fac_nro(docs) if docs.servicio == 'saf' else 0,
            'adm_nro': get_adm_nro(docs) if docs.servicio == 'saf' else 0,
            'importe_utilizado_fac': get_importe_utilizado_fac(docs) if docs.servicio == 'saf' else 0,
            'importe_utilizado_adm': get_importe_utilizado_adm(docs) if docs.servicio == 'saf' else 0,
        }
        if docs.servicio == 'scd':
            return report_obj.render('transferencias_devoluciones.gastos_template_scd', docargs)
        if docs.servicio == 'saf':
            return report_obj.render('transferencias_devoluciones.gastos_template_saf', docargs)

